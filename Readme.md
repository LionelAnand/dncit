## Installation

### Script Installation
```bash
sh ./install.sh --USERNAME="<git_username>" --email="<git_user_email>"
```
and your project should be now available at your Desktop.
### Manual Installation

#### 1. Git Setup
```bash
sudo apt-get install git
which git
git config --global user.name "<git_username>"
git config --global user.email "<git_user_email>"
git clone https://<git_username>@bitbucket.org/dncit/dnc_test.git
```

#### 2. Dependencies Setup

```bash
sudo apt-get install php7.2-curl
sudo apt-get install php7.2-dom
sudo apt-get install php7.20-mcrypt
sudo apt-get install php7.2-simplexml
sudo apt-get install php7.2-spl
sudo apt-get install php7.2-xsl
sudo apt-get install php7.2-intl
sudo apt-get install php7.2-mbstring
sudo apt-get install php7.2-ctype
sudo apt-get install php7.2-hash
sudo apt-get install php7.2-openssl
sudo apt-get install php7.2-zip
sudo apt-get install php7.2-xmlwriter
sudo apt-get install php7.2-gd
sudo apt-get install php7.2-iconv
sudo apt-get install curl
sudo apt-get install php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml php-token-stream php7.2-json
curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
sudo chmod +x /usr/local/bin/composer
```
#### 3. App setup
```bash
composer install
composer update
php artisan dusk:install
```


#### 4.Env setup

During your first installation or cloning, you'll need a 
".env" file which won't be cloned along with the other files.

To get the file just copy the environment.php file and rename the copied file to ".env"
OR run _cp environment.php .env_ in your terminal.
#
Setup complete, you're ready to go!


### Useful Dusk Commands ###

```bash
php artisan dusk:chrome-driver          Install the ChromeDriver binary
php artisan dusk:component              Create a new Dusk component class
php artisan dusk:fails                  Run the failing Dusk tests from the last run and stop on failure
php artisan dusk:install                Install Dusk into the application
php artisan dusk:make                   Create a new Dusk test class
php artisan dusk:page                   Create a new Dusk page class
php artisan make:test                   Create a new test class
php artisan dusk                        Run full suite of test cases
php artisan dusk folder/path/folder/    Run only a single folder
php artisan dusk --filter class_name    Run single test case
```