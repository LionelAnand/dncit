<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;

class groupsPage extends branchesPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function click_enroll_subscriber(Browser $browser)
    {
        $browser->press('Enroll Subscriber');
    }

    public function enroll_subscriber(Browser $browser)
    {
        $browser->press('Enroll');
    }

    public function remove_the_enrollment(Browser $browser)
    {
        $browser->click('#btnGroupTabs1')->press('Remove Enrollment')->assertsee('removed sucessfully.');
    }

    public function click_new_group(Browser $browser)
    {
        $browser->clickLink('New Group');
    }

  

    public function fill_group_edit_details(Browser $browser, $edit_details)
    {
        foreach ($edit_details as $key => $value) {
            switch ($key) {
          case 'MonthyAuctionDate1':
          $browser->select('#monthly_auction_date', $value);
          break;
          case 'AuctionTime':
          $browser->type('#auction_time', $value);
          break;
          case 'PSONo':
          $browser->type('#pso_no', $value);
          break;
          case 'PSODate':
          $browser->type('#pso_date', $value);
          break;
          case 'ByLawNo':
          $browser->type('#by_law_no', $value);
          break;
          case 'ByLawDate':
          $browser->type('#by_law_date', $value);
          break;
          case 'FDAmount':
          $browser->type('#fd_amount', $value);
          break;
          case 'FDInterest':
          $browser->type('#fd_interest', $value);
          break;
          case 'FDDate':
          $browser->type('#fd_date', $value);
          break;
          case 'FDNumber':
          $browser->type('#fd_number', $value);
          break;
          case 'FDReferenceNo':
          $browser->type('#fd_reference_no', $value);
          break;
          case 'FDBankName':
          $browser->type('#fd_bank_name', $value);
          break;
          case 'FDBranchName':
          $browser->type('#fd_branch_name', $value);
          break;
          default:
                // code...
          break;  }
        }
    }

    public function delete_the_group(Browser $browser)
    {
        $browser->clicklink('Group Details')
                ->clickLink('Delete')
    //$browser->assertDialogOpened('Are you sure you want to delete this group?');
        ->acceptDialog();
    }

    public function search_and_delete_the_group(Browser $browser, $group)
    {
        $browser->go_to_group($group->name)
                ->clicklink('Group Details')
                ->clickLink('Delete')
                ->assertDialogOpened('Are you sure you want to delete this group?')
                ->acceptDialog();
    }

    public function search_and_click_edit_group_button(Browser $browser, $group_name)
    {
        $browser->go_to_group($group_name)
        ->clickLink('Group Details')
        ->clickLink('Edit Group Details');
    }

    public function remove_an_enrolment(Browser $browser)
    {
        $browser->click('#btnGroupTabs1')
        ->click('div > div > div > form > button > i');
    }

    public function fill_notes(Browser $browser, $notes)
    {
        $browser->type('notes', $notes);
    }

    public function fill_employee_to_edit_enrollment(Browser $browser, $subscriber, $link)
    {
        $browser->clear('#main-container input[name = "employee"]')
        ->type('#main-container input[name = "employee"]', $subscriber)
        ->waitForLink($link, 60)
        ->clickLink($link);
    }

    public function fill_nominee_to_edit_enrollment(Browser $browser, $nominee, $relationship)
    {
        $browser->clear('nominee_name')
        ->type('nominee_name', $nominee)
        ->select('#nominee_relationship', $relationship);
    }

    public function fill_introducer_to_edit_enrollment(Browser $browser, $introducer, $link)
    {
        $browser->type('#main-container input[name = "introducer"] ', $introducer)
        ->waitForLink($link, 60)
        ->clickLink($link);
    }

    public function enroll_the_subscriber(Browser $browser)
    {
        $browser->press('Enroll')
        ->pause(2000);
        //->assertsee('enrolled sucessfully.');
    }

    public function click_edit_enrollment_details_button(Browser $browser)
    {
        $browser->click('#main-container > div.bg-primary-dark > div > div > div.col-2.text-right.d-print-none > a');
    }

    public function remove_introducer_commission_disbursement(Browser $browser)
    {
        $browser->click('Delete Introducer Commission Disbursement');
    }

    public function save_enrollment_form(Browser $browser)
    {
        $browser->press('Save');
    }

    public function introducer_name_in_enrollment_form(Browser $browser, $introducer, $link)
    {
        $browser->type('introducer_name_in_enrollment_form', $introducer)
    ->waitForLink($link, 60)
    ->clickLink($link);
    }

    public function disburse_introducer_comission_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('Disburse Introducer Commission');
    }

    public function elements()
    {
        return [
            'Enroll Subscriber' => '#main-container > div.content > div:nth-child(2) > div > div > ul > li.nav-item.ml-auto > div > a',
            'subscriberForEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > input.form-control.entity-search',
            'introducerForEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(6) > div > input.form-control.entity-search',
            'employeeForEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(7) > div > input.form-control.entity-search',
            'enrollButton' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(9) > div.col-sm-9 > button',
            'nominee_name' => '#main-container input[name = "nominee_name"]',
            'Enrollments(1)' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row:nth-child(2) div.col-lg-12 div.block.block-rounded.block-bordered ul.nav.nav-tabs.nav-tabs-alt.nav-tabs-block.align-items-center li.nav-item:nth-child(2) > a.nav-link.active',
            'introducerForEditingEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(7) > div > input.form-control.entity-search',
            'employeeForEditingEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(8) > div > input.form-control.entity-search',
            'nomineeNameForEditingEnrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(9) > div.col-sm-6 > input',

            'Number of Active Enrollments in Group' => '#btabs-static-enrollments > table > tbody > tr > td:not(.table-secondary):not(.table-danger) > a',

            'Chit Value in Commission Disbursement' => '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(1) > td.text-right',

            'Chit Value in Groups Page' => '#main-container > div.content > div:nth-child(1) > div:nth-child(3) > div > div > div.font-size-h4.font-w400.text-dark',

            'Number of Enrollments in the groups page' => '#main-container > div.content > div.col-lg-12 > div > div.block-content > div > div > table > tbody > tr > td:nth-child(9)',

            'Number of Auctions in the groups page' => '#main-container > div.content > div.col-lg-12 > div > div.block-content > div > div > table > tbody > tr > td:nth-child(10)',

            'Delete Introducer Commission Disbursement' => '#introducer_commission_tab > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(8) > form > button',

            'introducer_name_in_enrollment_form' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(7) > div > input.form-control.entity-search',

            'Enroll' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(10) > div.col-sm-9 > button',

            'Other charges tab in enrollment ledger' => '#enrollment_ledger_tabs > li:nth-child(3) > a',

            'Edit Enrollment Details' => '#main-container > div.bg-primary-dark > div > div > div.col-2.text-right.d-print-none > a',

            'Disburse Introducer Commission' => '#introducer_commission_tab > div > div:nth-child(1) > div > div.col-md-6.text-right > a',
        ];
    }
}
