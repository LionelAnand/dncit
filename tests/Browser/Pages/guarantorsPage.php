<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;

class guarantorsPage extends groupsPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function go_to_and_click_new_gurarantor_button(Browser $browser)
    {
        $browser->clickLink('Tambaram')
        ->clickLink('Guarantors')
        ->clickLink('New Guarantor');
    }

    public function fill_guarantor_details(Browser $browser, $guarantor_details)
    {
        foreach ($guarantor_details as $key => $value) {
            switch ($key) {
                case 'fullname':
                $browser->type('#name', $value);
                break;
                case 'branch':
                $browser->select('GuarantorBranch', $value);
                break;
                case 'gender':
                $browser->select('#gender', $value);
                break;
                case 'dob':
                $browser->type('#dob', $value);
                break;
                case 'mobile_primary':
                $browser->type('#mobile_primary', $value);
                break;
                case 'mobile_alternate':
                $browser->type('#mobile_alternate', $value);
                break;
                case 'pan_no':
                $browser->type('#pan_no', $value);
                break;
                case 'ration_no':
                $browser->type('#ration_no', $value);
                break;
                case 'aadhaar_no':
                $browser->type('#aadhaar_no', $value);
                break;
                case 'address_present':
                $browser->type('#address_present', $value);
                break;
                case 'city_present':
                $browser->type('#city_present', $value);
                break;
                case 'district_present':
                $browser->type('#district_present', $value);
                break;
                case 'state_present':
                $browser->type('#state_present', $value);
                break;
                case 'pincode_present':
                $browser->type('#pincode_present', $value);
                break;
                case 'address_permanent':
                $browser->type('#address_permanent', $value);
                break;
                case 'city_permanent':
                $browser->type('#city_permanent', $value);
                break;
                case 'district_permanent':
                $browser->type('#district_permanent', $value);
                break;
                case 'state_permanent':
                $browser->type('#state_permanent', $value);
                break;
                case 'pincode_permanent':
                $browser->type('#pincode_permanent', $value);
                break;
                case 'source of income':
                $browser->type('#source_of_income', $value);
                break;
                case 'monthly income':
                $browser->type('#monthly_income', $value);
                break;
                case 'father name':
                $browser->type('#father_name', $value);
                break;
                case 'mother name':
                $browser->type('#mother_name', $value);
                break;
                case 'spouse name':
                $browser->type('#spouse_name', $value);
                break;

                default:
                // code...
                break;
            }
        }
    }

    public function assert_that_guarantor_is_created(Browser $browser)
    {
        $browser->assertSee('Aravind')
        ->pause(3000);
    }

    public function click_edit_guarantor_button(Browser $browser)
    {
        $browser->clickLink('Edit Guarantor Details');
    }

    public function save_the_guarantor(Browser $browser)
    {
        $browser->press('Save');
    }

    public function assert_thet_the_guarantor_is_edited(Browser $browser)
    {
        $browser->assertsee('Guarantor details saved.');
    }

    public function convert_guarantor_to_subscriber(Browser $browser, $guarantor)
    {
        $browser->clickLink($guarantor)
        ->click('Convert to Suscriber')
        ->acceptDialog()
        ->assertSee('Guarantor has been converted into a Subscriber.');
    }

    public function go_to_guarantors_page_of_branch(Browser $browser, $branch)
    {
        $browser->clickLink('Branches')
        ->clickLink($branch)
        ->clickLink('Guarantors');
    }

    public function go_to_guarantor(Browser $browser, $branch, $guarantor)
    {
        $browser->clickLink('Branches')
        ->clickLink($branch)
        ->clickLink('Guarantors')
        ->clickLink($guarantor);
    }

    public function delete_the_guarantor_documents(Browser $browser)
    {
        $browser->press('#btnGroupTabs1')
        ->pause(500)
        ->clickLink('Edit Details')
        ->press('Delete')
        ->assertSee('Document removed successfully.');
    }

    public function remove_relationship(Browser $browser)
    {
        $browser->click('Remove Relationship')
                     ->assertsee('Relationship removed successfully.');
    }
       
    
    public function filter_possibly_vacant_enrollments(Browser $browser)
    {
        $browser->select('#enrollment_status', 'Possibly Vacant');
    }

    public function filter_eligible_for_recovery_enrollments(Browser $browser)
    {
        $browser->select('#enrollment_status', 'Eligible for Recovery');
    }

    public function filter_eligible_for_legal_actions_enrollments(Browser $browser)
    {
        $browser->select('#enrollment_status', 'Eligible for Legal Action');
    }

    public function filter_as_all_enrollment(Browser $browser)
    {
        $browser->select('#group_status', 'All');
    }

    public function select_one_enrollment_from_generated_report(Browser $browser, $enrollment_link)
    {
        $browser->clickLink($enrollment_link);
    }

    

    public function elements()
    {
        return [
            'branch' => 'div.form-group.row:nth-child(4) div.col-sm-4',
            'GuarantorBranch' => 'div.form-group.row:nth-child(4) div.col-sm-4',
            'Convert to Suscriber' => '#main-container > div.content > div:nth-child(5) > div.block-header.border-bottom > div > a.btn.btn-sm.btn-warning',
            //   'Edit Details' => '#documents_tab > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > div > div > a',
            'Type Detail' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search',
            'Select Relationship Type' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(4) > div > select',
            'Remove Relationship' => '#main-container > div.content > div:nth-child(2) > div > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(6) > form > button',
            'Relationship Input Field' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > input.form-control.entity-search',
                      
            
        ];
    }
}
