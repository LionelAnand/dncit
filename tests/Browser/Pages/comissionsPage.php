<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;

class comissionsPage extends branchesPage
{

    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;
    


    

    public function click_group_wise_from_introducer_commission_in_menu(Browser $browser)
    {
        $browser->click('introducer group wise');
    }

    public function clcik_introducer_name(Browser $browser, $introducer_Name)
    {
        $browser->clickLink($introducer_Name);
    }

    public function click_commission_group(Browser $browser, $commission_Group)
    {
        $browser->clickLink($commission_Group);
    }

    public function fill_commission_disbursement_details(Browser $browser, $disbursement)
    {
        foreach ($disbursement as $key => $value) {
            switch ($key) {
                case 'Amount':
                $browser->type('#amount', $value);
                break;
                case 'Disbursement Date':
                $browser->select('#receipt_date', $value);
                break;
                default:
                // code...
                break;
            }
        }
    }

    public function click_subscriber_in_enrollments_page(Browser $browser, $subscriber_name)
    {
        $browser->clickLink($subscriber_name);
    }

    public function go_to_introducer_wise_comission_report(Browser $browser)
    {
        $browser->click('Introducer Comm.')
        ->click('Introducer Wise');
    }

    public function generate_introduce_wise_comission_report(Browser $browser, $branch_id, $from_date, $to_date)
    {
        $browser->select('#branch_id', $branch_id)
        ->type('#from_date', $from_date)
        ->type('#to_date', $to_date)
        ->press('Generate');
    }



    public function assert_commission_details_with_pan_card(Browser $browser, $tds, $disbursable_commission, $undisbursed_commission) 
    {       
        $browser->assertSeeIn('TDS amount in Commission Disbursement Details', $tds);
        $browser->assertSeeIn('Disbursable Commission in Enrollment Ledger', $disbursable_commission);
        $browser->assertSeeIn('Undisbursable Commission in Enrollment Ledger', $undisbursed_commission);
    }

    public function assert_commission_details_without_pan_card(Browser $browser, $tds, $disbursable_commission, $undisbursed_commission) 
    {   
        $browser->assertSeeIn('TDS amount in Commission Disbursement Details', $tds);
        $browser->assertSeeIn('Disbursable Commission in Enrollment Ledger', $disbursable_commission);
        $browser->assertSeeIn('Undisbursable Commission in Enrollment Ledger', $undisbursed_commission);
    }


    public function assert_commission_details_after_disbursement_with_pan_card(Browser $browser, $disbursable_commission, $undisbursed_commission) 
    {       
        $browser->assertSeeIn('Disbursable Commission in Enrollment Ledger', $disbursable_commission);
        $browser->assertSeeIn('Undisbursable Commission in Enrollment Ledger', $undisbursed_commission);
    }

    public function assert_commission_details_after_disbursement_without_pan_card(Browser $browser, $disbursable_commission, $undisbursed_commission) 
    {       
        $browser->assertSeeIn('Disbursable Commission in Enrollment Ledger', $disbursable_commission);
        $browser->assertSeeIn('Undisbursable Commission in Enrollment Ledger', $undisbursed_commission);
    }
    
    public function elements()
    {
        return [
            'introducer_branch_wise' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(12) > ul > li > a > span',
            'introducer group wise' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(12) > ul > li:nth-of-type(2) > a',

            'Employee Commission Policy 2'  => '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(4) > td:nth-child(11)',

            'Group Details' =>  '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(1) > a',

            'Enrollments'   =>  '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a',

            'Remove Disbursed Commission'   =>  '#introducer_commission_tab > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(8) > form > button',

            'Save Collection'   =>   '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(7) > div > div:nth-child(12) > div.col-sm-9 > button',

            'Chit Value in Dusbursement Commission' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(1) > td.text-right',

            'Commission Amount in Dusbursement Commission' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(2) > td.text-right',

            'TDS in Dusbursement Commission' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td.text-right',

            'Disbursable Commission in Dusbursement Commission' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-info > td.text-right',

            'Undisbursed Commission in Dusbursement Commission' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-warning > td.text-right',

            'Introducer Commission Disbursement Count'    =>  '#main-container > div.content > div:nth-child(3) > div > div:nth-child(5) > div.block-content > div > div:nth-child(2) > table > tbody > tr > td:nth-child(8) > form > button',

            'Disburse Introducer Commission'  =>  '#introducer_commission_tab > div > div:nth-child(1) > div > div.col-md-6.text-right > a',

            'Delete Introducer Commission Disbursement' => '#introducer_commission_tab > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(8) > form > button',

            'Introducer Comm.'  =>  '#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(12) > a',

      //'Introducer Wise' =>  'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(12) > ul > li:nth-of-type(3) > a > span',

            'Introducer Comission Tab in Enrollment Ledger' =>  '#enrollment_ledger_tabs > li:nth-child(6) > a',

            'Introducer Commission Tab in Enrollment Ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(6) > a',

            'Introductions Tab in Employee Ledger'  =>  '#employee_tabs > li:nth-child(2) > a',

            'TDS amount in Commission Disbursement Details' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td.text-right',

            'Disbursable Commission in Enrollment Ledger' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-info > td.text-right',

            'Undisbursable Commission in Enrollment Ledger' =>  '#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-warning > td.text-right',

        ];
    }
}