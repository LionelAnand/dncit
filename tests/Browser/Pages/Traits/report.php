<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;
use Facebook\WebDriver\WebDriverBy;

trait report
{

public function generate_introducer_wise_introducer_commission_report(Browser $browser, $branch_name, $from_date, $to_date){
        $browser->click_introducer_commission_in_menu()
                ->click_introducer_wise_from_introducer_commission_in_menu()
                ->fill_details_for_generating_introducer_wise_commission_report([
                        'Branch' => $branch_name,
                        'IntroducerType' => 'Any',
                        'FromDate' => $from_date,
                        'ToDate' => $to_date,
            ])
        		->press('Generate');

}

    public function generate_subscriber_wise_Ato_Report(Browser $browser, $branch, $group, $dates)
    {
        $browser->go_to_subscriber_wise_ATO_report()
        ->choose_branch($branch->id)
        ->select_status($group->status)
        ->enter_enrollment_date($dates->from_date, $dates->to_date)
        ->press('Generate')
        ->assertsee('Export')
        ->export_the_report();
    }
    public function generate_subscriber_wise_Eto_Report(Browser $browser, $branch, $group, $date)
    {
        $browser->go_to_subscriber_wise_ETO_report()
        ->choose_branch($branch->id)
        ->select_status($group->status)
        ->enter_enrollment_date($date->from_date, $date->to_date)
        ->press('Generate')
        ->assertsee('Export')
        ->export_the_report();
    }

    public function generate_group_wise_Ato_Report(Browser $browser, $branch, $group, $dates)
    {
        $browser->go_to_subscriber_wise_ATO_report()
        ->choose_branch($branch->id)
        ->select_status($group->status)
        ->enter_enrollment_date($dates->from_date, $dates->to_date)
        ->press('Generate')
        ->assertsee('Export')
        ->export_the_report();
    }

    public function go_to_subscriber_wise_interaction_menu(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(6) > a > span') 
        ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li > a > span');
    }

        public function select_eligible_for_legal(Browser $browser)
    {

        $browser->check('#is_legal_eligible_status')
                ->uncheck('#is_recovery_eligible_status', '#is_possibly_vacant_status')
                ->assertChecked('#is_legal_eligible_status')
                ->assertNotChecked('#is_recovery_eligible_status', '#is_possibly_vacant_status');

    }
    public function select_Eligible_for_Recovery(Browser $browser)
    {

        $browser->check('#is_recovery_eligible_status')
                ->uncheck('#is_legal_eligible_status', '#is_possibly_vacant_status')
                ->assertChecked('#is_recovery_eligible_status')
                ->assertNotChecked('#is_legal_eligible_status', '#is_possibly_vacant_status');
            }
                
    public function select_Possibly_Vacant(Browser $browser)
    {

        $browser->check('#is_possibly_vacant_status')
                ->uncheck('#is_recovery_eligible_status')
                ->uncheck('#is_legal_eligible_status')
                ->assertChecked('#is_possibly_vacant_status')
                ->assertNotChecked('#is_recovery_eligible_status','#is_legal_eligible_status');
                      
    }


        public function generate_report(Browser $browser)
        {
            $browser->press('Generate');
        }
        public function assert_interaction(Browser $browser)
    {
        $browser->assertsee('TO BE COLLECTED','COLLECTED','POSSIB. VACANT','RECOVERY','LEGAL ACTION');
    }

    public function go_to_removed_enrollments_report(Browser $browser, $branch, $min_balance)
        {
            $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(8) > a > span')
            ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a > span')
            ->assertSee('Branch', 'Min. Balance')
            ->select('#branch_id', $branch->id)
            ->select('#min_balance', $min_balance)
            ->press('Generate');
        }

    public function assert_removed_enrollment_in_removed_enrollment_report(Browser $browser, $subscriber, $enrollment, $date)
    {
        $browser->assertSee($subscriber->name, $enrollment->name, $date);
    }

    public function go_to_due_for_renewal_report(Browser $browser,$branch, $end_of_enrollment, $score)
        {
            $browser->clickLink('Enrollments')->clickLink('Due for Renewal')
            ->assertSee('Branch','Old Enr. End Dt.','New Enr. Start Dt.','Min. Balance')
            ->select('#branch_id', $branch->id)->select('#enrollment_end_to_date', $end_of_enrollment)->select('#score', $score)
            ->press('Generate')->assertSee('SUBSCRIBER','GROUPS','ENROLLMENT','ENDING DATE','INTRODUCERS');
        }


    public function generate_disbursed_prize_money_report(Browser $browser, $branch_id, $from_date, $to_date)
        {
            $browser->clickLink('Disbursed Pr. Mn.')
                    ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li > a > span')
            ->select('#branch_id', $branch_id)
            ->type('#from_date', $from_date)
            ->type('#to_date', $to_date)
            ->press('Generate');
        
        }

    public function generate_undisbursed_prize_money_report(Browser $browser, $branch_id)
        {
            $browser->clickLink('Undisbursed Pr. Mn.')
                    ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a > span')
            ->select('#branch_id', $branch_id)
            ->press('Generate');
    }

   public function generate_prized_money_released_report(Browser $browser, $branch, $dates)
   {
       $browser->visit('https://staging.dncchits.in/reports/disbursed_prize_money_date_wise')
       ->select('#branch_id', $branch->id)
       ->type('#from_date', $dates->from_date)
        ->type('#to_date', $dates->to_date)
    
       ->press('Generate')
       ->assertsee('Export')
       ->export_the_report();
   }


    public function get_upm_amount_from_undisbursed_prize_money_report(Browser $browser, $branch, $enrollment)
    {
        $browser->generate_undisbursed_prize_money_report($branch);
        /*$browser->collect('#main-container > div.content > div > div > div:nth-child(2) > div.block-content > table > tbody > tr:nth-child(n) > td:nth-child(3)');
        */
        $formatted_name =  collect($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[8]')));
        $formatted_name = $formatted_name->map(function($el){
            return $el->getText(); 
        });
            $enrollment->name = $browser->check_score_exists_in_name($enrollment->name)->formatted_name;
            $key = ($formatted_name->search($enrollment->name));  
            $key = $key + 1;
       
    }
    
}

