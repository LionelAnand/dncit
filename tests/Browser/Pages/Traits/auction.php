<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait auction
{
    public function go_to_auctions_tab_of_group(Browser $browser, $group)
    {
        $browser->go_to_group($group->name)
            ->go_to_auctions_tab_in_group_ledger();
    }

    public function remove_auction(Browser $browser)
    {
        $el = $browser->elements('tr:nth-child(2) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)');
        if (count($el) > 0) {
            $browser->clickLink('tr:nth-child(2) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)')
                                        ->assertSee('deleted');
        }
    }

    public function remove_last_auction(Browser $browser, $group)
    {
        $browser->go_to_group_with_id($group)
                ->go_to_auctions_tab_in_group_ledger();
        $number_of_auctions = $browser->elements('#btabs-static-enrollments > table > tbody > tr > td:nth-child(9) > form > button > i');
        $number_of_auctions = count($number_of_auctions);
        $browser->remove_particular_auction($number_of_auctions)
                ->assert_auction_removed();
    }

    public function calculate_gst(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
                ->clicklink('Prize Money');
        $enrollment->chit_value = $browser->text('#prize_money_tab > div.row.mb-2 > div > table > tbody > tr:nth-child(1) > td.text-right');
        $enrollment->chit_value = str_replace(',', '', $enrollment->chit_value) * 1;
        $enrollment->commission = $enrollment->chit_value * 5 / 100;
        $enrollment->calculated_gst = $enrollment->commission * 12 / 100;
        $enrollment->displayed_gst = $browser->text('#prize_money_tab > div.row.mb-2 > div > table > tbody > tr:nth-child(4) > td.text-right');
        $enrollment->displayed_gst = str_replace(',', '', $enrollment->displayed_gst) * 1;
    }

    public function add_auction_entry_for_particular_subscriber(Browser $browser, $group, $auction_date, $winning_enrollment_id, $bid_amount)
    {
        $browser->visit('/groups/'.$group->id.'/auctions/create');
        $comission = $browser->value('#commission');
        $comission = $browser->format_amount($comission)->format_amount;
        $gross_bid_amount = $comission + $bid_amount;
        $browser->type('#auction_date', $auction_date)
                ->select('#winning_enrollment_id', $winning_enrollment_id)
                ->type('#gross_bid_amount', $gross_bid_amount)
                ->press('Save')
                ->pause(6000);//to avoid many issues
    }

    public function select_enrollment_for_auction(Browser $browser, $group, $enrollment)
    {
        $browser->go_to_auctions_tab_in_group_with_id($group);

        $number_of_auctions = $browser->elements('#btabs-static-enrollments > table > tbody > tr > td:nth-child(9) > form > button > i');
        $number_of_auctions = count($number_of_auctions);

        $guaranteed = 1;
        while ($guaranteed > 0) {
            $number_of_auctions = $number_of_auctions + 1;
            $browser->go_to_auctions_tab_in_group_with_id($group);

            $name = $browser->text('#btabs-static-enrollments table tbody tr:nth-child('.$number_of_auctions.') td.text-left a');

            try {
                $browser->assertSeeLink($name)
                ->click('#btabs-static-enrollments table tbody tr:nth-child('.$number_of_auctions.') td.text-left a');
            } catch (\Exception $e) {
                $browser->select_enrollment_for_auction($group, $enrollment);
            }

            $browser->clickLink('Guarantees');
            $guaranteed = count($browser->elements('#guarantees_tab > div:nth-child(2) > div > table > tbody > tr > td:nth-child(4) > form'));
  
        }
        $enrollment->id=$browser->get_enrollment_id()->enrollment_id;
    }
}
