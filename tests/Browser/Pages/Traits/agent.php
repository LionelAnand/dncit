<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait agent
{

public function create_a_new_agent(Browser $browser, $name, $mobile_primary, $pan, $aadhar)
    {
        $browser->clickLink('New Agent')->fill_details_for_agent_creation($name, $mobile_primary, $pan, $aadhar)
                ->press('Save');
    }


    public function fill_details_for_agent_creation(Browser $browser, $name, $mobile_primary, $pan, $aadhar) {

        $browser->fill_up_the_details_for_agent_creation(
            [
                'fullname' => $name,
                'branch' => '20',
                'gender' => 'Male',
                'dob' => '15091985',
                'mobile_primary' => $mobile_primary,
                'mobile_alternate' => '9994448880',
                'pan_no' => $pan,
                'ration_no' => 'PK0012101',
                'aadhaar_no' => $aadhar,
                'address_present' => 'No 1',
                'city_present' => 'Sharjah',
                'district_present' => 'Dubai',
                'state_present' => 'Felicity',
                'pincode_present' => '969002',
                'address_permanent' => 'No 1',
                'city_permanent' => 'Sharjah',
                'district_permanent' => 'Dubai',
                'state_permanent' => 'Felicity',
                'pincode_permanent' => '969002',
                'source of income'   => 'DNC Salary',
                'monthly income'     => '12000',
                'father name'        => 'Steven Strange',
                'mother name'        => 'Maria',
                'spouse name'        => 'Scarlet'
            ]
        );

    }



    public function fill_up_the_details_for_agent_creation(Browser $browser, $agent_details)
    {
        foreach ($agent_details as $key => $value) {
            switch ($key) {
                case 'fullname':
                $browser->type('#name', $value);
                break;
                // case 'branch':
                // $browser->select('branch_id', $value);
                // break;
                case 'gender':
                $browser->select('#gender', $value);
                break;
                case 'dob':
                $browser->type('#dob', $value);
                break;
                case 'mobile_primary':
                $browser->type('#mobile_primary', $value);
                break;
                case 'mobile_alternate':
                $browser->type('#mobile_alternate', $value);
                break;
                case 'pan_no':
                $browser->type('#pan_no', $value);
                break;
                case 'ration_no':
                $browser->type('#ration_no', $value);
                break;
                case 'aadhaar_no':
                $browser->type('#aadhaar_no', $value);
                break;
                case 'address_present':
                $browser->type('#address_present', $value);
                break;
                case 'city_present':
                $browser->type('#city_present', $value);
                break;
                case 'district_present':
                $browser->type('#district_present', $value);
                break;
                case 'state_present':
                $browser->type('#state_present', $value);
                break;
                case 'pincode_present':
                $browser->type('#pincode_present', $value);
                break;
                case 'address_permanent':
                $browser->type('#address_permanent', $value);
                break;
                case 'city_permanent':
                $browser->type('#city_permanent', $value);
                break;
                case 'district_permanent':
                $browser->type('#district_permanent', $value);
                break;
                case 'state_permanent':
                $browser->type('#state_permanent', $value);
                break;
                case 'pincode_permanent':
                $browser->type('#pincode_permanent', $value);
                break;
                case 'source of income':
                $browser->type('#source_of_income', $value);
                break;
                case 'monthly income':
                $browser->type('#monthly_income', $value);
                break;
                case 'father name':
                $browser->type('#father_name', $value);
                break;
                case 'mother name':
                $browser->type('#mother_name', $value);
                break;
                case 'spouse name':
                $browser->type('#spouse_name', $value);
                break;
                default:
                // code...
                break;
            }
        }
    }

    public function assert_agent_created(Browser $browser, $agent_name)
    {
        $browser->assertSee($agent_name);    
    }

        public function assert_agent_created_details(Browser $browser, $agent_name, $agent_code, $agent_score_tab, $agent_scores)
    {
        $browser->assertsee($agent_name, $agent_code, $agent_score_tab, $agent_scores);
    }

    public function click_edit_agent_link(Browser $browser)
    {
        $browser->clickLink('Edit Agent Details');
    }

    public function assert_edited_agent(Browser $browser, $agent_name, $agent_code, $agent_score_tab, $agent_scores)
    {
        $browser->assertsee('Agent details saved.')
                ->assert_agent_created_details($agent_name, $agent_code, $agent_score_tab, $agent_scores);
    }

    public function assert_agent_code_before_creation(Browser $browser, $agent_code)
    {
        $browser->assertDontSee($agent_code);
    }

    public function edit_agent(Browser $browser, $name, $mobile_primary, $pan, $aadhar)
    {
        $browser->click_edit_agent_link()->fill_details_for_agent_creation($name, $mobile_primary, $pan, $aadhar)->press('Save');

    }



}