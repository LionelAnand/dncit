<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait employee
{
    public function create_new_employee(Browser $browser, $branch, $employee)
    {
        $browser->visit('/branches/'.$branch->id.'/employees/create')
                ->fill_details_of_employee([
                    'fullname' => $empname = $employee->firstName,
                    'employeeBranch' => '9',
                    'gender' => 'Female',
                    'dob' => $employee->date($format = 'd-m-Y', $max = '1-1-1995'),
                    'mobile_primary' => $employee->mobile,
                    'mobile_alternate' => '9656862636',
                    'pan_no' => 'PKULL0536K',
                    'ration_no' => $employee->isbn13,
                    'aadhaar_no' => $employee->ean13,
                    'address_present' => $employee->buildingNumber,
                    'city_present' => $employee->city,
                    'district_present' => $employee->state,
                    'state_present' => $employee->country,
                    'pincode_present' => $employee->postcode,
                    'address_permanent' => $employee->buildingNumber,
                    'city_permanent' => $employee->city,
                    'district_permanent' => $employee->state,
                    'state_permanent' => $employee->country,
                    'pincode_permanent' => $employee->postcode,
                    'source_of_income' => $employee->jobTitle,
                    'monthly_income' => rand(10000, 60000),
                    "father's_name" => $employee->lastName,
                    "mother's_name" => $employee->firstNameMale,
                    "spouse's_name" => $employee->firstNameFemale
                ])
                ->save()
                ->assert_to_see_employee_name($empname)
                ->assert_to_confirm('New employee created.')
                ->get_employee_id($employee)
                ->get_member_details($employee, 'employee');
    }

    public function edit_employee_details(Browser $browser, $employee, $edit_employee)
    {
        $browser->visit('/branches/'.$employee->branch_id.'/employees/'.$employee->id.'/edit')
                ->fill_details_of_employee([
                    'fullname' => $empname = $edit_employee->firstName,
                    'employeeBranch' => rand(1, 34),
                    'gender' => 'Female',
                    'dob' => $edit_employee->date($format = 'd-m-Y', $max = '1-1-1995'),
                    'mobile_primary' => $edit_employee->mobile,
                    'mobile_alternate' => '9656862636',
                    'pan_no' => 'PKULL0536K',
                    'ration_no' => $edit_employee->isbn13,
                    'aadhaar_no' => $edit_employee->ean13,
                    'address_present' => $edit_employee->buildingNumber,
                    'city_present' => $edit_employee->city,
                    'district_present' => $edit_employee->state,
                    'state_present' => $edit_employee->country,
                    'pincode_present' => $edit_employee->postcode,
                    'address_permanent' => $edit_employee->buildingNumber,
                    'city_permanent' => $edit_employee->city,
                    'district_permanent' => $edit_employee->state,
                    'state_permanent' => $edit_employee->country,
                    'pincode_permanent' => $edit_employee->postcode,
                    'source_of_income' => $edit_employee->jobTitle,
                    'monthly_income' => rand(10000, 60000),
                    "father's_name" => $edit_employee->lastName,
                    "mother's_name" => $edit_employee->firstNameMale,
                    "spouse's_name" => $edit_employee->firstNameFemale,
                ])
                ->save()
                ->assert_to_see_employee_name($empname);
    }

    public function get_employee_credentials(Browser $browser, $employee)
    {
        $last_name = $employee->father_name;
        $first_name = $browser->format_name($employee->name)->formatted_name;
        $first_name = explode(' ', $first_name);
        if (strlen($first_name[0]) < 4) {
            $first_name = $first_name[1]     ;
        } else {
            $first_name = $first_name[0];
        }
        $last_name = rand(1, 100000);
        $employee->id_name = $first_name.'.'.$last_name;
        $employee->id_email = $first_name.'.'.$last_name.'@dncchits.com';
    }

    public function search_and_go_to_employee(Browser $browser, $employee)
    {
        $browser->search_for($employee->mobile, $employee->name);
    }

    public function provision_useraccess(Browser $browser, $access, $employee, $role)
    {
        if ($access == 'Provision User Access') {
            $employee->username = $employee->name.'.'.$employee->firstNameMale;
            $employee->username = str_replace(' ', '', $employee->username);
            $browser->clickLink('Provision User Access')->type('main#main-container form #email_prefix', $employee->username);
            $employee->username = $employee->username.'@dncchits.com';
        } elseif ($access == 'Manage User Access') {
            $browser->clickLink('Manage User Access');
            $employee->username = $browser->value('main#main-container form #email');
        }

        $super_admin = '1';
        $organization_admin = '2';
        $group_manager = '3';
        $collection = '4';
        $group_admin = '5';
        $accountant = '6';

        if ($role == 'Super Admin') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$super_admin."]']")
        ->uncheck("#main-container form input[name='roles[".$organization_admin."]']")->uncheck("#main-container form input[name='roles[".$collection."]']")
        ->uncheck("#main-container form input[name='roles[".$group_manager."]']")->uncheck("#main-container form input[name='roles[".$group_admin."]']")
        ->uncheck("#main-container form input[name='roles[".$accountant."]']");
        }

        if ($role == 'Organization Admin') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$organization_admin."]']")
            ->uncheck("#main-container form input[name='roles[".$collection."]']")->uncheck("#main-container form input[name='roles[".$group_manager."]']")->uncheck("#main-container form input[name='roles[".$group_admin."]']")
            ->uncheck("#main-container form input[name='roles[".$accountant."]']");
        }

        if ($role == 'Collection') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$collection."]']")
            ->uncheck("#main-container form input[name='roles[".$group_manager."]']")->uncheck("#main-container form input[name='roles[".$group_admin."]']")
            ->uncheck("#main-container form input[name='roles[".$accountant."]']");
        }

        if ($role == 'Group Manager') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$group_manager."]']")
            ->uncheck("#main-container form input[name='roles[".$collection."]']")->uncheck("#main-container form input[name='roles[".$group_admin."]']")
            ->uncheck("#main-container form input[name='roles[".$accountant."]']");
        }

        if ($role == 'Group Admin') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$group_admin."]']")
            ->uncheck("#main-container form input[name='roles[".$collection."]']")->uncheck("#main-container form input[name='roles[".$group_manager."]']")
            ->uncheck("#main-container form input[name='roles[".$accountant."]']");
        }

        if ($role == 'Accountant') {
            $browser->type('main#main-container form #password', '123456')->check("#main-container form input[name='roles[".$accountant."]']")
            ->uncheck("#main-container form input[name='roles[".$collection."]']")->uncheck("#main-container form input[name='roles[".$group_manager."]']")
            ->uncheck("#main-container form input[name='roles[".$group_admin."]']");
        }

        if ($access == 'Provision User Access') {
            $browser->press('Provision')->assert_user_access_provisioned();
        } elseif ($access == 'Manage User Access') {
            $browser->press('Save')->assertSee('User details saved successfully.');
        }
    }

    public function assert_user_access_provisioned(Browser $browser)
    {
        $browser->assertSee('User provisioned successfully.');
    }

    public function remove_this_employee(Browser $browser, $employee)
    {
        $browser->visit('/branches/'.$employee->branch_id.'/employees');
        $name = '';
        $row = 0;
        $name !== $employee->name;
        while ($name !== $employee->name.' '.$employee->score) {
            $row = $row + 1;
            $name = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$row.') > td.font-w600');
        }
        $browser->click('#main-container > div.content div.block-content table > tbody > tr:nth-child('.$row.') > td:nth-child(6)')
                ->clickLink('Remove Employee')
                ->acceptDialog()
                ->assertSee('Employee removed successfully.');
    }

    public function remove_an_employee_with_managing_enrollments(Browser $browser, $branch)
    {
        $browser->visit('/branches/'.$branch->id.'/employees');
        $row = 0;
        $enrollments = 0;
        while ($enrollments == 0) {
            $row = $row + 1;
            $enrollments = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$row.') > td.text-right');
        }
        $browser->click('#main-container > div.content div.block-content table > tbody > tr:nth-child('.$row.') > td:nth-child(6)')
        ->clickLink('Remove Employee')
        ->assertDialogOpened('Are you sure you want to remove this employee?')
                ->acceptDialog()
                ->assertSee('This user is managing '.$enrollments.' active enrollments. Please transfer them before removing this user.');
    }
}
