<?php

namespace Tests\Browser\Pages\Traits;

use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;

trait commission
{
    public function get_penalty_percentage_for_prized(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'penalty_prized_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(6)');
    }

    public function get_penalty_percentage_for_non_prized_prized(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'penalty_prized_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(7)');
    }

    public function get_pending_days_for_penalty(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'penalty_policy_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(8)');
    }

    public function get_agent_commission_percentage(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'agent_commission_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(10)');
    }

    public function get_employee_commission_percentage(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'employee_commission_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(11)');
    }

    public function get_subscriber_commission_percentage(Browser $browser, $policy, $policy_number, $row)
    {
        $browser->{'subscriber_commission_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(12)');
    }

    public function get_commission_percentage(Browser $browser, $type, $policy, $policy_number, $row)
    {
        $browser->{$type.'_commission_'.$policy.'_'.$policy_number} = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$row.') > td:nth-child(12)');
    }

    public function format_commission(Browser $browser, $number)
    {
        $browser->formatted_number = str_replace('%', '', $number) * 1;
    }

    public function commission_percentage(Browser $browser, $type, $policy, $policy_number, $row)
    {
        if ($type == 'agent') {
            $browser->commission_percentage = $browser->get_agent_commission_percentage($policy, $policy_number, $row)->{$type.'_commission_'.$policy.'_'.$policy_number};

            $browser->commission_percentage = $browser->format_commission($browser->commission_percentage)->formatted_number;
        }

        if ($type == 'employee') {
            $browser->commission_percentage = $browser->get_employee_commission_percentage($policy, $policy_number, $row)->{$type.'_commission_'.$policy.'_'.$policy_number};

            $browser->commission_percentage = $browser->format_commission($browser->commission_percentage)->formatted_number;
        }

        if ($type == 'subscriber') {
            $browser->commission_percentage = $browser->get_subscriber_commission_percentage($policy, $policy_number, $row)->{$type.'_commission_'.$policy.'_'.$policy_number};

            $browser->commission_percentage = $browser->format_commission($browser->commission_percentage)->formatted_number;
        }
    }

    public function click_group_wise_from_introducer_commission_in_menu(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(2) > a');
    }

    public function click_introducer_commission_in_menu(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(12) > a');
    }

    public function choose_branch_in_group_wise_introducer_commission_report(Browser $browser, $branch)
    {
        $browser->select('#branch_id', $branch->id);
    }

    public function get_documentation_charge_from_erp(Browser $browser, $enrollment)
    {
        $browser->visit('/groups/2641/enrollments/'.$enrollment->id)->clickLink('Other Charges');
        $charges_array = collect($browser->driver->findElements(WebDriverBy::cssSelector('#other_charges_tab > div > div > table > tbody > tr > td:nth-child(1)')));
        $charges_array = $charges_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($charges_array->search('Documentation Charge'));
        $key = $key + 1;
        $documentation_charge_in_erp = $browser->text('#other_charges_tab > div > div > table > tbody > tr:nth-child('.$key.') > td.text-right');
        $enrollment->documentation_charge_in_erp = str_replace(',', '', $documentation_charge_in_erp);
    }

    public function get_calculated_documentation_charge(Browser $browser, $group)
    {
        $browser->visit('/policies');
        $policies_array = collect($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[1]')));
        $policies_array = $policies_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($policies_array->search($group->policy_name));
        $key = $key + 1;
        $documentation_charge_in_percentage = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$key.') > td:nth-child(17)');
        $group->documentation_charge_in_percentage = str_replace('%', '', $documentation_charge_in_percentage);
        $group->calculated_documentation_charge = $group->documentation_charge_in_percentage * $group->chit_value / 100;
    }

    public function get_registration_charge_from_erp(Browser $browser, $enrollment)
    {
        $browser->visit('/groups/2641/enrollments/'.$enrollment->id)->clickLink('Other Charges');
        $charges_array = collect($browser->driver->findElements(WebDriverBy::cssSelector('#other_charges_tab > div > div > table > tbody > tr > td:nth-child(1)')));
        $charges_array = $charges_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($charges_array->search('Registration Charge'));
        $key = $key + 1;
        $registration_charge_in_erp = $browser->text('#other_charges_tab > div > div > table > tbody > tr:nth-child('.$key.') > td.text-right');
        $enrollment->registration_charge_in_erp = str_replace(',', '', $registration_charge_in_erp);
    }

    public function get_calculated_registration_charge(Browser $browser, $group)
    {
        $browser->visit('/policies');
        $policies_array = collect($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[1]')));
        $policies_array = $policies_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($policies_array->search($group->policy_name));
        $key = $key + 1;
        $registration_charge_in_percentage = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$key.') > td:nth-child(16)');
        $group->registration_charge_in_percentage = str_replace('%', '', $registration_charge_in_percentage);
        $group->calculated_registration_charge = $group->registration_charge_in_percentage * $group->chit_value / 100;
    }

 
}
