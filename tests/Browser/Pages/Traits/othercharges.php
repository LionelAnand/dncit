<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait othercharges
{

    public function add_manual_charges_in_other_charges_tab(Browser $browser, $manual_charges)
    {
      $browser->go_to_other_charges_tab_in_enrollment_ledger()->clickLink('Add Charge')
      ->type('#page-container main#main-container input[name="charges[Other Charges]"]', $manual_charges)
      ->press('Add Charges')->assertSee('Charges added successfully.');
    }

    public function assert_total_in_other_charges_tab(Browser $browser, $other_charges)
    {
        $browser->go_to_other_charges_tab_in_enrollment_ledger()
                ->assertSeeIn('#page-container main#main-container #other_charges_tab table.table tbody tr.table-info > td.text-right', $other_charges);
    }


public function get_number_of_other_charge_receipts(Browser $browser, $receipt)
{
    $receipt->count = $browser->elements('#page-container main#main-container #other_charges_tab table.table tbody tr td:nth-child(8) small a');

    $receipt->count = count($receipt->count);
            
    }

    public function delete_other_charges_receipts(Browser $browser, $receipt)
    {
        $browser->get_number_of_other_charge_receipts($receipt);
        while ($receipt->count != 0) {
            $browser->click('#page-container main#main-container #other_charges_tab table.table tbody tr:nth-child(1) td:nth-child(8) small a')
            ->clickLink('Delete')->go_to_other_charges_tab_in_enrollment_ledger()->get_number_of_other_charge_receipts($receipt);
        }
    }

    public function add_registration_charge_in_other_charges_tab(Browser $browser)
    {
      $browser->go_to_other_charges_tab_in_enrollment_ledger()->clickLink('Add Charge')
      ->check('#page-container main#main-container input[name="charges[Registration Charge]"]')
      ->press('Add Charges')->assertSee('Charges added successfully.');
    }

    public function add_cancellation_charge_in_other_charges_tab(Browser $browser)
    {
      $browser->go_to_other_charges_tab_in_enrollment_ledger()->clickLink('Add Charge')
      ->check('#page-container main#main-container input[name="charges[Cancellation Charge]"]')
      ->press('Add Charges')->assertSee('Charges added successfully.');
    }

    public function get_cancellation_charge(Browser $browser, $charges, $row) 
    {

        $charges->cancellation = $browser->text('main#main-container table.table tbody tr:nth-child('.$row.') > td:nth-child(18)');
        $charges->cancellation = $browser->format_number($charges->cancellation)->format_number;
        $charges->cancellation = $charges->cancellation / 100;

    }

    public function get_number_of_not_paid_other_charges(Browser $browser, $charges)
    {
        $charges->count = $browser->elements('#other_charges_tab table.table tbody > tr > td:nth-child(9) form button');
        $charges->count = count($charges->count);
    }

    /*** On next iteration */
    public function remove_other_charges_receipts(Browser $browser, $charges, $enrollment)
    {
        $browser->go_to_other_charges_tab_in_enrollment_ledger()->get_number_of_not_paid_other_charges($charges);

                $browser->click('#page-container main#main-container #other_charges_tab table.table tbody > tr:nth-child(2) > td:nth-child(9) form button')
                ->assert_other_charges_removed();
    }
    /*** On next iteration */

        public function assert_other_charges_removed(Browser $browser)
        {
            $browser->assertSee('Charge removed successfully.');
        }

    
}

