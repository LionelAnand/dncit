<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait collection {


    public function delete_the_collection_receipt(Browser $browser)
    {
        $browser->clickLink('Delete');
    }

    public function delete_the_receipt_through_enrollment_ledger(Browser $browser, $receipt)
    {

$browser->clickLink($receipt->subscriber)->clickLink($receipt->number)->delete_the_collection_receipt();
    }

    public function click_add_collection(Browser $browser)
    {
        $browser->clickLink('Ledger')->clickLink('Add Collection');
    }

    public function enter_receipt_details_in_receipt_entry_form(Browser $browser, $amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $processed_date, $processed_time, $realized_date, $realized_time, $notes)
    {

            $browser->fill_receipt_details_in_receipt_entry_form([
                'amount'            => $amount,
                'receipt date'      =>  $date,
                'receipt time'      => '1200',
                'mode of payment'   => $payment_mode,
                'reference number'  => $reference_number,
                'bank'              => $bank_name,
                'branch'            => $branch_name,
                'processed date'    => $processed_date,
                'processed time'    => $processed_time,
                'realized date'     => $realized_date,
                'realized time'     => $realized_time,
                'notes'             => $notes,
            ]);

    }

    public function fill_receipt_details_in_receipt_entry_form(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
                case 'amount':
                $browser->type('#amount', $value);
                break;
                case 'receipt date':
                $browser->type('#receipt_date', $value);
                break;
                case 'receipt time':
                $browser->type('#receipt_time', $value);
                break;
                case 'mode of payment':
                $browser->select('#type', $value);
                break;
                case 'reference number':
                $browser->type('#reference_no', $value);
                break;
                case 'bank':
                $browser->type('#bank_name', $value);
                break;
                case 'branch':
                $browser->type('#branch_name', $value);
                break;
                case 'processed date':
                $browser->type('#processed_date', $value);
                break;
                case 'processed time':
                $browser->type('#processed_time', $value);
                break;
                case 'realized date':
                $browser->type('#realized_date', $value);
                break;
                case 'realized time':
                $browser->type('#realized_time', $value);
                break;
                case 'notes':
                $browser->type('#notes', $value);
                break;
            }
        }
    }


public function get_details_in_receipt(Browser $browser, $receipt)
    {
        $receipt->type = $browser->text('main#main-container > div:nth-child(1) div.col-8 > h3');

        $receipt->number = $browser->text('#main-container > div:nth-child(1) > div > div > div.col-8 > h1');

        $receipt->subscriber = $browser->text('#collection-details > div > div > div > div:nth-child(1) > label:nth-child(2) > a');

        $receipt->branch = $browser->text('#collection-details > div > div > div > div:nth-child(1) > label:nth-child(5)');

        $receipt->group = $browser->text('#collection-details > div > div > div > div:nth-child(2) > label:nth-child(2)');

        $receipt->ticket = $browser->text('#collection-details > div > div > div > div:nth-child(2) > label:nth-child(4)');

        $receipt->amount = $browser->text('#collection-details > div > div > div > div:nth-child(4) > div > div > label');

        $receipt->receipt_time = $browser->text('#collection-details > div > div > div > div:nth-child(4) > label.col-4.col-form-label');

        $receipt->payment_mode = $browser->text('#collection-details > div > div > div > div:nth-child(6) > label:nth-child(2)');

        $receipt->reference_number = $browser->text('#collection-details > div > div > div > div:nth-child(6) > label:nth-child(4)');

        $receipt->bank = $browser->text('#collection-details > div > div > div > div:nth-child(7) > label:nth-child(2)');

        $receipt->branch = $browser->text('#collection-details > div > div > div > div:nth-child(7) > label:nth-child(4)');

        $receipt->processed_time = $browser->text('#collection-details > div > div > div > div:nth-child(8) > label:nth-child(2)');

        $receipt->realized_time = $browser->text('#collection-details > div > div > div > div:nth-child(8) > label:nth-child(4)');

        $receipt->notes = $browser->text('#collection-details > div > div > div > div:nth-child(9) > label.col-10.col-form-label');

        $receipt->collected_by = $browser->text('#collection-details > div > div > div > div:nth-child(11) > label.col-4.col-form-label > a');

        try 
        {
            $browser->assertSee('Alternate receipt numbers:');
            $receipt->alternate_number =   $browser->get_alternate_receipt_number($receipt);
        } 
        catch (\Exception $e) 
        {
        //    $browser->assertSee('Refund Receipt');
        }
        
        

    }


    public function click_change_log_tab_in_collection_receipt(Browser $browser)
    {
        $browser->click('#main-container > div:nth-child(2) > div:nth-child(1) > div > div > ul > li:nth-child(2) > a');
    }

    public function assert_details_in_change_log(Browser $browser, $receipt)
    {
        $receipt->amount = $browser->format_amount($receipt->amount)->format_amount;

        $browser->assertSee('Time', 'User', 'Type', 'Changes')
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(1) > small > span > b', $receipt->amount)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(2) > small > span > b', $receipt->payment_mode)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(4) > small > span > b', 'App\Employee')
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(5) > small > span > b', $receipt->reference_number)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(6) > small > span > b', $receipt->bank)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(7) > small > span > b', $receipt->branch)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(8) > small > span > b', $receipt->notes)
                // ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(9) > small > span > b', $transaction_time)
                // ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(10) > small > span > b', $processed_time)
                // ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(4) > ul > li:nth-child(11) > small > span > b', $realized_time)
        ->assertSeeIn('#change-log > div > div > table > tbody > tr:nth-child(1) > td:nth-child(2)', $receipt->collected_by);
    }

    public function go_to_collection_details_in_receipts_page($browser)
    {
        $browser->clickLink('Collection Details');
    }

public function refund_collection_for_removed_enrollment(Browser $browser, $amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $processed_date, $processed_time, $realized_date, $realized_time, $notes)
    {
        $browser->go_to_ledgers_tab_in_enrollment_ledger()
        ->click('#ledger_tab > div.text-right.d-print-none > div > a')
        ->enter_receipt_details_in_receipt_entry_form($amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $processed_date, $processed_time, $realized_date, $realized_time, $notes);
    }

    public function remove_other_charges(Browser $browser)
    {
        $browser->click('#page-container main#main-container #other_charges_tab table.table tbody tr:nth-child(2) td:nth-child(9) form button');
    }

    public function get_count_of_other_charges(Browser $browser, $enrollment){
        $browser->clicklink('Other Charges');
        $el = $browser->elements('#page-container main#main-container #other_charges_tab table.table tbody tr td:nth-child(9) form button');
        $enrollment->charges_count = count($el);
    }

 public function assert_and_remove_other_charges(Browser $browser, $enrollment)
    {
        $browser->get_count_of_other_charges($enrollment);
        while ($enrollment->charges_count > 0) {
            $browser->remove_other_charges()
                    ->assert_other_charges_removed()
                    ->get_count_of_other_charges($enrollment);
        }
    }

    public function assert_that_refund_receipt_is_saved(Browser $browser)
    {
        $browser->assertSee('Refund receipt saved.');
    }

public function go_to_add_collection_page(Browser $browser, $enrollment)
{
    $browser->visit('/enrollments/'.$enrollment->id.'/collections/create');
}
    public function print_receipt(Browser $browser)
    {
      $browser->click('#main-container > div:nth-child(2) > div:nth-child(1) > div > div > ul > li.nav-item.ml-auto > div > a:nth-child(2)');
    }

    public function assert_back_date_entry_restricted(Browser $browser)
    {
        $browser->assertSee('Back dated entries are not permitted.');
    }

    public function assert_additional_collection_in_enrollment_ledger(Browser $browser)
    {
        $browser->assertSeeIn('#page-container main#main-container  table.table tbody', 'Additional Collection');
    }
    
    public function enter_receipt_details_in_receipt_entry_form_collection_role(Browser $browser, $amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $notes)
    {

            $browser->fill_receipt_details_in_receipt_entry_form_collection_role([
                'amount'            => $amount,
                'receipt date'      =>  $date,
                'receipt time'      => '1200',
                'mode of payment'   => $payment_mode,
                'reference number'  => $reference_number,
                'bank'              => $bank_name,
                'branch'            => $branch_name,
                'notes'             => $notes,
            ]);

    }

    public function fill_receipt_details_in_receipt_entry_form_collection_role(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
                case 'amount':
                $browser->type('#amount', $value);
                break;
                case 'receipt date':
                $browser->type('#receipt_date', $value);
                break;
                case 'receipt time':
                $browser->type('#receipt_time', $value);
                break;
                case 'mode of payment':
                $browser->select('#type', $value);
                break;
                case 'reference number':
                $browser->type('#reference_no', $value);
                break;
                case 'bank':
                $browser->type('#bank_name', $value);
                break;
                case 'branch':
                $browser->type('#branch_name', $value);
                break;
            }
        }
    }

    /*** Temporary function to remove both other charges - Will be changed in next iteration */
    public function assert_and_remove_other_charges_one(Browser $browser, $enrollment)
    {
        $browser->get_count_of_other_charges($enrollment);
        while ($enrollment->charges_count > 0) {
            $browser->click('#page-container main#main-container #other_charges_tab table.table tbody tr:nth-child(1) td:nth-child(9) form button')
                    ->assert_other_charges_removed()
                    ->get_count_of_other_charges($enrollment);
        }
    }
    /*** Temporary function to remove both other charges - Will be changed in next iteration */

    public function get_alternate_receipt_number(Browser $browser, $receipt)
    {
        $receipt->alternate = $browser->text('#page-container main#main-container > div:nth-child(1) div.col-8 > small');

        $receipt->alternate = explode(' ', $receipt->alternate);

        $receipt->alternate = $receipt->alternate[3];
        }

        /**** Will be on next iteration */
        public function remove_other_charges_one(Browser $browser, $enrollment)
            {
                $browser->go_to_enrollment_with_id($enrollment)->clickLink('Other Charges');
        
                $el = $browser->elements('tr > td:nth-child(9) > form > button > i');
         
                $count = count($el);
                if ($count > 0) 
                {
                    $browser->click('#other_charges_tab > div > div > table > tbody > tr:nth-child('.$count.') > td:nth-child(9) > form > button > i')
                        ->assert_charges_removed();
                }
        /**** Will be on next iteration */

    }

}