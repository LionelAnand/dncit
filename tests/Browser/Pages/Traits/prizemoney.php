<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait prizemoney
{
    public function add_document(Browser $browser, $type)
    {
        $browser->clicklink('Add Document')
                    ->upload_a_document('/Pictures/meow.jpg')
                    ->fill_details_for_document_upload([
                        'name' => 'test PNG',
                        'type' => $type,
                        'description' => 'testing Upload',
                    ])
                    ->save()
                    ->confirm_document_saved();
    }

    public function delete_disbursed_prized_money(Browser $browser)
    {
        $browser->go_to_enrollment_with_id('53294')
                ->clicklink('Prize Money');
        $count = $browser->elements('#prize_money_tab > div:nth-child(2)  table > tbody > tr > td:nth-child(8) > form > button > i');

        $count = count($count);
        while ($count > 0) {
            $browser->go_to_enrollment_with_id('53294')
                    ->clickLink('Prize Money')
                    ->remove_disbursement();
        }
    }

    public function disburse_prize_money(Browser $browser, $enrollment)
    {
        $date = date('d-m-Y');
        $reference_no = rand(1, 9999999);
        $time = date('h:i');
        $browser->go_to_enrollment_with_id($enrollment)
                ->clickLink('Prize Money')
                ->click_disburse_prized_money();
        if ($enrollment->deduction = 1) {
            $browser->check('#tab-disburse-to-subscriber > form:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)');
        } elseif ($enrollment->deduction = 0) {
            $browser->uncheck('#tab-disburse-to-subscriber > form:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)');
        }
        $browser->fill_in_the_details(
            [
                'prize_money' => $enrollment->disbursement,
                'disbursementDate' => $date,
                'disbursementTime' => $time,
                'type' => 'Cheque',
                'referenc_no' => $reference_no,
                'bank_name' => 'KVB',
                'branch_name' => $enrollment->subscriber_branch,
                'processed_date' => $date,
                'realized_date' => $date,
                'notes' => 'For Testing Purpose'
            ]
        )
            ->press('Disburse')
            ->assertSee('disbursed');
    }

public function disburse_prize_money_without_deduction(Browser $browser, $enrollment)
    {
        $date = date('d-m-Y');
        $reference_no = rand(1, 9999999);
        $time = date('h:i');
        $browser->go_to_enrollment_with_id($enrollment)
                ->clickLink('Prize Money')
                ->click_disburse_prized_money();
        if ($enrollment->deduction = 1) {
            $browser->uncheck('#tab-disburse-to-subscriber > form:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)');
        } elseif ($enrollment->deduction = 0) {
            $browser->check('#tab-disburse-to-subscriber > form:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > input:nth-child(1)');
        }
        $browser->fill_in_the_details(
            [
                'prize_money' => $enrollment->disbursement,
                'disbursementDate' => $date,
                'disbursementTime' => $time,
                'type' => 'Cheque',
                'referenc_no' => $reference_no,
                'bank_name' => 'KVB',
                'branch_name' => $enrollment->subscriber_branch,
                'processed_date' => $date,
                'realized_date' => $date,
                'notes' => 'For Testing Purpose'
            ]
        )
            ->press('Disburse')
            ->assertSee('disbursed');
    }
    public function confirm_documents_requirement_for_disbursement(Browser $browser)
    {
        $browser->assertSee('Submission of security documents is mandatory to disburse prize money.');
    }

    public function confirm_disburse_prize_money_button_is_disabled(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
                ->clickLink('Prize Money')
                ->mouseover('#prize_money_tab > div.row.mb-2 div.col-md-6.text-right > a')
                ->assertSee('Submission of security documents is mandatory to disburse prize money.');
    }

    public function confirm_disburse_prize_money_button_enabled(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
                ->clickLink('Prize Money');
    }
    public function remove_disbursed_prize_money(Browser $browser, $enrollment)
    {
        $browser->get_disbursement_count($enrollment);
        while ($enrollment->disbursement_count > 0) {
            $browser->remove_disbursement()
                    ->get_disbursement_count($enrollment);
        }
    }

    public function get_prize_money_details(Browser $browser, $group, $enrollment)
    {
        $browser->get_upm($enrollment);
        $enrollment->commission = $group->value * 5 / 100;
        $enrollment->gst = $enrollment->commission * 12 / 100;
        $enrollment->disbursable = $group->value - $enrollment->bid_amount - $enrollment->commission - $enrollment->gst;
    }

    public function get_upm(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)->clickLink('Prize Money');
        $enrollment->upm_text = $browser->text('#prize_money_tab > div.row.mb-2 > div > table > tbody > tr.table-warning > td.text-right');
        $enrollment->upm = str_replace(',', '', $enrollment->upm_text);
    }

    public function get_bid_amount_with_percentage(Browser $browser, $group, $enrollment)
    {
        $enrollment->bid_amount = $group->value * $enrollment->bid_percentage / 100;
    }

    public function get_disbursement_count(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)->clicklink('Prize Money');
        $el = $browser->elements('tr:nth-child(n) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)');
        $enrollment->disbursement_count = count($el);
    }

    public function get_deductable_tbc_after_auction(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
                ->clickLink('Prize Money')
                ->click_disburse_prized_money();         
        $enrollment->deductable_tbc_text = $browser->value('#main-container div:nth-child(4) > input');
        $enrollment->deductable_tbc = str_replace(',', '', $enrollment->deductable_tbc_text);
       
    }

    
}
