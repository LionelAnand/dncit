<?php

namespace Tests\Browser\Pages\Traits;
use Facebook\WebDriver\WebDriverBy;

use Laravel\Dusk\Browser;

trait penalty
{

    public function penalty_percentage(Browser $browser, $penalty, $type, $policy, $row) 
    {

        if ($type == 'non_prized') 
        {
        $penalty->non_prized_percentage = $browser->get_non_prized_percentage($policy, $row)->{'non_prized_penalty_'.$policy};
        $penalty->non_prized_percentage = $browser->format_amount($penalty->non_prized_percentage)->format_amount;
        $penalty->percentage = $penalty->non_prized_percentage;
        }

        else
        {
        $penalty->prized_percentage = $browser->get_prized_percentage($policy, $row)->{'prized_penalty_'.$policy};
        $penalty->prized_percentage = $browser->format_amount($penalty->prized_percentage)->format_amount;
        $penalty->percentage = $penalty->prized_percentage;
        }

}

public function get_non_prized_percentage(Browser $browser, $policy, $row)
    {
        $browser->{'non_prized_penalty_'.$policy} = $browser->text('#page-container main#main-container table.table tbody tr:nth-child('.$row.') td:nth-child(7)');
    }

public function get_prized_percentage(Browser $browser, $policy, $row)
    {
        $browser->{'prized_penalty_'.$policy} = $browser->text('#page-container main#main-container table.table tbody tr:nth-child('.$row.') td:nth-child(6)');
    }


    public function get_details_for_penalty_calculations_in_enrollment_ledger(Browser $browser, $penalty)
    {
        $penalty->payment_days = $browser->text('#page-container #settlements-table tbody tr:nth-child(2) td:nth-child(5)');
        
        $penalty->installment_amount = $browser->text('#page-container #settlements-table tbody tr:nth-child(2) td:nth-child(2)');
        
        $penalty->installment_amount = $browser->format_amount($penalty->installment_amount)->format_amount;

        $penalty->penalty_amount = $browser->text('#page-container #settlements-table tbody tr:nth-child(2) td:nth-child(4)');


    }

    public function penalty_calculations(Browser $browser, $penalty)
    {       
    $penalty->calculation = (($penalty->payment_days * $penalty->percentage * $penalty->installment_amount) / 30);
    $penalty->calculation = floor($penalty->calculation);
    }

    

}