<?php

namespace Tests\Browser\Pages\Traits;

use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;

trait common
{
    public function get_branch_details(Browser $browser, $branch)
    {
        $branch->name = $browser->go_to_branches_page_from_menu()
                ->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$branch->number.') > td.text-left > a');
        $browser->click_branch_name_in_branches_page($branch->name);
        $branch->url = $browser->get_url()->url;
        $branch->id = $branch->url = explode('/', $branch->url);
        $branch->id = $branch->url[4];
    }

    public function sign_in(Browser $browser)
    {
        $browser->visit('/')
                ->type('#email', 'prabu.kannan@dncchits.com')
                ->type('#password', '9001487')
                ->press('Login');
    }

    public function get_branch_name(Browser $browser)
    {
        $browser->go_to_branches_page_from_menu();

        $browser->number_of_branches = $browser->number_of_branches_in_branches_page()->random;

        $browser->name = $browser->text('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$browser->number_of_branches.') > td.text-left > a');
    }

    public function get_enrollment_name(Browser $browser)
    {
        $browser->number_of_enrollments = $browser->elements('#DataTables_Table_0 > tbody > tr > td:nth-child(2) > a');

        $browser->number_of_enrollments = count($browser->number_of_enrollments);

        $browser->number_of_enrollments = $browser->random_function($browser->number_of_enrollments)->random;

        $browser->name = $browser->text('#DataTables_Table_0 > tbody > tr:nth-child('.$browser->number_of_enrollments.') > td:nth-child(2) > a');

        $enrollment_name = $browser->name = $browser->remove_score_from_name($browser->name)->formatted_name;
    }

    public function get_todays_date(Browser $browser)
    {
        $browser->date = date('d-m-Y');
    }

    public function get_branch_code(Browser $browser, $number)
    {
        $browser->branch_code = $browser->text('#main-container > div.content td:nth-child('.$number.') > > td:nth-child(2)');
    }

    public function go_to_group(Browser $browser, $group_name)
    {
        $browser->search_for($group_name, $group_name);
    }

    public function save(Browser $browser)
    {
        $browser->press('Save');
    }

    public function go_to_enrollment(Browser $browser, $group_name, $subscriber_name)
    {
        $browser->go_to_group($group_name)
        ->clickLink($subscriber_name);
    }

    public function go_to_subscriber_enrollment_ledger($browser, $subscriber_ledger)
    {
        $browser->clickLink($subscriber_ledger);
    }

    public function search_for(Browser $browser, $keyword, $target)
    {
        $browser->type('#search-autocomplete', $keyword)
        ->waitForLink($target, 60)
        ->clickLink($target);
    }

    public function go_to_employee_ledger(Browser $browser, $employee_name)
    {
        $browser->type('#search-autocomplete', $employee_name)
        ->waitForLink($employee_name, 60)
        ->clickLink($employee_name);
    }

    /*************************** 1 - GLOBAL SEARCH FUNCTIONS ***************************/

    /********************** 2 - Functions in Enrollment Ledger ********************/

    /******** 2.1 Ledger Tab - Enrollment Ledgers ********/

    public function go_to_ledgers_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(1) > a');
    }

    public function Regenerate_settlements_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#dropdown-default-outline-primary')->clickLink('Regenerate Settlements');
    }

    /******** 2.1 Ledger Tab - Enrollment Ledger ********/

    /******** 2.2 Prize Money Tab - Enrollment Ledger ********/
    public function go_to_prize_money_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(2) > a');
    }

    /******** 2.2 Prize Money Tab - Enrollment Ledger ********/

    /******** 2.3 Other Charges Tab - Enrollment Ledger ********/

    public function go_to_other_charges_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(3) > a');
    }

    public function click_add_charges_in_other_charges_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->clicklink('Add Charge');
    }

    public function enter_other_charges_amount(Browser $browser, $amount)
    {
        $browser->type('charges amount', $amount);
    }

    /******** 2.3 Other Charges Tab - Enrollment Ledgers ********/

    /******** 2.4 Guarantees Tab - Enrollment Ledger ********/

    public function go_to_guarantees_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->clickLink('Guarantees');
    }

    /******** 2.4 Guarantees Tab - Enrollment Ledger ********/

    /******** 2.5 Documents Tab - Enrollment Ledger ********/

    public function go_to_documents_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#page-container main#main-container div.content div.block ul.nav-tabs li.nav-item a[href="#documents_tab"]');
    }

    /******** 2.5 Documents Tab - Enrollment Ledger ********/

    /******** 2.6 Introducer Commission Tab - Enrollment Ledger ********/

    public function go_to_introducer_commission_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(6) > a');
    }

    public function click_disburse_introducer_commission_in_introducer_commission_tab(Browser $browser)
    {
        $browser->click('#introducer_commission_tab > div > div:nth-child(1) > div > div.col-md-6.text-right > a');
    }

    /******** 2.6 Introducer Commission Tab - Enrollment Ledger ********/

    /******** 2.7 Interactions Tab - Enrollment Ledger ********/

    public function go_to_interactions_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(7) > a');
    }

    /******** 2.7 Interactions Tab - Enrollment Ledger ********/

    /******** 2.8 Statement Tab - Enrollment Ledger ********/

    public function go_to_statement_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(8) > a');
    }

    /******** 2.8 Statement Tab - Enrollment Ledger ********/

    /******** 2.9 Notification Tab - Enrollment Ledger ********/
    public function go_to_notification_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(9) > a');
    }

    /******** 2.9 Notification Tab - Enrollment Ledger ********/

    /******** 2.10 Others - Enrollment Ledger ********/

    public function click_subscriber_to_go_to_enrollment_ledger(Browser $browser, $subscriber_name)
    {
        $browser->clickLink($subscriber_name);
    }

    public function click_edit_enrollment_details_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#main-container > div.bg-primary-dark > div > div > div.col-2.text-right.d-print-none > a');
    }

    /******** 2.10 Others - Enrollment Ledger ********/

    /********************** 2 - Functions in Enrollment Ledgers *********************/

    /********************** 3 - Functions in Group Ledgers *********************/

    public function go_to_enrollment_page_in_group_ledger(Browser $browser)
    {
        $browser->click('Enrollments');
    }

    public function go_to_group_from_subscriber_ledger(Browser $browser, $group_name)
    {
        $browser->clickLink($group_name);
    }

    public function go_to_group_details(Browser $browser, $group_name)
    {
        $browser->search_for($group_name, $group_name)
        ->clickLink('Group Details');
    }

    public function go_to_group_details_in_group_ledger(Browser $browser)
    {
        $browser->clickLink('Group Details');
    }

    public function go_to_auctions_tab_in_group_ledger(Browser $browser)
    {
        $browser->click('#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a');  //click on Auction
    }

    public function go_to_enrollment_from_enrollments_page(Browser $browser, $enrollment)
    {
        $browser->clickLink($enrollment);
    }

    /********************** 3 - Functions in / to Group Ledgers *********************/

    /********************** 4 - Employee / Subscriber / Agent Ledger *********************/

    /**************** 4.1 Employee Ledger ****************/

    public function go_to_todays_collection_details_tab_in_employee_ledger(Browser $browser)
    {
        $browser->click('#employee_tabs > li:nth-child(4) > a');
    }

    public function go_to_todays_collection(Browser $browser)
    {
        $browser->click('#page-container main#main-container div.block ul.nav div.btn-group a');
    }

    public function go_to_details_tab(Browser $browser)
    {
        $browser->clickLink('Details');
    }

    /**************** 4.1 Employee Ledger ****************/

    /**************** 4.2 Agent Ledger ****************/

    /**************** 4.2 Agent Ledger ****************/

    /**************** 4.3 Subscriber Ledger ****************/

    /**************** 4.3 Subscriber Ledger ****************/

    /*********************** 4 - Employee / Subscriber / Agent Profile *********************/

    /************************** 5 - Menu *************************/

    public function go_to_policy_page(Browser $browser)
    {
        $browser->clickLink('Masters')
        ->clickLink('Policies')
        ->assertSee('Policies');
    }

    public function go_to_dashboard(Browser $browser)
    {
        $url = env('APP_URL');
        $browser->visit($url);
    }

    public function go_to_holidays_page(Browser $browser)
    {
        $browser->clickLink('Masters')->clickLink('Holidays');
    }

    public function go_to_branches_page_from_menu(Browser $browser)
    {
        $browser->clickLink('Branches');
    }

    public function click_introducer_wise_from_introducer_commission_in_menu(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a');
    }

    public function click_go_to_removed_enrollments_report(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content div.content-side.content-side-full > ul > li.nav-main-item.open > a > span');
    }

    public function click_branch_wise_from_introducer_commission_in_menu(Browser $browser)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(1) > a');
    }

    /************************** 5 - Menu *************************/

    /********************************* 6 - Reports ********************************/

    /**************** 6.1 - To Be Collected Report ****************/

    /**************** 6.1 - To Be Collected Report ****************/

    /**************** 6.2 - Collection Report ****************/

    public function go_to_datewise_collection_report(Browser $browser, $forbranch_id, $bybranch_id, $from_date, $to_date, $employee_id, $payment_type)
    {
        $browser->clickLink('Collections')->clickLink('Date Wise')
        ->select('#for_branch_id', $forbranch_id)->select('#by_branch_id', $bybranch_id)
        ->type('#from_date', $from_date)->type('#to_date', $to_date)->select('main#main-container #transactor_id', $employee_id)
        ->select('#payment_type', $payment_type)->press('Generate');
    }

    public function go_to_additional_collections_report(Browser $browser, $branch_id)
    {
        $browser->clickLink('Collections')->clickLink('Additional Collections')->assertSee('Collections', 'Additional', 'Subscriber', 'Group', 'Balance Amount')
        ->select('#branch_id', $branch_id)->press('Generate');
    }

    public function fill_details_for_generating_introducer_wise_commission_report(Browser $browser, $commission_details)
    {
        foreach ($commission_details as $key => $value) {
            switch ($key) {
                case 'Branch':
                $browser->select('#branch_id', $value);
                break;
                case 'IntroducerType':
                $browser->select('#introducer_type', $value);
                break;
                case 'FromDate':
                $browser->type('#from_date', $value);
                break;
                case 'ToDate':
                $browser->type('#to_date', $value);
                break;
                default:

                break;
            }
        }
    }

    public function export_the_report(Browser $browser)
    {
        $browser->assertsee('Export')->clickLink('Export');
    }

    public function click_generate_in_reports(Browser $browser)
    {
        $browser->press('Generate');
    }

    /**************** 6.12 - Others - Report ****************/

    /********************************* 6 - Reports ********************************/

    /********************************* 7 - Common Assertions ********************************/

    /**************** 7.1 - Receipt - Common Assertions ****************/

    public function assert_additional_collection(Browser $browser, $subscriber_name, $group_name, $balance_amount)
    {
        $browser->assertSee('Collection', 'Additional')
        ->pause(10000)
        ->assertSeeLink($subscriber_name)
        ->assertSee($group_name, $balance_amount);
    }

    public function assert_that_collection_receipt_is_saved(Browser $browser)
    {
        $browser->assertSee('Collection receipt saved.');
    }

    public function assert_notification_in_ledger_after_receipt_entry(Browser $browser, $notification_message)
    {
        $browser
        ->assertSee('NOTIFICATIONS', 'DATE', 'MESSAGE')
        ->assertSee($notification_message);
    }

    public function assert_that_all_charges_are_unpaid(Browser $browser)
    {
        $browser->assertSee('Not Paid');
    }

    public function assert_that_all_other_charges_are_paid(Browser $browser)
    {
        $browser->assertSee('Paid');
    }

    public function assert_registration_charge_paid(Browser $browser)
    {
        //   $browser->assertSeeIn('Registration Charge Type', 'Registration Charge')
        $browser->assertSee('CHG-0000');
    }

    public function assert_no_other_charges(Browser $browser)
    {
        $browser->assertSee('No other charges');
    }

    public function assert_registration_charge(Browser $browser)
    {
        $browser->assertSee('Registration Charge');
    }

    public function assert_receipt_number(Browser $browser, $receipt)
    {
        $browser->assertSee($receipt->number);
    }

    public function assert_charges_removed(Browser $browser)
    {
        $browser->assertSee('Charge removed successfully.');
    }

    public function assert_cancellation_charge(Browser $browser)
    {
        $browser->assertSee('Cancellation Charge');
    }

    public function assert_cancellation_charges_not_paid_in_enrollment_ledger(Browser $browser)
    {
        $browser->assertSee('Cancellation Charge', 'not paid');
    }

    public function assert_cancellation_charges_paid_in_enrollment_ledger(Browser $browser)
    {
        $browser->assertSee('Cancellation Charge', 'paid');
        //  ->assertSeeIn('Cancellation Charge Receipt Number', 'CHG');
    }

    /***Need to be in auction page when we link pages correctly****/
    public function assert_document_charge(Browser $browser)
    {
        $browser->assertSee('Documentation Charge');
    }

    /***Need to be in auction page when we link pages correctly****/

    public function assert_charges_added(Browser $browser)
    {
        $browser->assertSee('Charges added successfully.');
    }

    public function assert_manual_other_charges_not_paid(Browser $browser)
    {
        $browser->assertSee('Other Charges')
        ->assertSee('Not Paid');
    }

    public function assert_manual_other_charges_paid(Browser $browser)
    {
        $browser //->assertSeeIn('other charges table', 'Other Charges')
        ->assertSee('CHG-000')
        ->assertSee('Paid');
    }

    public function assert_other_charges_in_not_paid_status(Browser $browser)
    {
        $browser->assertSeeIn('other charges table', 'Not Paid');
    }

    public function assert_other_charges_in_paid_status(Browser $browser)
    {
        $browser->assertSeeIn('other charges table', 'Paid');
    }

    public function assert_collection_and_other_charge_in_same_receipt(Browser $browser, $enrollment_amount, $other_charges_amount)
    {
        $browser->assertSeeIn('Amount Field In Receipt', 'Enrollment: '.$enrollment_amount.' + Other Charges: '.$other_charges_amount);
    }

    public function assert_alert_for_other_charges_in_add_collection(Browser $browser, $other_charges_amount)
    {
        $browser->assertSee('From the received amount, '.$other_charges_amount.' will be adjusted towards other charges.');
    }

    public function assert_alternate_receipt_number(Browser $browser, $alternate_receipt_number)
    {
        $browser->assertSee($alternate_receipt_number);
    }

    public function assert_whether_both_collection_and_other_charge_receipt_deleted(Browser $browser, $alternate_receipt_number)
    {
        $browser->assertDontSee($alternate_receipt_number);
    }

    public function assert_information_in_date_wise_collection_report(Browser $browser)
    {
        $browser->assertSee('Additional columns available in Export: IDs (Transaction, Enrollment, Group, Subscriber), Alt. Transaction No., Reference No., Alt. Group Name, Bonus, Penalty, Transactor, Employee, Notes')
        ->assertSee('COLLECTIONS');
    }

    public function assert_document_added(Browser $browser)
    {
        $browser->assertSee('New document saved.');
    }

    public function assert_document_charges_not_paid(Browser $browser)
    {
        $browser->assertSee('Documentation Charge', 'Not Paid');
    }

    public function assert_document_charges_paid(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)->clickLink('Other Charges')->pause(60000);
        $charges_array = collect($browser->driver->findElements(WebDriverBy::cssSelector('#other_charges_tab > div > div > table > tbody > tr > td:nth-child(1)')));
        $charges_array = $charges_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($charges_array->search('Documentation'));
        $key = $key + 1;
        $browser->assertSeeIn('#other_charges_tab > div > div > table > tbody > tr:nth-child('.$key.') > td:nth-child(3) > span', 'Paid');
    }

    public function assert_that_document_charge_receipt_is_deleted(Browser $browser)
    {
        $browser->assertSee('deleted');
    }

    public function assert_document_charges_after_auction(Browser $browser)
    {
        $browser->assertSee('Documentation Charge', 'Not Paid');
    }

    public function assert_receipt_deleted(Browser $browser)
    {
        $browser->assertSee('deleted');
    }

    /**************** 7.1 - Receipt - Common Assertions ****************/

    /**************** 7.2 - Group - Common Assertions ****************/

    public function assert_new_group_created(Browser $browser)
    {
        $browser->assertsee('New group');
    }

    public function assert_group_details_edited(Browser $browser)
    {
        $browser->assertsee('Group details saved.');
    }

    public function assert_that_group_has_been_deleted(Browser $browser)
    {
        $browser->assertSee('deleted');
    }

    /**************** 7.2 - Group - Common Assertions ****************/

    /**************** 7.3 - Others - Common Assertions ****************/

    public function assert_subscriber_enrollment(Browser $browser)
    {
        $browser->assertSee('enrolled sucessfully.');
    }

    public function assert_commission_disbursement_details(Browser $browser, $agent_name, $chit_value)
    {
        $browser->assertSee($agent_name, $chit_value);
    }

    public function assert_commission_removed(Browser $browser)
    {
        $browser->assertSee('deleted.');
    }

    public function assert_KYC_documents_mandatory_before_enrollment(Browser $browser)
    {
        $browser->assertSee('Submission of KYC documents is mandatory to enroll a subscriber in a group.');
    }

    public function assert_auction_removed(Browser $browser)
    {
        $browser->assertSee('removed.');
    }

    public function assert_documentation_charges(Browser $browser)
    {
        $browser->assertSee('Documentation Charge');
    }

    public function assert_commission_deleted(Browser $browser)
    {
        $browser->assertSee('deleted.');
    }

    public function assert_that_introducer_commission_has_not_been_disbursed(Browser $browser)
    {
        $browser->assertSee('not disbursed.');
    }

    public function assert_that_introducer_commission_has_been_disbursed(Browser $browser)
    {
        $browser->assertSee('disbursed.');
    }

    public function assert_independence_day_in_holidays_page(Browser $browser)
    {
        $browser->assertSee('Independence Day', '15-08-19');
    }

    public function assert_the_details_in_datewise_collection_report(Browser $browser)
    {
        $browser->assertsee('Additional columns available in Export: IDs (Transaction, Enrollment, Group, Subscriber), Alt. Transaction No., Reference No., Alt. Group Name, Bonus, Penalty, Transactor, Employee, Notes', 'COLLECTIONS', 'TIME', 'SUBSCRIBER', 'GROUP', 'TICKET', 'RECEIPT NO', 'CREDIT', 'DEBIT', 'TYPE', 'COLLECTED');
    }

    public function assert_the_details_in_removed_enrollments_report(Browser $browser)
    {
        $browser->assertsee('SUBSCRIBER', 'ENROLLMENT', 'REMOVED DATE', 'GR. ST.', 'COLLECTED', 'REFUNDED', 'BALANCE', 'CAN. CHARGE');
    }

    /**************** 7.3 - Others - Common Assertions ****************/

    /********************************* 7 - Common Assertions ********************************/

    /************************** 9 - Functions in Dashboard *************************/
    public function click_branch_in_dashboard(Browser $browser, $branch_number)
    {
        $browser->click('#dashboard_tab > table > tbody > tr:nth-child('.$branch_number.') > td.text-left > a');
    }

    public function click_branch_name_in_dashboard(Browser $browser, $branch)
    {
        $browser->clickLink($branch);
    }

    public function click_branch_in_branches_page(Browser $browser, $branch)
    {
        $browser->click('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child('.$branch.') > td.text-left > a');
    }

    public function click_branch_name_in_branches_page(Browser $browser, $branch)
    {
        $browser->go_to_branches_page_from_menu()
                ->assertPathIs('/branches')
                ->clicklink($branch);
    }

    /************************** 9 - Functions in Dashboard *************************/

    /********************** 10 - Functions in Branch Ledgers *********************/

    public function click_groups_tab_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Groups');
    }

    public function click_subscribers_tab_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Subscribers');
    }

    /********************** 10 - Functions in Branch Ledgers *********************/

    /********************** 11 - Functions in Authentication *********************/

    public function log_out(Browser $browser)
    {
        $browser->click('#page-header-user-dropdown > i.fa.fa-fw.fa-angle-down.d-none.d-sm-inline-block')
        ->click('#page-header > div > div:nth-child(2) > div > div > div > a:nth-child(3) > i');
    }

    public function custom_sign_in(Browser $browser, $email, $password)
    {
        $browser->value('#email', $email)
        ->type('#password', $password)
        ->press('Login')
        ->pause(1000);
    }

    public function sign_in_as_group_manager(Browser $browser)
    {
        $browser->custom_sign_in('baby.shalini@dncchits.com', '123456');
    }

    /********************** 11 - Functions in Authentication *********************/

    /********************** 12 - Functions in Prize Money Disbursement *********************/

    public function click_disburse_in_prize_money_disbursement_form(Browser $browser)
    {
        $browser->press('Disburse');
    }

    /********************** 12 - Functions in Prize Money Disbursement *********************/

    /********************** 13 - Functions in Documents *********************/

    public function attach_document_in_current_page(Browser $browser, $path)
    {
        $browser->attach('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(4) > div > input', __DIR__.$path);
    }

    public function attach_document_in_guarantors_page(Browser $browser, $path)
    {
        $browser->attach('document_file', __DIR__.$path);
    }

    /********************** 13 - Functions in Documents *********************/

    /*********************  14 - Current Page Activities ********************/

    public function click_group_name_in_current_page(Browser $browser, $group_name)
    {
        $browser->clickLink($group_name);
    }

    public function click_receipt_in_current_page(Browser $browser, $receipt)
    {
        $browser->clickLink($receipt);
    }

    public function remove_alerts_in_current_page(Browser $browser)
    {
        $browser->press('remove alerts');
    }

    public function get_url(Browser $browser)
    {
        $browser->url = $browser->driver->getCurrentURL();
    }

    /*********************  14 - Current Page Activities ********************/

    /*********************  15 - Group Enrollment ********************/
    public function fill_random_subscriber_for_enrollment(Browser $browser, $subscriber)
    {
        $browser->type('subscriberForEnrollment', $subscriber)
        ->waitFor('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)', 60)
        ->click('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)');
    }

    public function fill_random_employee_for_enrollment(Browser $browser, $employee)
    {
        $browser->type('employeeForEnrollment', $employee)
        ->waitFor('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)', 60)
        ->click('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)');
    }

    public function fill_random_nominee_for_enrollment(Browser $browser, $nominee)
    {
        $browser->type('nominee_name', $nominee);
    }

    public function fill_random_introducer_for_enrollment(Browser $browser, $introducer)
    {
        $browser->type('introducerForEnrollment', $introducer)
        ->waitFor('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)', 60)
        ->click('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div > a:nth-child(1)');
    }

    /*********************  15 - Group Enrollment ********************/

    /********************************* 16 - Other Functions ********************************/

    public function click_employees_tab_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Employees');
    }

    /********************************* 16 - Others Functions ********************************/

    /************************ 17 - Functions in Branch Ledger ************************/
    public function click_agents_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Agents');
    }

    public function click_employees_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Employees');
    }

    public function click_subscribers_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Subscribers');
    }

    public function click_groups_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Groups');
    }

    public function click_enrollments_in_branch_ledger(Browser $browser)
    {
        $browser->click('#main-container > div.content > div.row > div:nth-child(1) > div > div.block-content > div > div:nth-child(4) > a');
    }

    public function click_guarantors_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Guarantors');
    }

    /************************ 17 - Functions in Branch Ledger ************************/

    /************************ 18 - Commissions ************************/
    public function enter_introducer_commission_disbursement_details(Browser $browser, $amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $processed_date, $processed_time, $realized_date, $realized_time, $notes)
    {
        $browser->enter_receipt_details_in_receipt_entry_form($amount, $date, $payment_mode, $reference_number, $bank_name, $branch_name, $processed_date, $processed_time, $realized_date, $realized_time, $notes);
    }

    public function remove_introducer_commission_disbursements(Browser $browser)
    {
        $browser->click('#introducer_commission_tab > div > div:nth-child(2) > table > tbody > tr:nth-child(2) > td:nth-child(8) > form > button');
    }

    /************************ 18 - Commissions ************************/

    public function get_branch_url(Browser $browser, $branch)
    {
        $browser->go_to_branches_page_from_menu()
        ->click_branch_name_in_branches_page($branch);
        $url = $browser->driver->getCurrentURL();
        $url = explode('/', $url);
        $browser->branch_id = $url[4];
    }

    public function enroll_new_subscriber(
        Browser $browser,
        $group,
        $subscriber,
        $employee,
        $introducer,
        $enrollment
    ) {
        $browser->go_to_create_new_enrollment_page($group)
                ->fill_subscriber_for_enrollment($subscriber->mobile, $subscriber->name)
                ->fill_introducer_for_enrollment($introducer->mobile, $introducer->name)
                ->fill_employee_for_enrollment($employee->mobile, $employee->name)
                ->fill_nominee_for_enrollment($subscriber->name)
                ->enroll_the_subscriber()
                ->clicklink($subscriber->name)
                ->get_enrollment_id($enrollment)
                ->get_enrollment_details($enrollment);
        $enrollment->group = $group->name;
        $enrollment->type = 'Enrollment';
        $enrollment->subscriber_id = $subscriber->id;
        $enrollment->group_id = $group->id;
        $enrollment->subscriber_name = $subscriber->name;
        $enrollment->subscriber_branch = $subscriber->branch;
        $enrollment->group_branch = $group->branch;
        $enrollment->url = $browser->get_url()->url;
    }

    public function edit_enrollment(
        Browser $browser,
        $group,
        $subscriber,
        $employee2,
        $introducer2,
        $enrollment
    ) {
        $browser->go_to_edit_enrollment_page($enrollment)
        ->fill_introducer_to_edit_enrollment($introducer2->mobile, $introducer2->name)
        ->fill_employee_to_edit_enrollment($employee2->mobile, $employee2->name)
        ->fill_nominee_to_edit_enrollment('Danny', 'Mother')
        ->fill_notes('Testing 123')
        ->press('Save')
        ->assertSee('saved sucessfully.')
        ->get_enrollment_id($enrollment)
        ->get_enrollment_details($enrollment);
        $enrollment->group = $group->name;
        $enrollment->type = 'Enrollment';
        $enrollment->subscriber_id = $subscriber->id;
        $enrollment->group_id = $group->id;
        $enrollment->subscriber_name = $subscriber->name;
        $enrollment->subscriber_branch = $subscriber->branch;
        $enrollment->group_branch = $group->branch;
    }

    public function format_name(Browser $browser, $rawname)
    {
        $name_without_special_characters = preg_replace('/[^A-Za-z0-9\-]/', ' ', $rawname);
        $name_without_hiphens = preg_replace('/-+/', ' ', $name_without_special_characters);
        $formatted_name = trim(preg_replace('/\s+/', ' ', $name_without_hiphens));
        $browser->formatted_name = $formatted_name;
    }

    public function remove_score_from_name(Browser $browser, $name_with_scores)
    {
        $name_without_scores = substr($name_with_scores, 0, -4);

        $formatted_name = $name_without_scores;

        $browser->formatted_name = $formatted_name;
    }

    public function click_enroll_subscriber(Browser $browser)
    {
        $browser->click('#main-container > div.content > div:nth-child(2) > div > div > ul > li.nav-item.ml-auto > div > a');
    }

    public function go_to_branch(Browser $browser, $branch)
    {
        $browser->go_to_branches_page_from_menu()
                ->click_branch_name_in_dashboard($branch->name);
    }

    public function get_details_in_enrollment_ledger(Browser $browser, $enrollment_ledger)
    {
        $enrollment_ledger->installment_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(3)');

        $enrollment_ledger->to_be_collected_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(4)');

        $enrollment_ledger->paid_installment_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(5)');

        $enrollment_ledger->bonus_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(6)');

        $enrollment_ledger->penalty_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(7)');

        $enrollment_ledger->payment_days = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(8)');

        $enrollment_ledger->settled_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(9)');

        $enrollment_ledger->total_receipt_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(10)');

        $enrollment_ledger->balance_amount = $browser->text('#settlements-table > tbody > tr.table-info > td:nth-child(11)');
    }

    public function format_amount(Browser $browser, $amount)
    {
        $browser->format_amount = preg_replace('/[^A-Za-z0-9\-]/', '', $amount);
    }

    public function click_toggle_settlement_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#toggle-settlement-information');
    }

    public function get_employee_name(Browser $browser, $number)
    {
        $browser->employee_name = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$number.') > td.font-w600 > a');
    }

    public function get_agent_name(Browser $browser, $number)
    {
        $browser->agent_name = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$number.') > td.font-w600 > a');
    }

    public function get_subscriber_name(Browser $browser, $number)
    {
        $browser->subscriber_name = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$number.') > td.font-w600 > a');
    }

    public function get_transactor_name(Browser $browser)
    {
        $browser->transactor_name = $browser->get_employee_name(2)->employee_name;

        $transactor_name = $browser->transactor_name = $browser->remove_score_from_name($browser->transactor_name)->formatted_name;

        $transactor_name = $browser->format_name($transactor_name)->formatted_name;
    }

    public function save_the_collections(Browser $browser)
    {
        $browser->press('Save Collection')->waitForText('Collection receipt saved.', 20);
    }

    public function enter_transactor_name(Browser $browser, $transactor_name)
    {
        $browser->type('main#main-container form input[name="transactor"]', $transactor_name)->wait_and_click_in_dropdown($transactor_name);
    }

    public function wait_and_click_in_dropdown(Browser $browser, $name)
    {
        $browser->waitForLink($name, 60)->clickLink($name);
    }

    public function assert_receipt(Browser $browser, $receipt)
    {
        $browser->assertSee($receipt->number);
    }

    public function click_receipt(Browser $browser, $receipt_number)
    {
        $browser->clickLink($receipt_number);
    }

    public function get_branch_id(Browser $browser, $branch_name)
    {
        $browser->click_branch_name_in_branches_page($branch_name);

        $browser->url = $browser->get_url()->url;
        $browser->url = explode('/', $browser->url);
        $browser->id = $browser->url = $browser->url[4];
    }

    public function group_enrollment_details($browser, $type, $row)
    {
        $browser->{$type.'->name'} = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$row.') > td.font-w600 > a');

        $browser->{$type.'->name'} = $browser->remove_score_from_name($browser->{$type.'->name'})->formatted_name;

        $browser->{$type.'->name'} = $browser->format_name($browser->{$type.'->name'})->formatted_name;
    }

    public function random_function($browser, $count)
    {
        $browser->random = rand(1, $count);
    }

    public function check_score_exists_in_name($browser, $name)
    {
        $browser->formatted_name = $name;

        $browser->score = substr($browser->formatted_name, -4);

        $browser->score = (float)$browser->score;

        $browser->score = is_float($browser->score);

        if (is_float($browser->score) == 'true' || $browser->score == 'N/A') {
            $browser->score = $browser->remove_score_from_name($browser->formatted_name)->formatted_name;
        } else {
            $browser->score = $browser->formatted_name;
        }
    }

    public function pick_member($browser, $member)
    {
        $subscriber = (object)[];
        $employee = (object)[];
        $agent = (object)[];

        $count = $browser->elements('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr > td.font-w600');

        $count = count($count);

        $count_in_page_array = $browser->text('#main-container > div.content > div > div > div > div.block-header.border-bottom > h3');

        $count_in_page = explode(' ', $count_in_page_array);
        $count_in_page_array = $count_in_page_array[0];

        if ($count_in_page == '0' && $member == 'subscriber') {
            do {
                $browser->pick_subscriber($subscriber);
            } while ($count == 0);
        }

        if ($count_in_page == '0' && $member == 'employee') {
            do {
                $browser->pick_employee($employee);
            } while ($count == 0);
        }

        if ($count_in_page == '0' && $member == 'agent') {
            do {
                $browser->pick_agent($agent);
            } while ($count == 0);
        }

        $count = $browser->random_function($count)->random;

        $member->name = $browser->text('#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child('.$count.') > td.font-w600');

        $member->name = $browser->check_score_exists_in_name($member->name)->formatted_name;

        $browser->pause(300)->clickLink($member->name);
    }

    public function number_of_branches_in_dashboard(Browser $browser)
    {
        $browser->number_of_branches = $browser->elements('#dashboard_tab > table > tbody > tr > td.text-left > a');

        $browser->number_of_branches = count($browser->number_of_branches);

        $browser->number_of_branches = $browser->random_function($browser->number_of_branches)->random;
    }

    public function number_of_branches_in_branches_page(Browser $browser)
    {
        $browser->number_of_branches = $browser->elements('#main-container > div.content > div > div > div > div.block-content > table > tbody > tr > td.text-left > a');

        $browser->number_of_branches = count($browser->number_of_branches);

        $browser->number_of_branches = $browser->random_function($browser->number_of_branches);
    }

    public function pick_random_page_number(Browser $browser)
    {
        $browser->number_of_pages = $browser->elements('#main-container > div.content  div.block-content nav > ul  a');

        $browser->page_number = $browser->rand(1, count($browser->number_of_pages));

        $browser->clicklink($browser->page_number);
    }

    public function navigate_from_branch_ledger(Browser $browser, $branch_id, $type)
    {
        $URL = env('APP_URL');

        $browser->visit($URL.'/branches/'.$branch_id.'/'.$type);
    }

    public function pagination(Browser $browser)
    {
        $browser->page_number = $browser->elements('#main-container > div.content nav > ul.pagination > li.page-item:not(:nth-child(1)):not(:nth-last-child(1))');
        $browser->page_number = count($browser->page_number);
        if ($browser->page_number > '1' && $browser->page_number < '8') {
            $browser->page_number = $browser->random_function($browser->page_number)->random;
        }
        if ($browser->page_number > '7') {
            $browser->page_number = $browser->random_function('8')->random;
        }
    }

    public function get_member_details(Browser $browser, $member, $type)
    {
        if ($type == 'subscriber') {
            $browser->go_to_details_tab();
            $member->score = $browser->text('#page-container main#main-container div.font-size-h4.font-w600');
            $member->code = $browser->value('#alt_id');
            $browser->get_subscriber_id($member);
        }

        if ($type == 'employee') {
            $browser->go_to_details_tab()
                        ->get_employee_id($member);
            $member->score = $browser->text('#page-container main#main-container div.font-size-h4.font-w600');
        }
        if ($type == 'guarantor') {
            $browser->get_guarantor_id($member);
        }

        $member->name = $browser->text('#main-container div.bg-primary-dark div h2');
        $member->mobile = $browser->value('#mobile_primary');
        $member->pan = $browser->value('#pan_no');
        $member->aadhar = $browser->value('#aadhaar_no');
        $member->branch = $browser->value('#branch_id');
        $member->ration = $browser->value('#ration_no');
        $member->gst = $browser->value('#gst_no');
        $member->address_present = $browser->value('#address_present');
        $member->city_present = $browser->value('#city_present');
        $member->district_present = $browser->value('#district_present');
        $member->state_present = $browser->value('#state_present');
        $member->pincode_present = $browser->value('#pincode_present');
        $member->address_permanent = $browser->value('#address_permanent');
        $member->city_permanent = $browser->value('#city_permanent');
        $member->district_permanent = $browser->value('#district_permanent');
        $member->state_permanent = $browser->value('#state_permanent');
        $member->pincode_permanent = $browser->value('#pincode_permanent');
        $member->source_of_income = $browser->value('#source_of_income');
        $member->monthly_income = $browser->value('#monthly_income');
        $member->father_name = $browser->value('#father_name');
        $member->mother_name = $browser->value('#mother_name');
        $member->spouse_name = $browser->value('#spouse_name');
        $member->type = ucfirst($type);
    }

    public function pick_subscriber(Browser $browser, $subscriber)
    {
        $browser->pick_random_person_of_type('subscriber', $subscriber);
    }

    public function pick_employee(Browser $browser, $employee)
    {
        $browser->pick_random_person_of_type('employee', $employee);
    }

    public function pick_employee_from(Browser $browser, $branch, $employee)
    {
        $URL = env('APP_URL');
        $browser->visit($URL.'/branches/'.$branch->id.'/employees');
        $browser->page_number = $browser->pagination()->page_number;
        $browser->page_number = $browser->random_function($browser->page_number)->random;
        if ($browser->page_number > '1') {
            $browser->click('#main-container > div.content nav > ul.pagination > li:nth-child('.$browser->page_number.') > .page-link');
        }

        $browser->pick_member($employee, 'employee');
        $browser->get_member_details($employee, 'employee');
    }

    public function pick_agent(Browser $browser, $agent)
    {
        $browser->pick_random_person_of_type('agent', $agent);
        // $branch = (object)[];
        // $URL = env('APP_URL');
        // $browser->pick_random_branch($branch);
        // $browser->visit($URL.'/branches/'.$branch->id.'/agents');
        // $browser->page_number = $browser->pagination()->page_number;
        // $browser->page_number = $browser->random_function($browser->page_number)->random;

        // if ($browser->page_number > '1') {
        //     $browser->click('#main-container > div.content nav > ul.pagination > li:nth-child('.$browser->page_number.') > .page-link');
        // }

        // $browser->pick_member($agent, 'agent');
        // $browser->get_member_details($agent, 'agent');
    }

    public function fill_subscriber_for_enrollment(Browser $browser, $subscriber_mobile, $subscriber_name)
    {
        $browser->type('#main-container input[name = "subscriber"]', $subscriber_mobile)
                ->waitForlink($subscriber_name, 60)
                ->clicklink($subscriber_name);
    }

    public function fill_employee_for_enrollment(Browser $browser, $employee_mobile, $employee_name)
    {
        $browser->type('#main-container input[name = "employee"]', $employee_mobile)
                ->waitForLink($employee_name, 60)
                ->clickLink($employee_name);
    }

    public function fill_nominee_for_enrollment(Browser $browser, $subscriber)
    {
        $browser->type('nominee_name', $subscriber);
    }

    public function fill_introducer_for_enrollment(Browser $browser, $introducer_mobile, $introducer_name)
    {
        $browser->type('#main-container input[name = "introducer"]', $introducer_mobile)
                ->waitForLink($introducer_name, 60)
                ->clickLink($introducer_name);
    }

    public function get_details_from_introducer_commission_tab_in_enrollment_ledger(Browser $browser, $commission)
    {
        $commission->introduced_by = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > div > div:nth-child(1) > h5');
        $commission->chit_value = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(1) > td.text-right');
        $commission->commission = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(2) > td.text-right');
        $commission->tds = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr:nth-child(3) > td.text-right');
        $commission->disbursable = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-info > td.text-right');
        $commission->undisbursed = $browser->text('#introducer_commission_tab > div > div:nth-child(1) > table > tbody > tr.table-warning > td.text-right');
    }

    public function get_enrollment_date_in_enrollment_ledger(Browser $browser)
    {
        $browser->date = $browser->text('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(3) > small');
        $browser->date = explode(' ', $browser->date);
        $browser->date = $browser->date[1];
    }

    public function pick_random_branch(Browser $browser, $branch)
    {
        $browser->go_to_branches_page_from_menu();
        $branch->count = $browser->elements('#main-container > div.content div.block-content > table > tbody > tr > td.text-left > a');
        $branch->count = count($branch->count);
        $branch->number = $browser->random_function($branch->count)->random;
        $browser->get_branch_details($branch);
    }

    public function go_to_member_ledger($browser, $type)
    {
        $rows = $browser->elements('main#main-container div.content div.block div.block-content table.table tbody tr');

        $activeClass = '';
        $randomElementIndex = -1;

        do {
            $randomElementIndex = $browser->random_function(count($rows))->random;
            $activeClass = $browser->attribute('main#main-container div.content div.block div.block-content table.table tbody tr:nth-child('.$browser->random.')', 'class');

            $browser->{$type.'_name'} = $browser->text('main#main-container div.content div.block div.block-content table.table tbody tr:nth-child('.$randomElementIndex.') td.font-w600 a');
        } while ($activeClass == 'bg-danger-light');

        $browser->{$type.'_name'} = $browser->check_score_exists_in_name($browser->{$type.'_name'})->formatted_name;

        $browser->clickLink($browser->{$type.'_name'});
    }

    public function set_back_date(Browser $browser, $number_of_days)
    {
        $browser->back_date = date('d-m-Y');

        $browser->back_date = date_create($browser->back_date);

        date_sub($browser->back_date, date_interval_create_from_date_string($number_of_days.' days'));

        $browser->back_date = date_format($browser->back_date, 'd-m-Y');
    }

    public function set_future_date(Browser $browser, $number_of_days)
    {
        $browser->back_date = date('d-m-Y');

        $browser->back_date = date_create($browser->back_date);

        date_add($browser->back_date, date_interval_create_from_date_string($number_of_days.' days'));

        $browser->back_date = date_format($browser->back_date, 'd-m-Y');
    }

    public function select_random_relationship(Browser $browser)
    {
        $browser->relation = $browser->elements('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(4) > div > select > option:nth-child(n)');
        $browser->relation = count($browser->relation);
        $browser->relation = $browser->random_function($browser->relation)->random;
    }

    public function pick_random_enrollment(Browser $browser, $enrollment)
    {
        $branch = (object)[];
        $URL = env('APP_URL');
        $browser->pick_random_branch($branch);
        $browser->visit($URL.'/reports/enrollment_quality?branch_id='.$branch->id);

        $number_of_enrollments = $browser->elements('#page-container main#main-container .block #DataTables_Table_0 tbody tr td.font-size-sm a');

        $count = $browser->random_function(count($number_of_enrollments))->random;

        $enrollment->name = $browser->text('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->name = $browser->check_score_exists_in_name($enrollment->name)->formatted_name;

        $browser->click('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->id = $browser->get_enrollment_id()->enrollment_id;

        $enrollment->url = $browser->get_url()->url;
        $browser->get_enrollment_details($enrollment);
    }

    public function get_enrollment_id(Browser $browser)
    {
        $browser->url = $browser->driver->getCurrentURL();
        $browser->URL = explode('/', $browser->url);
        $browser->enrollment_id = $browser->URL[6];
        $browser->group_id = $browser->URL[4];
    }

    public function get_subscriber_id(Browser $browser, $subscriber)
    {
        $url = $browser->driver->getCurrentURL();
        $url = explode('/', $url);
        $subscriber_id = $url[6];
        $subscriber->id = $subscriber_id;
    }

    public function assert_that_lined_enrollment_cannot_win_auction(Browser $browser, $group, $enrollment, $date)
    {
        $browser->clickLink($group->name)
        ->add_auction_entry_for_liened_enrollment($date, $enrollment->id, '1100')
        ->assertsee('This enrollment is not permitted to win the auction since it is provided as guarantee for another enrollment.');
    }

    public function remove_guarantor_from_enrollment(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
        ->go_to_guarantees_tab_in_enrollment_ledger();
        $el = $browser->elements('#guarantees_tab > div:nth-child(1) > div > table > tbody > tr > td:nth-child(6) > form > button > i');
        $count = count($el);

        while ($count > 0) {
            $browser->click('#guarantees_tab > div:nth-child(1) > div > table > tbody > tr:nth-child(1) > td:nth-child(6) > form > button > i')
            ->assertsee('Guarantee removed sucessfully.')
            ->go_to_enrollment_with_id($enrollment)
            ->go_to_guarantees_tab_in_enrollment_ledger();
            $el = $browser->elements('#guarantees_tab > div:nth-child(1) > div > table > tbody > tr > td:nth-child(6) > form > button > i');
            $count = count($el);
        }
    }

    public function go_to_datewise_auction_report(Browser $browser, $branch_id, $month, $include)
    {
        $browser->clickLink('Auctions')->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li > a > span')
        ->select('#branch_id', $branch_id)->select('#auction_month', $month)->select('#auction_type', $include)->press('Generate');
    }

    public function assert_the_details_in_datewise_auction_report(Browser $browser)
    {
        $browser->assertsee('Additional columns available in Export: Alt. Group Name, Chit Value, Introducer Commission, GST Receipt No., Present Address');
    }

    public function go_to_subscriber_wise_tbc_report(Browser $browser, $branch_id, $employee_id, $payment_days, $to_be_collected_percent, $prized_status)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(4) > a > span')
        ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(1) > a > span')
        ->select('#branch_id', $branch_id)
        ->select('#employee_id', $employee_id)
        ->type('#payment_days', $payment_days)
        ->type('#to_be_collected_percent', $to_be_collected_percent)
        ->select('#prized_status', $prized_status)
        ->press('Generate');
    }

    public function assert_the_details_in_subscriber_wise_tbc_report(Browser $browser)
    {
        $browser->assertsee('Additional columns available in Export: Branch, Mobile, Last Auction Date, Chit Value, Alt. Group Name, Enrollment Date, Address (Present), Address (Permanent), IDs (Branch, Subscriber, Group, Enrollment)', 'SUBSCRIBER', 'TO BE COLLECTED', 'NAME', 'ENRL.', 'PR. ST.', 'GR. ST.', 'TBC', '%', 'INST.', 'AMT.', 'BON.', 'PEN.', 'O. C.', 'DAYS', 'TBC INST.', 'COLL.', 'EMP.');
    }

    public function go_to_group_wise_tbc_report(Browser $browser, $branch_id)
    {
        $browser->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(4) > a > span')
        ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(2) > a > span')
        ->select('#branch_id', $branch_id)
        ->press('Generate')
        ->click('#main-container > div.content > div > div > div:nth-child(2) > div.block-header.border-bottom > a');
    }

    public function assert_the_details_in_group_wise_tbc_report(Browser $browser)
    {
        $browser->assertsee('NAME', 'BRANCH', 'STATUS', 'TO BE COLLECTED', 'INSTALLMENT', 'BONUS', 'PENALTY OT. CH.');
    }

    public function format_mobile(Browser $browser, $member)
    {
        $member->mobile = $member->phoneNumber;
        $member->mobile = $browser->format_name($member->mobile)->formatted_name;
        $member->mobile = str_replace(' ', '', $member->mobile);
        $member->mobile = substr($member->mobile, 0, 10);
    }

    public function go_to_relationships_tab_in_subscriber_ledger(Browser $browser)
    {
        $browser->click('#page-container main#main-container div.content div.block ul.nav-tabs li.nav-item a[href="#relationships_tab"]');
    }

    public function lien_the_non_prized_chit(Browser $browser, $target, $group, $subscriber)
    {
        $browser->go_to_enrollment($group->name, $subscriber->name)
                ->go_to_guarantees_tab_in_enrollment_ledger()
                ->clickLink('Add Guarantee')
                ->type('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search', $target)
                ->waitForLink($target, 60)
                ->clickLink($target)
                ->press('Add');
    }

    public function assert_that_guarantee_added(Browser $browser)
    {
        $browser->assertsee('Guarantee added sucessfully.');
    }

    public function get_enrollment_details(Browser $browser, $enrollment)
    {
        $enrollment->ticket_status = $browser->text('#main-container > div.bg-primary-dark div.col-10 > h3 > div > div:nth-child(1)');
        $enrollment->ticket_status = explode(' ', $enrollment->ticket_status);
        $enrollment->ticket_name = $enrollment->ticket_status[2];
        $ticket_name = $enrollment->ticket_name;
        $enrollment->ticket_number = preg_replace('/[^A-Za-z0-9\-]/', '', $ticket_name);

        try {
            $browser->assertSee('Non Prized');
            $enrollment->prized_status = $enrollment->ticket_status[3].' '.$enrollment->ticket_status[4];
        } catch (\Exception $e) {
            $enrollment->prized_status = $enrollment->ticket_status[3];
        }
        $enrollment->id = $browser->get_enrollment_id()->enrollment_id;
        $enrollment->url = $browser->get_url()->url;
        $enrollment->group_id = $browser->get_group_id_from_enrollment_ledger($enrollment)->group_id;

        $enrollment->date = $browser->get_enrollment_date_in_enrollment_ledger()->date;

        if ($enrollment->prized_status == 'Non Prized') {
            $enrollment->name = $browser->text('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(1) > a');
            $enrollment->score = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(1) div.font-size-h4.font-w600 > span > span');
            $enrollment->collected = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(2) div.font-size-h4.font-w400');
            $enrollment->tbc = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(3) div.font-size-h4.font-w400');
            $enrollment->future_liab = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(4) div.font-size-h4.font-w400');
            $enrollment->kyc = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(5) div.font-size-h4.font-w400');
            $enrollment->secyrity_docs = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(6) div.font-size-h4.font-w400');
            $enrollment->primary_name = $browser->text('#main-container > div.bg-primary-dark > div > h2 > a');
        } else {
            $enrollment->name = $browser->text('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(1) > a');
            $enrollment->score = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(1) div.font-size-h4.font-w600');
            $enrollment->collected = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(2) div.font-size-h4.font-w400');
            $enrollment->tbc = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(3) div.font-size-h4.font-w400');
            $enrollment->disbursed_pr_mn = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(4) div.font-size-h4.font-w400');
            $enrollment->und_pr_mn = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(5)  div.font-size-h4.font-w400');
            $enrollment->no_of_guarantees = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(6) div.font-size-h4.font-w400');
            $future_liab = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(7) div.font-size-h4.font-w400');
            $enrollment->future_liab = $browser->format_amount($future_liab)->format_amount;
            $enrollment->kyc = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(8) div.font-size-h4.font-w400');
            $enrollment->secyrity_docs = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(9) div.font-size-h4.font-w400');
            $enrollment->verification = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(10) div.font-size-h4.font-w400');
            $enrollment->guarantees_available = $browser->text('#main-container > div.content > div:nth-child(2) > div:nth-child(6) div.font-size-h4.font-w400');
            $enrollment->name = $browser->text('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(1) > a');
            $enrollment->primary_name = $browser->text('#main-container > div.bg-primary-dark > div > h2 > a');
        }
    }

    public function lien_the_subscriber(Browser $browser, $group, $subscriber, $target)
    {
        $browser->go_to_enrollment($group->name, $subscriber->name)
                   ->go_to_guarantees_tab_in_enrollment_ledger()
                   ->clickLink('Add Guarantee')
                   ->type('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search', $target)
                   ->waitForLink($target, 60)
                   ->clickLink($target)
                   ->press('Add');
    }

    public function pick_guarantor(Browser $browser, $guarantor)
    {
        $browser->pick_random_person_of_type('guarantor', $guarantor);
    }

    public function assert_that_prized_chit_cannot_be_lined(Browser $browser, $group, $subscriber, $enrollment)
    {
        $browser->go_to_enrollment($group->name, $subscriber->name)
                 ->go_to_guarantees_tab_in_enrollment_ledger()
                 ->clickLink('Add Guarantee')
                 ->type('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search', $enrollment->name)
                 ->waitForLink($enrollment->name, 60)
                 ->clickLink($enrollment->name)
                 ->press('Add')
                 ->assertsee('Prized enrollments cannot be provided as guarantee.');
    }

    public function go_to_group_with_id(Browser $browser, $group)
    {
        $browser->visit('/groups/'.$group->id.'/enrollments');
    }

    public function go_to_enrollment_with_id(Browser $browser, $enrollment)
    {
        dump($enrollment->group_id);
        $enrollment_url = 'https://staging.dncchits.in/groups/'.$enrollment->group_id.'/enrollments/'.$enrollment->id;
        $browser->visit($enrollment_url);
    }

    public function get_guarantor_id(Browser $browser, $guarantor)
    {
        $browser->pause(200);//this may prevent errors
        $url = $browser->driver->getCurrentURL();
        $url = explode('/', $url);
        $guarantor->id = $url[6];
    }

    public function pick_random_person_of_type(Browser $browser, $type, $member)
    {
        $branch = (object)[];
        $URL = env('APP_URL');
        $count = 0;

        while ($count == 0) {
            $browser->pick_random_branch($branch);
            $member->branch_id = $branch->id;
            $browser->visit($URL.'/branches/'.$branch->id.'/'.$type.'s/');
            $browser->page_number = $browser->pagination()->page_number;
            $browser->page_number = $browser->random_function($browser->page_number)->random;
            if ($browser->page_number > '1') {
                $browser->click('#main-container > div.content nav > ul.pagination > li:nth-child('.$browser->page_number.') > .page-link');
            }

            $rows = $browser->elements('main#main-container div.content div.block div.block-content table.table tbody tr');
            $count = count($rows);
        }
        $activeClass = '';
        $randomElementIndex = -1;
        do {
            $randomElementIndex = $browser->random_function($count)->random;
            $activeClass = $browser->attribute('main#main-container div.content div.block div.block-content table.table tbody tr:nth-child('.$browser->random.')', 'class');

            $member->name = $browser->text('main#main-container div.content div.block div.block-content table.table tbody tr:nth-child('.$randomElementIndex.') td.font-w600 a');
        } while ($activeClass == 'bg-danger-light');

        $member->name = $browser->check_score_exists_in_name($member->name)->formatted_name;

        if ($type == 'agent') {
            try {
                $browser->assertSeeLink($member->name);
            } catch (\Exception $e) {
                $browser->pick_agent($member);
            }
        }
        $browser->clickLink($member->name)->get_member_details($member, $type);
    }

    public function go_to_branch_with_id(Browser $browser, $branch)
    {
        $browser->visit('/branches/'.$branch->id);
    }

    public function assert_KYC_documents_are_mandatory_to_enroll_a_subscriber_in_group(Browser $browser)
    {
        $browser->assertSee('Submission of KYC documents is mandatory to enroll a subscriber in a group');
    }

    public function click_guarantors_tab_in_branch_ledger(Browser $browser)
    {
        $browser->clickLink('Guarantors');
    }

    public function go_to_guarantees_tab_in_subscriber_main_ledger(Browser $browser)
    {
        $browser->clickLink('Guarantees');
    }

    public function go_to_interactions_tab_in_subscriber_main_ledger(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(6) > a');
    }

    public function go_to_subscriber_main_ledger(Browser $browser, $subscriber)
    {
        $browser->type('#search-autocomplete', $subscriber->mobile)
                ->waitForLink($subscriber->mobile, 60)
                ->clickLink($subscriber->mobile);
    }

    public function add_interaction(Browser $browser, $keyword, $keyword1, $keyword2)
    {
        $browser
            ->clickLink('Add Interaction')
            ->select('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(4) > div > select', $keyword)
            ->type('#notes', $keyword1)
            ->type('#follow_up_date', $keyword2)
            ->press('Save');
    }

    public function pick_random_group(Browser $browser, $group)
    {
        $branch = (object)[];
        $URL = env('APP_URL');
        $browser->pick_random_branch($branch);
        $browser->visit($URL.'/branches/'.$branch->id.'/groups');
        $group->branch_id = $branch->id;
        $number_of_groups = $browser->elements('#page-container main#main-container table.table tr td.text-left.font-w600 a');

        $count = $browser->random_function(count($number_of_groups))->random;

        $group->name = $browser->text('#page-container main#main-container table.table tr:nth-child('.$count.') td.text-left.font-w600 a');

        $group->name = $browser->check_score_exists_in_name($group->name)->formatted_name;

        $browser->click('#page-container main#main-container table.table tr:nth-child('.$count.') td.text-left.font-w600 a');

        $browser->get_group_details($group);

        while ($group->status == 'Running') {
            $browser->pick_random_group($group);
        }
    }

    public function get_employee_id(Browser $browser, $employee)
    {
        $employee->url = $browser->driver->getCurrentURL();
        $employee->url = explode('/', $employee->url);
        $employee->id = $employee->url[6];
    }

    public function assert_add_charges_are_locked(Browser $browser)
    {
        $browser->assertdontsee('Other Charges Cant Be Added');
    }

    public function assert_add_collections_are_locked(Browser $browser)
    {
        $browser->assertdontsee('Collections are Locked');
    }

    public function assert_auction_is_saved(Browser $browser)
    {
        $browser->assertsee('Auction saved.');
    }

    public function assert_disburse_prize_money_is_locked(Browser $browser)
    {
        $browser->assertdontsee('Disbursement Is Locked');
    }

    public function get_enrollment_details_with_id(Browser $browser, $enrollment)
    {
        $browser->go_to_enrollment_with_id($enrollment)
                ->get_enrollment_details($enrollment);
    }

    public function go_to_enrollments_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('#main-container div:nth-child(2) li:nth-child(1) > a');
    }

    public function go_to_auctions_tab_in_group_with_id(Browser $browser, $group)
    {
        $browser->visit('/groups/'.$group->id.'/auctions');
    }

    public function add_guarantor_to_enrollment(Browser $browser, $enrollment, $guarantor)
    {
        $browser
                 ->visit('/enrollments/'.$enrollment->id.'/guarantees/create')
                 ->type('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search', $guarantor->mobile)
                 ->waitForLink('Guarantor', 60)
                 ->clickLink('Guarantor')
                 ->press('Add');
    }

    /****  Function name will updated in next iteration ****/
    public function just_enroll_subscriber(Browser $browser, $group, $subscriber, $employee, $introducer)
    {
        $browser->go_to_create_new_enrollment_page($group)
            ->fill_subscriber_for_enrollment($subscriber->mobile, $subscriber->name)
            ->fill_introducer_for_enrollment($introducer->mobile, $introducer->name)
            ->fill_employee_for_enrollment($employee->mobile, $employee->name)
            ->fill_nominee_for_enrollment($subscriber->name)
            ->enroll_the_subscriber();
    }

    /****  Function name will updated in next iteration ****/

    public function pick_random_date_in_holidays_page(Browser $browser, $date)
    {
        $holiday_dates_array = collect($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[2]')));
        $holiday_dates_array = $holiday_dates_array->map(function ($el) {
            return $el->getText();
        });

        $count = count($holiday_dates_array);

        $random = $browser->random_function($count)->random;

        $date->holiday = $browser->text('#page-container main#main-container table.table tbody tr:nth-child('.$random.') td:nth-child(2)');

        $date->holiday_format = str_replace('-', '', $date->holiday);

        $date->holiday_format = substr_replace($date->holiday_format, '20', 4, 0);

        $todays_date = date('d-m-Y');

        $date->holiday = substr_replace($date->holiday, '20', 6, 0);

        $date->holiday = date_create($date->holiday);

        $date->holiday = date_format($date->holiday, 'd-m-Y');

        while ($todays_date < $date->holiday) {
            $browser->pick_random_date_in_holidays_page($date);
        }

        $date->holiday = date_create($date->holiday);

        $date->holiday = date_format($date->holiday, 'd-m-Y h:i:s');
    }

    public function pick_a_prized_enrollment(Browser $browser, $dates, $enrollment)
    {
        $number_of_rows = 0;
        while ($number_of_rows == 0) {
            $branch = (object)[];
            $browser->pick_random_branch($branch)
                    ->generate_prized_money_released_report($branch, $dates);
            $el = $browser->elements('#page-container main#main-container table.table tbody tr td:nth-child(1) a');
            $number_of_rows = count($el);
        }

        $picked_number = rand(1, $number_of_rows);

        $picked_member = $browser->text('#page-container main#main-container table.table tbody tr:nth-child('.$picked_number.') td:nth-child(1) a');

        try {
            $browser->assertSeeLink($picked_member);
        } catch (\Exception $e) {
            $browser->pick_a_prized_enrollment($dates, $enrollment);
        }

        $browser->click('#page-container main#main-container table.table tbody tr:nth-child('.$picked_number.') td:nth-child(1) a')
                ->get_enrollment_details($enrollment);
    }

    public function add_guarantee_to_enrollment(Browser $browser, $group, $subscriber, $target)
    {
        $browser
                 ->go_to_enrollment($group->name, $subscriber->name)
                 ->go_to_guarantees_tab_in_enrollment_ledger()
                 ->clickLink('Add Guarantee')
                 ->type('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search', $target)
                 ->waitForLink($target, 60)
                 ->clickLink($target)
                 ->press('Add');
    }

    public function format_number(Browser $browser, $number)
    {
        $browser->format_number = preg_replace('/[^A-Za-z0-9\-]/', '', $number);
    }

    public function pick_one_subscriber_from_possibly_Vacant_report(Browser $browser, $branch, $enrollment)
    {
        $URL = env('APP_URL');
        $browser->visit($URL.'/reports/enrollment_quality?branch_id='.$branch->id)
                ->select('#enrollment_status', 'is_possibly_vacant')
                ->press('Generate');

        $number_of_enrollments = $browser->elements('#page-container main#main-container .block #DataTables_Table_0 tbody tr td.font-size-sm a');

        $count = $browser->random_function(count($number_of_enrollments))->random;

        $enrollment->name = $browser->text('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->name = $browser->check_score_exists_in_name($enrollment->name)->formatted_name;

        $browser->click('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->id = $browser->get_enrollment_id()->enrollment_id;

        $enrollment->url = $browser->get_url()->url;
        $browser->get_enrollment_details($enrollment);
    }

    public function pick_one_subscriber_from_eligible_for_legal_action(Browser $browser,$branch, $enrollment)
    {
       
        $URL = env('APP_URL');        
        $browser->visit($URL.'/reports/enrollment_quality?branch_id='.$branch->id)
                ->select('#enrollment_status', 'is_legal_eligible')
                ->press('Generate');

        $number_of_enrollments = $browser->elements('#page-container main#main-container .block #DataTables_Table_0 tbody tr td.font-size-sm a');

        $count = $browser->random_function(count($number_of_enrollments))->random;

        $enrollment->name = $browser->text('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->name = $browser->check_score_exists_in_name($enrollment->name)->formatted_name;

        $browser->click('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->id = $browser->get_enrollment_id()->enrollment_id;

        $enrollment->url = $browser->get_url()->url;
        $browser->get_enrollment_details($enrollment);
    }

    public function pick_one_subscriber_from_recovery_eligible_report(Browser $browser, $branch, $enrollment)
    {
        $URL = env('APP_URL');
        $browser->visit($URL.'/reports/enrollment_quality?branch_id='.$branch->id)
                ->select('#enrollment_status', 'is_recovery_eligible')
                ->press('Generate');

        $number_of_enrollments = $browser->elements('#page-container main#main-container .block #DataTables_Table_0 tbody tr td.font-size-sm a');

        $count = $browser->random_function(count($number_of_enrollments))->random;

        $enrollment->name = $browser->text('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->name = $browser->check_score_exists_in_name($enrollment->name)->formatted_name;

        $browser->click('#page-container main#main-container .block #DataTables_Table_0 tbody tr:nth-child('.$count.') td.font-size-sm a');

        $enrollment->id = $browser->get_enrollment_id()->enrollment_id;
        $enrollment->branch_id = $branch->id;
        $enrollment->url = $browser->get_url()->url;
        $browser->get_enrollment_details($enrollment);
    }

    public function assert_in_interaction_report_in_possibly_vacant_filter(Browser $browser,$branch)
    {
       $browser ->visit('https://staging.dncchits.in/reports/interactions_subscriber_wise?branch_id='.$branch->id)
                ->select_Possibly_Vacant()
                ->generate_report();
    }
    public function assert_in_interaction_report_in_legal_action_filter(Browser $browser,$branch)
    {
       $browser ->visit('https://staging.dncchits.in/reports/interactions_subscriber_wise?branch_id='.$branch->id)
                ->select_eligible_for_legal()
                ->generate_report();
    }
    public function assert_in_interaction_report_in_recovery_eligible_filter(Browser $browser,$branch)
    {
       $browser ->visit('https://staging.dncchits.in/reports/interactions_subscriber_wise?branch_id='.$branch->id)
                ->select_Eligible_for_Recovery()
                ->generate_report();
    }
}
                
       

