<?php

namespace Tests\Browser\Pages\Traits;

use Laravel\Dusk\Browser;

trait guarantor
{

public function create_a_new_guarantor(Browser $browser, $branch, $guarantor)
{
  $browser->go_to_branches_page_from_menu()->click_branch_name_in_branches_page($branch->name)
  ->click_guarantors_tab_in_branch_ledger()->click_new_guarantor()
  ->fill_guarantor_details_in_guarantor_creation_form($branch, $guarantor)->save();
}

public function click_new_guarantor(Browser $browser){
    $browser->click('#main-container > div.content > div > div > div > div.block-header.border-bottom > a');
}

public function fill_guarantor_details_in_guarantor_creation_form(Browser $browser, $branch, $guarantor) 
{
    $browser->fill_guarantor_details([
        'fullname'          => $guarantor->firstName,
        'branch'            => $branch->id,
        'gender'            => 'Female',
        'dob'               => $guarantor->date($format = 'd-m-Y', $max = '1-1-1995'),
        'mobile_primary' => $guarantor->mobile,
        'mobile_alternate' => '9994448880',
        'pan_no' => 'HDMAW51037',
        'ration_no' => $guarantor->isbn13,
        'aadhaar_no' => $guarantor->ean13,
        'address_present' => $guarantor->buildingNumber,
        'city_present' => $guarantor->city,
        'district_present' => $guarantor->state,
        'state_present' => $guarantor->country,
        'pincode_present' => $guarantor->postcode,
        'address_permanent' => $guarantor->buildingNumber,
        'city_permanent' => $guarantor->city,
        'district_permanent' => $guarantor->state,
        'state_permanent' => $guarantor->country,
        'pincode_permanent' => $guarantor->postcode,
        'source_of_income'  => $guarantor->jobTitle,
        'monthly_income'    => rand(10000, 75000),
        'father_name'       => $guarantor->firstNameMale,
        'mother_name'       => $guarantor->firstNameFemale,
        'spouse_name'       => $guarantor->firstNameFemale,
    ]);

}

public function fill_guarantor_details(Browser $browser, $details)
{
   foreach($details as $key => $value){
       switch ($key) {

        case 'fullname':
        $browser->type('#name', $value);
        break;
        case 'branch':
        $browser->select('branch_id', $value);
        break;
        case 'gender':
        $browser->select('#gender', $value);
        break;
        case 'dob':
        $browser->type('#dob', $value);
        break;
        case 'mobile_primary':
        $browser->type('#mobile_primary', $value);
        break;
        case 'mobile_alternate':
        $browser->type('#mobile_alternate', $value);
        break;
        case 'pan_no':
        $browser->type('#pan_no', $value);
        break;
        case 'ration_no':
        $browser->type('#ration_no', $value);
        break;
        case 'aadhaar_no':
        $browser->type('#aadhaar_no', $value);
        break;
        case 'address_present':
        $browser->type('#address_present', $value);
        break;
        case 'city_present':
        $browser->type('#city_present', $value);
        break;
        case 'district_present':
        $browser->type('#district_present', $value);
        break;
        case 'state_present':
        $browser->type('#state_present', $value);
        break;
        case 'pincode_present':
        $browser->type('#pincode_present', $value);
        break;
        case 'address_permanent':
        $browser->type('#address_permanent', $value);
        break;
        case 'city_permanent':
        $browser->type('#city_permanent', $value);
        break;
        case 'district_permanent':
        $browser->type('#district_permanent', $value);
        break;
        case 'state_permanent':
        $browser->type('#state_permanent', $value);
        break;
        case 'pincode_permanent':
        $browser->type('#pincode_permanent', $value);
        break;
        case 'source_of_income':
        $browser->type('#source_of_income', $value);
        break;
        case 'monthly_income':
        $browser->type('#monthly_income', $value);
        break;
        case 'father_name':
        $browser->type('#father_name', $value);
        break;
        case 'mother_name':
        $browser->type('#mother_name', $value);
        break;
        case 'spouse_name':
        $browser->type('#spouse_name', $value);
        break;
        default:
                # code...
        break;
        }
    }
}  

public function assert_guarantor_created(Browser $browser)
{
    $browser->assertSee('New guarantor created.');
}


public function click_add_documents(Browser $browser){
    $browser->click('main#main-container input[name="document_file"]');
}

public function add__document_in_guarantor_ledger(Browser $browser, $path, $type, $description){
    $browser->go_to_documents_tab()
    ->click_add_document()                
    ->upload_document($path, $type, $description);
}

public function convert_a_guarantor_into_a_subscriber(Browser $browser)
    {
        $browser->clickLink('Convert to Subscriber');
        $browser->assertDialogOpened('Are you sure you want to convert this guarantor into a subscriber?');
        $browser->acceptDialog();
    }
    public function adding_relationship_to_a_guarantor(Browser $browser, $guarantor){
    $browser->clickLink('Add Relationship'); 
    $browser->select_random_relationship(); 
    $browser->type('#main-container input.form-control.entity-search',$guarantor->name)
    ->waitForLink($guarantor->name, 20)->clickLink($guarantor->name)->press('Save');
}

public function get_relationship_details_in_guarantor_ledger(Browser $browser, $relationship)
{

	$relationship->type = $browser->text('#main-container > div:nth-child(3) > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(1)');

	$relationship->name = $browser->text('#main-container > div:nth-child(3) > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(2)');

	$relationship->member_type = $browser->text('#main-container > div:nth-child(3) > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(3)');

	$relationship->phone = $browser->text('#main-container > div:nth-child(3) > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(4)');

	$relationship->enrollments = $browser->text('#main-container > div:nth-child(3) > div:nth-child(4) > div.block-content > table > tbody > tr > td:nth-child(5)');

}

public function assert_relationship_details_after_converting_to_subscriber(Browser $browser, $relationship)
{

	$browser->assertSeeIn('#relationships_tab > table > tbody > tr > td:nth-child(1)', $relationship->type);

	$browser->assertSeeIn('#relationships_tab > table > tbody > tr > td:nth-child(2)', $relationship->name);

	$browser->assertSeeIn('#relationships_tab > table > tbody > tr > td:nth-child(3)', $relationship->member_type);

	$browser->assertSeeIn('#relationships_tab > table > tbody > tr > td:nth-child(4)', $relationship->phone);

	$browser->assertSeeIn('#relationships_tab > table > tbody > tr > td:nth-child(5)', $relationship->enrollments);


}

public function edit_guarantor_details(Browser $browser, $branch, $guarantor)
    {
        $browser->click('#page-container main#main-container div.block-header.border-bottom a.btn.btn-sm.btn-primary')
                ->fill_guarantor_details_in_guarantor_creation_form($branch, $guarantor)->press('Save');
    }

    public function assert_edited_guarantor_details(Browser $browser, $guarantor)
    {
        $browser->assert_guarantor_details($guarantor);
    }

    public function assert_guarantor_details(Browser $browser, $guarantor)
    {
        $browser->assertSeeIn('#main-container div.bg-primary-dark div h2', $guarantor->name);
        $browser->assertInputValue('#page-container main#main-container input[name="mobile_primary"]', $guarantor->mobile);
        $browser->assertInputValue('#pan_no', 'HDMAW51037');
        $browser->assertInputValue('#aadhaar_no', $guarantor->aadhar);
        $browser->assertInputValue('#ration_no', $guarantor->ration);
        $browser->assertInputValue('#gst_no', $guarantor->gst);
        $browser->assertInputValue('#address_present', $guarantor->address_present);
        $browser->assertInputValue('#city_present', $guarantor->city_present);
        $browser->assertInputValue('#district_present', $guarantor->district_present);
        $browser->assertInputValue('#state_present', $guarantor->state_present);
        $browser->assertInputValue('#pincode_present', $guarantor->pincode_present);
        $browser->assertInputValue('#address_permanent', $guarantor->address_permanent);
        $browser->assertInputValue('#city_permanent', $guarantor->city_permanent);
        $browser->assertInputValue('#district_permanent', $guarantor->district_permanent);
        $browser->assertInputValue('#state_permanent', $guarantor->state_permanent);
        $browser->assertInputValue('#pincode_permanent', $guarantor->pincode_permanent);
        $browser->assertInputValue('#source_of_income', $guarantor->source_of_income);
        $browser->assertInputValue('#monthly_income', $guarantor->monthly_income);
        $browser->assertInputValue('#father_name', $guarantor->father_name);
        $browser->assertInputValue('#mother_name', $guarantor->mother_name);
        $browser->assertInputValue('#spouse_name', $guarantor->spouse_name);
    }

}