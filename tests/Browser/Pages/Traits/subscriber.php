<?php

namespace Tests\Browser\Pages\Traits;

use Exception;
use Laravel\Dusk\Browser;

trait subscriber
{
    public function get_subscribers_name(Browser $browser)
    {
        $subscriber_name_zip = $browser->text('#main-container > div.bg-primary-dark > div > h2');
        $subscriber_name_array = explode(' ', $subscriber_name_zip);
        $subscriber_name = $subscriber_name_array[0].$subscriber_name_array[1];
        $browser->subscriber_name = $subscriber_name;
    }

    public function create_a_new_subscriber(Browser $browser, $branch, $subscriber, $document_path)
    {
        $browser->visit('/branches/'.$branch->id.'/subscribers/create')
  ->fill_subscriber_details_in_subscriber_creation_form($branch, $subscriber)
  ->add_subscriber_photo_and_application()->save()
  ->waitForText('New subscriber created.', 60);
    }

    public function adding_relationship_to_a_subscriber(Browser $browser, $subscriber)
    {
        $browser->go_to_relationships_tab_in_subscriber_ledger();
        $browser->clickLink('Add Relationship');
        $browser->select_random_relationship();
        $browser->type('#main-container input.form-control.entity-search', $subscriber->name)
    ->waitForLink($subscriber->name, 20)->clickLink($subscriber->name)->press('Save');
    }

    public function add_subscriber_document_in_subscriber_ledger(Browser $browser)
    {
        $browser->go_to_documents_tab()
    ->click_add_document();
    }

    public function add_subscriber_kyc_documents(Browser $browser, $subscriber)
    {
        $browser->upload_document(1, $subscriber)
                ->upload_document(2, $subscriber)
                ->upload_document(4, $subscriber);
    }

    public function click_add_document(Browser $browser)
    {
        $browser->clickLink('Add Document');
    }

    public function fill_details_for_document_upload(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
                    // case 'name':
                    // $browser->type('name', $value);
                    // break;
            case 'type':
            $browser->select('document_type_id', $value);
            break;
            case 'description':
            $browser->type('description', $value);
            break;
            default:
                    // code...
            break;
        }
        }
    }

    public function go_to_subscriber_details_tab(Browser $browser)
    {
        $browser->click('#enrollment_ledger_tabs > li:nth-child(7) > a');
    }

    public function delete_the_subscriber_documents(Browser $browser)
    {
        $browser->press('#btnGroupTabs1')
    ->clickLink('Edit Details')
    ->press('Delete')
    ->assertSee('Document removed successfully.');
    }

    public function click_new_subscriber(Browser $browser)
    {
        $browser->click('#main-container > div.content > div > div > div > div.block-header.border-bottom > a');
    }

    public function fill_subscriber_details(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
        case 'fullname':
        $browser->type('#name', $value);
        break;
        case 'branch':
        $browser->select('branch_id', $value);
        break;
        case 'gender':
        $browser->select('#gender', $value);
        break;
        case 'dob':
        $browser->type('#dob', $value);
        break;
        case 'mobile_primary':
        $browser->type('#mobile_primary', $value);
        break;
        case 'mobile_alternate':
        $browser->type('#mobile_alternate', $value);
        break;
        case 'pan_no':
        $browser->type('#pan_no', $value);
        break;
        case 'ration_no':
        $browser->type('#ration_no', $value);
        break;
        case 'aadhaar_no':
        $browser->type('#aadhaar_no', $value);
        break;
        case 'address_present':
        $browser->type('#address_present', $value);
        break;
        case 'city_present':
        $browser->type('#city_present', $value);
        break;
        case 'district_present':
        $browser->type('#district_present', $value);
        break;
        case 'state_present':
        $browser->type('#state_present', $value);
        break;
        case 'pincode_present':
        $browser->type('#pincode_present', $value);
        break;
        case 'address_permanent':
        $browser->type('#address_permanent', $value);
        break;
        case 'city_permanent':
        $browser->type('#city_permanent', $value);
        break;
        case 'district_permanent':
        $browser->type('#district_permanent', $value);
        break;
        case 'state_permanent':
        $browser->type('#state_permanent', $value);
        break;
        case 'pincode_permanent':
        $browser->type('#pincode_permanent', $value);
        break;
        case 'source_of_income':
        $browser->type('#source_of_income', $value);
        break;
        case 'monthly_income':
        $browser->type('#monthly_income', $value);
        break;
        case 'father_name':
        $browser->type('#father_name', $value);
        break;
        case 'mother_name':
        $browser->type('#mother_name', $value);
        break;
        case 'spouse_name':
        $browser->type('#spouse_name', $value);
        break;
        default:
                // code...
        break;
        }
        }
    }
        

    public function attach_application_form_in_subscriber_form(Browser $browser, $path)
    {
        $browser->attach('main#main-container div.content div.block form input.form-control-file[name="application_form"]', __DIR__.$path);
    }

public function go_to_subscriber(Browser $browser, $keyword) {
    $browser->type('#search-autocomplete', $keyword)
        ->waitForLink($keyword, 60)
        ->clickLink($keyword);
      }

    public function add_subscriber_document_through_enrollment_ledger(Browser $browser)
    {
        $browser->click('#page-container main#main-container div.content #documents_tab div.row div.col-4.text-right a');
    }

    public function assert_subscriber_details(Browser $browser, $subscriber)
    {
        $browser->assertSeeIn('#main-container div.bg-primary-dark div h2', $subscriber->name);
        $browser->assertInputValue('#page-container main#main-container div#subscriber_details_tab input[name="mobile_primary"]', $subscriber->mobile);
        $browser->assertInputValue('#pan_no', 'HDMAW51037');
        $browser->assertInputValue('#aadhaar_no', $subscriber->aadhar);
        $browser->assertInputValue('#ration_no', $subscriber->ration);
        $browser->assertInputValue('#gst_no', $subscriber->gst);
        $browser->assertInputValue('#address_present', $subscriber->address_present);
        $browser->assertInputValue('#city_present', $subscriber->city_present);
        $browser->assertInputValue('#district_present', $subscriber->district_present);
        $browser->assertInputValue('#state_present', $subscriber->state_present);
        $browser->assertInputValue('#pincode_present', $subscriber->pincode_present);
        $browser->assertInputValue('#address_permanent', $subscriber->address_permanent);
        $browser->assertInputValue('#city_permanent', $subscriber->city_permanent);
        $browser->assertInputValue('#district_permanent', $subscriber->district_permanent);
        $browser->assertInputValue('#state_permanent', $subscriber->state_permanent);
        $browser->assertInputValue('#pincode_permanent', $subscriber->pincode_permanent);
        $browser->assertInputValue('#source_of_income', $subscriber->source_of_income);
        $browser->assertInputValue('#monthly_income', $subscriber->monthly_income);
        $browser->assertInputValue('#father_name', $subscriber->father_name);
        $browser->assertInputValue('#mother_name', $subscriber->mother_name);
        $browser->assertInputValue('#spouse_name', $subscriber->spouse_name);
    }

    public function assert_subscriber_created(Browser $browser)
    {
        $browser->assertSee('New subscriber created.');
    }

    public function click_edit_subscriber_details_in_subscriber_ledger(Browser $browser)
    {
        $browser->clickLink('Edit Subscriber Details');
    }

    public function assert_edited_subscriber_details_saved(Browser $browser)
    {
        $browser->assertSee('Subscriber details saved.');
    }

    public function fill_subscriber_details_in_subscriber_creation_form(Browser $browser, $branch, $subscriber)
    {
        $browser->fill_subscriber_details([
            'fullname' => $subscriber->firstName,
            'branch' => $branch->id,
            'gender' => 'Female',
            'dob' => $subscriber->date($format = 'd-m-Y', $max = '1-1-1995'),
            'mobile_primary' => $subscriber->mobile,
            'mobile_alternate' => '9994448880',
            'pan_no' => 'HDMAW51037',
            'ration_no' => $subscriber->isbn13,
            'aadhaar_no' => $subscriber->ean13,
            'address_present' => $subscriber->buildingNumber,
            'city_present' => $subscriber->city,
            'district_present' => $subscriber->state,
            'state_present' => $subscriber->country,
            'pincode_present' => $subscriber->postcode,
            'address_permanent' => $subscriber->buildingNumber,
            'city_permanent' => $subscriber->city,
            'district_permanent' => $subscriber->state,
            'state_permanent' => $subscriber->country,
            'pincode_permanent' => $subscriber->postcode,
            'source_of_income' => $subscriber->jobTitle,
            'monthly_income' => rand(10000, 75000),
            'father_name' => $subscriber->firstNameMale,
            'mother_name' => $subscriber->firstNameFemale,
            'spouse_name' => $subscriber->firstNameFemale,
        ]);
    }

    public function edit_subscriber_details(Browser $browser, $branch, $subscriber)
    {
        $browser->fill_subscriber_details_in_subscriber_creation_form($branch, $subscriber)->press('Save');
    }

    public function assert_edited_subscriber_details(Browser $browser, $subscriber)
    {
        $browser->assert_subscriber_details($subscriber);
    }

    public function merge_a_subscriber(Browser $browser, $subscriber)
    {
        $browser->clickLink('Merge')->type('main#main-container form  input[name = "new_subscriber"]', $subscriber->mobile)->waitForLink($subscriber->name, 60)->clickLink($subscriber->name)->press('Merge');
    }

    public function assert_same_subscriber_cannot_be_merged(Browser $browser)
    {
        $browser->assertSee('Please select a different subscriber to merge with.');
    }

    public function assert_subscriber_merged(Browser $browser)
    {
        $browser->assertSee('Subscriber successfully merged.');
    }

    public function assert_absence_of_merged_subscriber_in_global_search(Browser $browser, $subscriber)
    {
        $browser->type('#search-autocomplete', $subscriber->mobile)->assertDontSeeLink($subscriber->name);
    }

    public function lock_a_subscriber_profile(Browser $browser)
{
    $browser->radio('#is_locked_true', '1')->assertRadioSelected('#is_locked_true', '1')->assertRadioNotSelected('#is_locked_false', '1')->type('main#main-container form #lock_message', 'Subscriber has been locked');
}

     public function add_subscriber_photo_and_application(Browser $browser)
    {
        $browser->attach('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(22) > div > input', __DIR__.'/Pictures/Aadhar.jpg')
  
        ->attach('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(23) > div > input', __DIR__.'/Pictures/Aadhar.jpg');     
    }

    public function assert_subscriber_locked(Browser $browser)
{
    $browser->assertSee('This account has been locked by an Administrator.');
}

public function assert_locked_subscriber_cannot_be_merged(Browser $browser)
{
    $browser->assertSee("The selected subscriber's account is locked and hence cannot be merged.");
}


public function check_subscriber_status_unlock(Browser $browser){
    $browser->clicklink('Edit Subscriber Details')
            ->radio('is_locked', '0')
            ->press('Save');
}

public function check_subscriber_status_lock(Browser $browser){
    $browser->clicklink('Edit Subscriber Details')
            ->radio('is_locked', '1')
            ->press('Save');
}

public function assert_subscriber_id_generated(Browser $browser, $subscriber)
{
    if(preg_match('/^[a-z0-9 .\-]+$/i', $subscriber->code)) {
        $browser->assertInputValue('#alt_id', $subscriber->code);
    }
    else 
    {
        $error = "Subscriber ID is not generated";
        throw new exception($error);
        }
    }

    public function get_number_of_documents_in_subscriber_ledger(Browser $browser, $document)
{
    $document->count = $browser->elements('#documents_tab > table.table > tbody > tr > td button#btnGroupTabs1');
    $document->count = count($document->count);
}

public function delete_a_particular_document(Browser $browser, $document)
{
    $browser->click('#documents_tab > table.table > tbody > tr:nth-child('.$document->count.') > td button#btnGroupTabs1')->clickLink('Edit Details')->press('Delete');
}

public function assert_document_deleted(Browser $browser)
{
    $browser->assertSee('Document removed successfully.');
}

public function assert_do_not_see_enrollment_removal(Browser $browser){
    $browser->assertDontSee("Removal of Enrollment");
}
public function assert_subscriber_locked_by_admin_to_enroll(Browser $browser){
    $browser->assertSee("The selected subscriber's account is locked and hence cannot be added to the group.");
}

    public function go_to_subscriber_with_id(Browser $browser, $subscriber)
    {
        $browser->visit('/branches/'.$subscriber->branch_id.'/subscribers/'.$subscriber->id);
    }

}


