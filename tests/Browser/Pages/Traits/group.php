<?php

namespace Tests\Browser\Pages\Traits;

use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;

trait group
{
    public function click_new_group(Browser $browser)
    {
        $browser->clickLink('New Group');
    }

    public function create_new_group_in_branch(Browser $browser, $branch, $group, $date)
    {
        $auction_date = explode('-', $date);
        $group->auction_date = $auction_date[0];
        $group->branch = $branch->name;
        $group->branch_id = $branch->id;
        $date = date('d-m-Y');

        if (! isset(($group->scheme))) {
            $group->scheme = rand(1, 24);
        }
        if (! isset(($group->policy))) {
            $group->policy = rand(1, 9);
        }
        if (! isset(($group->auction_date))) {
            $group->auction_date = $date;
        }
        if (! isset(($group->auction_time))) {
            $group->auction_time = '2359';
        }
        if (! isset(($group->pso_no))) {
            $group->pso_no = '000455';
        }
        if (! isset(($group->pso_date))) {
            $group->pso_date = $date;
        }
        if (! isset(($group->bylaw_no))) {
            $group->bylaw_no = '0002555';
        }
        if (! isset(($group->bylaw_date))) {
            $group->bylaw_date = $date;
        }
        if (! isset(($group->fd_amount))) {
            $group->fd_amount = '100000';
        }
        if (! isset(($group->fd_interest))) {
            $group->fd_interest = '20';
        }
        if (! isset(($group->fd_date))) {
            $group->fd_date = $date;
        }
        if (! isset(($group->fd_no))) {
            $group->fd_no = '000155';
        }
        if (! isset(($group->fd_reference_no))) {
            $group->fd_reference_no = '55555556644';
        }
        if (! isset(($group->fd_bank_name))) {
            $group->fd_bank_name = 'Bank of Parota';
        }
        if (! isset(($branch->name))) {
            $branch->name = 'Iraq';
        }
        $browser->visit('/branches/'.$branch->id.'/groups/create')
        ->fill_group_details([
            'Scheme' => $group->scheme,
            'Policy' => $group->policy,
            'MonthyAuctionDate' => $group->auction_date,
            'AuctionTime' => $group->auction_time,
            'PSONo' => $group->pso_no,
            'PSODate' => $date,
            'ByLawNo' => $group->bylaw_no,
            'ByLawDate' => $date,
            'FDAmount' => $group->fd_amount,
            'FDInterest' => $group->fd_interest,
            'FDDate' => $date,
            'FDNumber' => $group->fd_no,
            'FDReferenceNo' => $group->fd_reference_no,
            'FDBankName' => $group->fd_bank_name,
            'FDBranchName' => $branch->name,
        ])
        ->press('Save')->assert_new_group_created();

        $group->name_zip = $browser->text('#main-container > div.bg-primary-dark > div > div > h3');
        $browser->format_group_name($group)
                ->get_group_details($group)->pause(10000)
                ->get_group_details_in_group_details_tab($group);
    }

    public function format_group_name(Browser $browser, $group)
    {
        $group->name_zip = explode(' ', $group->name_zip);
        $group->name = $group->name_zip[1];
    }

    public function fill_group_details(Browser $browser, $group_details)
    {
        foreach ($group_details as $key => $value) {
            switch ($key) {
                case 'Scheme':
                    $browser->select('#scheme_id', $value);
                    break;
                    case 'Policy':
                    $browser->select('#policy_id', $value);
                    break;
                    case 'MonthyAuctionDate':
                    $browser->select('#monthly_auction_date', $value);
                    break;
                    case 'AuctionTime':
                    $browser->type('#auction_time', $value);
                    break;
                    case 'PSONo':
                    $browser->type('#pso_no', $value);
                    break;
                    case 'PSODate':
                    $browser->type('#pso_date', $value);
                    break;
                    case 'ByLawNo':
                    $browser->type('#by_law_no', $value);
                    break;
                    case 'ByLawDate':
                    $browser->type('#by_law_date', $value);
                    break;
                    case 'FDAmount':
                    $browser->type('#fd_amount', $value);
                    break;
                    case 'FDInterest':
                    $browser->type('#fd_interest', $value);
                    break;
                    case 'FDDate':
                    $browser->type('#fd_date', $value);
                    break;
                    case 'FDNumber':
                    $browser->type('#fd_number', $value);
                    break;
                    case 'FDReferenceNo':
                    $browser->type('#fd_reference_no', $value);
                    break;
                    case 'FDBankName':
                    $browser->type('#fd_bank_name', $value);
                    break;
                    case 'FDBranchName':
                    $browser->type('#fd_branch_name', $value);
                    break;
        default:
                // code...
        break;  }
        }
    }

    public function get_group_details(Browser $browser, $group)
    {
        $browser->clicklink('Group Details')
                ->get_group_id($group)
                ->visit('/branches/'.$group->branch_id.'/groups/'.$group->id);

        $group->score = $browser->text('#main-container > div.content > div.row div.block > div.block-content.block-content-full span.font-weight-normal span');

        $group->status = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(2) > div > div > div.font-size-h4.font-w400');

        $group->chit_value = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(3) > div > div > div.font-size-h4.font-w400');
        $group->chit_value = str_replace(',', '', $group->chit_value);

        $group->start_date = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(5) > div > div > div.font-size-h4.font-w400');

        $group->to_be_collected = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(6) > div > div > div.font-size-h4');

        $group->dummy_vacant = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(7) > div > div > div.font-size-h4.font-w400');

        $group->value = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(3) > div > div > div.font-size-h4.font-w400');
        $group->value = str_replace(',', '', $group->value);

        $group->policy_name = $browser->value('#btabs-static-enrollments > div > div > div > div:nth-child(1) > div:nth-child(2) > input');
        $browser->get_number_of_completed_auctions($group);
        $browser->get_group_duration($group);
        $browser->get_calculated_fl($group);

        $group->url = $browser->get_url()->url;
    }

    public function get_group_id(Browser $browser, $group)
    {
        $group->url = $browser->get_url()->url;
        $group_id = explode('/', $group->url);

        $group_branch_id = $group_id[4];
        $group->branch_id = $group_branch_id;
        $group->id = $group_id[6];
    }

    public function go_to_create_new_enrollment_page(Browser $browser, $group)
    {
        $browser->visit('/groups/'.$group->id.'/enrollments/create');
    }

    public function go_to_edit_enrollment_page(Browser $browser, $enrollment)
    {
        $browser->visit('/groups/'.$enrollment->group_id.'/enrollments/'.$enrollment->id.'/edit');
    }

    public function delete_group(Browser $browser, $group)
    {
        $browser->visit('/branches/'.$group->branch_id.'/groups/'.$group->id)
                ->clicklink('Delete')->acceptDialog();
    }

    public function open_group_edit(Browser $browser, $group)
    {
        $browser->visit('/branches/'.$group->branch_id.'/groups/'.$group->id.'/edit');
    }

    public function get_number_of_completed_auctions(Browser $browser, $group)
    {
        $completed_auctions = $browser->text('#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a');
        $completed_auctions = $browser->format_name($completed_auctions)->formatted_name;
        $completed_auctions = explode(' ', $completed_auctions);
        $group->completed_auctions = $completed_auctions[1];
    }

    public function get_group_duration(Browser $browser, $group)
    {
        $duration = $browser->text('#main-container > div.content > div:nth-child(1) > div:nth-child(4) > div > div > div.font-size-h4.font-w400');
        $duration = explode(' ', $duration);
        $group->duration = $duration[0];
    }

    public function get_calculated_fl(Browser $browser, $group)
    {
        $fl_months = $group->duration - $group->completed_auctions;
        $full_due = $group->value / $group->duration;
        $group->fl = $fl_months * $full_due;
    }

    public function get_group_details_with_id(Browser $browser, $group)
    {
        $browser->go_to_group_with_id($group)
                ->get_group_details($group);
    }

    public function go_to_group_enrollments_with_id(Browser $browser, $group)
    {
        $browser->visit('/groups/'.$group->id.'/enrollments');
    }

    public function calculate_group_score(Browser $browser, $group, $group_score)
    {
        $browser->go_to_group_enrollments_with_id($group);
        $enrollment_scores_array = collect($browser->elements('Enrollment Scores of active enrollments array'));

        $enrollment_scores_array = $enrollment_scores_array->map(function ($el) {
            return $el->getText();
        });

        $number_of_enrollments = count($enrollment_scores_array);
        $number_of_enrollments = $number_of_enrollments - 1;

        is_array($enrollment_scores_array);

        $group_score_sum = collect($enrollment_scores_array)->sum();

        $group_score_sum = $group_score_sum - 5;

        $group_score->score = $group_score_sum / $number_of_enrollments;

        $group_score->score = round($group_score->score, 1, PHP_ROUND_HALF_UP);

        $group_score->score_in_erp = $browser->text('Group Score in Enrollments Page');
    }

    public function pick_random_group_l(Browser $browser, $group)
    {
        $branch = (object)[];
        $URL = env('APP_URL');
        $browser->pick_random_branch($branch);
        $browser->visit($URL.'/branches/'.$branch->id.'/groups');
        $group->branch_id = $branch->id;

        $auctions = 20;
        while ($auctions > 18 or $auctions < 1) {
            $rows = $browser->elements('main#main-container div.content div.block div.block-content table.table tbody tr td.font-w600 a');
            $count = count($rows);
            $group_row = $browser->random_function($count)->random;
            $auctions = $browser->text('#main-container > div.content > div.col-lg-12 > div > div.block-content > div > div > table > tbody > tr:nth-child('.$group_row.') > td:nth-child(10)');
        }
        $group_name = $browser->text('#main-container > div.content > div.col-lg-12 > div > div.block-content > div > div > table > tbody > tr:nth-child('.$group_row.') > td:nth-child(1)');

        $group_name = $browser->remove_score_from_name($group_name)->formatted_name;
        $group_name = explode(' ', $group_name);
        $group_name = $group_name[0];
        $browser->clickLink($group_name)->get_group_details($group);
    }

    public function remove_auction_for_enrollment(Browser $browser, $group, $enrollment)
    {
        $browser->visit('/groups/'.$group->id.'/auctions');

        $subscriber_array = collect($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[3]')));
        $subscriber_array = $subscriber_array->map(function ($el) {
            return $el->getText();
        });
        $key = ($subscriber_array->search($enrollment->ticket_number));
        $key = $key + 1;
        $browser->click('#page-container main#main-container div.block table.table tbody tr:nth-child('.$key.') td:nth-child(9) form button');
    }

    public function get_group_details_in_group_details_tab(Browser $browser, $group)
    {
        $browser->clickLink('Group Details');

        /** General Details of group */
        $group->policy = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(2) > input');

        $group->pso_number = $browser->value('#btabs-static-enrollments div:nth-child(2) > div:nth-child(2) > input');

        $group->by_law_number = $browser->value('#btabs-static-enrollments div:nth-child(3) > div:nth-child(2) > input');

        $group->auction_date = $browser->value('#btabs-static-enrollments div:nth-child(4) > div:nth-child(2) > input');

        $group->fd_amount = $browser->value('#btabs-static-enrollments div:nth-child(5) > div:nth-child(2) > input');

        $group->fd_date = $browser->value('#btabs-static-enrollments div:nth-child(6) > div:nth-child(2) > input');

        $group->fd_reference_number = $browser->value('#btabs-static-enrollments div:nth-child(7) > div:nth-child(2) > input');

        $group->fd_branch_name = $browser->value('#btabs-static-enrollments div:nth-child(8) > div:nth-child(2) > input');

        $group->start_date = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->pso_date = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->by_law_date = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->auction_time = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->fd_interest = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->fd_number = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        $group->fd_bank_name = $browser->value('#btabs-static-enrollments div:nth-child(1) > div:nth-child(4) > input');

        /** Policy Details of group */

        $group->policy_name = $browser->value('#btabs-static-enrollments div:nth-child(10) > div > input');

        $group->policy_start_date = $browser->value('#btabs-static-enrollments div:nth-child(11) > div:nth-child(2) > input');

        $group->bonus_percentage = $browser->value('#btabs-static-enrollments div:nth-child(12) > div:nth-child(2) > input');

        $group->penalty_percentage = $browser->value('#btabs-static-enrollments div:nth-child(13) > div:nth-child(2) > input');

        $group->pending_days_limit = $browser->value('#btabs-static-enrollments div:nth-child(14) > div:nth-child(2) > input');

        $group->company_commission = $browser->value('#btabs-static-enrollments div:nth-child(15) > div:nth-child(2) > input');

        $group->employee_commission = $browser->value('#btabs-static-enrollments div:nth-child(16) > div:nth-child(2) > input');

        $group->registration_charge = $browser->value('#btabs-static-enrollments div:nth-child(17) > div:nth-child(2) > input');

        $group->tds_with_pan_number = $browser->value('#btabs-static-enrollments div:nth-child(18) > div:nth-child(2) > input');

        $group->number_of_full_due_months = $browser->value('#btabs-static-enrollments div:nth-child(11) > div:nth-child(4) > input');

        $group->bonus_days_limit = $browser->value('#btabs-static-enrollments div:nth-child(12) > div:nth-child(4) > input');

        $group->penalty_percentage_nps = $browser->value('#btabs-static-enrollments div:nth-child(13) > div:nth-child(4) > input');

        $group->gst = $browser->value('#btabs-static-enrollments div:nth-child(14) > div:nth-child(4) > input');

        $group->agent_commission = $browser->value('#btabs-static-enrollments div:nth-child(15) > div:nth-child(4) > input');

        $group->subscriber_commission = $browser->value('#btabs-static-enrollments div:nth-child(16) > div:nth-child(4) > input');

        $group->documentation_charge = $browser->value('#btabs-static-enrollments div:nth-child(17) > div:nth-child(4) > input');

        $group->tds_without_pan_number = $browser->value('#btabs-static-enrollments div:nth-child(18) > div:nth-child(4) > input');

}

public function get_group_id_from_enrollment_ledger(Browser $browser, $enrollment)
{
    $enrollment->group_id = explode('/', $enrollment->url);
    $enrollment->group_id = $enrollment->group_id[4];
}

}