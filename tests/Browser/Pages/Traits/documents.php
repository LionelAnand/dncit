<?php

namespace Tests\Browser\Pages\Traits;

use Facebook\WebDriver\WebDriverBy;
use Laravel\Dusk\Browser;

trait documents
{
    public function check_subscriber_kyc_document(Browser $browser)
    {
        $browser->go_to_documents_tab();
        $number_of_documents = $browser->elements('#btnGroupTabs1');
        $number_of_documents = count($number_of_documents);
        if ($number_of_documents >= '0') {
            $document_type_array = collect($browser->driver->findElements(WebDriverBy::cssSelector('#documents_tab > table > tbody > tr:nth-child(n) > td:nth-child(2)')));
            $document_type_array = $document_type_array->map(function ($el) {
                return $el->getText();
            });
            if ($document_type_array->contains('Aadhaar')) {
                $key = ($document_type_array->search('Aadhaar'));
                return $key;
                if ($key == 'true') {
                    echo 'Documents Available';
                }
            } else {
                echo 'Document Not Available Please Upload KYC Documents to Enroll';
            }
        }
    }

    public function click_add_document_in_subscriber_ledger(Browser $browser)
    {
        $browser->clickLink('Add Document');
    }

    public function assert_document(Browser $browser, $document_name)
    {
        $browser->assertSee($document_name);
    }

    public function go_to_documents_tab(Browser $browser)
    {
        $browser->clickLink('Documents');
    }


    public function go_to_edit_documents(Browser $browser, $number){
        $browser->click('#documents_tab > table.table > tbody > tr:nth-child('.$number.') > td button#btnGroupTabs1')
                ->clickLink('Edit Details');
    }

    public function assert_remove_documents(Browser $browser){
        $browser->assertsee('Document removed successfully.');
    }

    public function remove_subscriber_kyc_document(Browser $browser){
        $browser->go_to_documents_tab();
        $number_of_documents = $browser->elements('#btnGroupTabs1');
        $number_of_documents = count($number_of_documents); 
        
        while ($number_of_documents>'0'){
        
                    $browser->go_to_edit_documents('1')
                            ->press('input.btn.btn-danger')
                            ->assert_remove_documents();
                    $browser->go_to_documents_tab();
                    $number_of_documents = $browser->elements('#btnGroupTabs1');
                    $number_of_documents = count($number_of_documents);
            } 
        }
    
    public function upload_document(Browser $browser, $doc_type, $member)
    {
        if ($member->type == 'Enrollment') {
           $enrollment_doc_url='https://staging.dncchits.in/documents/create?person_type=App%5CSubscriber&person_id='.$member->subscriber_id.
           '&enrollment_id='.$member->id.'&redirect_url=https%3A%2F%2Fstaging.dncchits.in%2Fgroups%2F'.$member->group_id.'%2Fenrollments%2F'.$member->id;
  
           $browser->visit($enrollment_doc_url);
        } else {
            $browser->visit('https://staging.dncchits.in/documents/create?person_type=App%5C'.$member->type.'&person_id='.$member->id);
        }

        $browser->attach('#main-container > div.content > div > div > div > div.block-content form > div:nth-child(5) > div > input', __DIR__.'/Pictures/Aadhar.jpg')
                ->fill_details_for_document_upload([
                    'type' => $doc_type,
                    'description' => 'Sample Upload',
                ]);
        if (! empty($member->group)) {
            if ($member->type == 'Enrollment') {
                $browser->check('enrollments['.$member->id.']');
            } else {
                $browser->check('enrollments['.$member->enrollment_id.']');
            }
        }
        $browser->press('Save')
                        ->confirm_document_saved();
    }

    public function upload_documents_necessary_for_prized_money_disbursement(Browser $browser, $enrollment)
    {
        $browser->upload_document(4, $enrollment)
                ->upload_document(23, $enrollment)
                ->upload_document(17, $enrollment)
                ->upload_document(25, $enrollment)
                ->upload_document(22, $enrollment)
                ->upload_document(24, $enrollment)
                ->upload_document(26, $enrollment)
                ->upload_document(27, $enrollment);
    }

    public function upload_guarantor_documents_for_prized_money_disbursement(Browser $browser, $guarantor, $subscriber)
    {
        $browser->upload_guarantor_document(17, $guarantor, $subscriber)
                ->upload_guarantor_document(21, $guarantor, $subscriber)
                ->upload_guarantor_document(23, $guarantor, $subscriber)
                ->upload_guarantor_document(3, $guarantor, $subscriber)
                ->upload_guarantor_document(5, $guarantor, $subscriber);
    }

    public function confirm_document_saved(Browser $browser)
    {
        $browser->assertSee('New document saved.');
    }

    public function upload_guarantor_document(Browser $browser, $doc_type, $guarantor, $enrollment)
    {
        $browser->visit('https://staging.dncchits.in/documents/create?person_type=App%5C'.$guarantor->type.'&person_id='.$guarantor->id)
                ->attach('#main-container > div.content > div > div > div > div.block-content form > div:nth-child(5) > div > input', __DIR__.'/Pictures/Aadhar.jpg')
                ->fill_details_for_document_upload([
                    'type' => $doc_type,
                    'description' => 'Sample Upload',
                ]);
        if (! empty($enrollment->group_id)) {
            $browser->check('enrollments['.$enrollment->id.']');
        }
        $browser->press('Save')
                ->confirm_document_saved();
    }
}

