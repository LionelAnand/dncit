<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\documents;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;

class branchesPage extends Page
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function url()
    {
        // return env('APP_URL');
    }

    /****** End of Prized Money disbursement  - Prabu Kannan*******/

    /*******General purpose use Functions*******/



    
    public function elements()
    {
        return [
            'New Group' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered div.block-content div.row div.col-12 table.table.table-sm.table-striped.text-center tbody:nth-child(2) tr:nth-child(1) td.font-w600:nth-child(1) > a:nth-child(1)',
            'charges amount' => 'main#main-container > div:nth-of-type(2) > div > div > div > div:nth-of-type(2) > div > div > form > div:nth-of-type(2) > div > input',
            'delete other charges' => 'main#main-container > div:nth-of-type(2) > div:nth-of-type(3) > div > div:nth-of-type(3) > div:nth-of-type(2) > div > div > table > tbody > tr > td:nth-of-type(9) > form > button > i',
            'Enrollments' =>  '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a',
        ];
    }
}
