<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\documents;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\subscriber;

class prizedMoneyPage extends subscribersPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function fill_in_the_details(Browser $browser, $prized_money_details)
    {
        foreach ($prized_money_details as $key => $value) {
            switch ($key) {/*******Prized Money Disbursement - Prabu Kannan*******/
        case 'prize_money':
           $browser->clear('#amount')
                  ->type('#amount', $value);
          break;
        case 'Enrollment':
          $browser->pause($value);
          break;
        case 'disbursementDate':
             $browser->type('#receipt_date', $value);
          break;
        case 'disbursementTime':
         $browser->type('#receipt_time', $value);
          break;
        case 'type':
         $browser->select('#type', $value);
          break;
        case 'reference_no':
          $browser->type('#reference_no', $value);
          break;
        case 'bank_name':
          $browser->type('#bank_name', $value);
          break;
        case 'branch_name':
         $browser->type('branch_name', $value);
          break;
        case 'processed_date':
         $browser->type('processed_date', $value);
          break;
        case 'realized_date':
         $browser->type('realized_date', $value);
          break;
        case 'notes':
         $browser->type('notes', $value);
          break;/*******Prized Money Disbursement - Prabu Kannan*******/
        default:
                     // code...
          break;
    }
        }
    }

    public function click_disburse_prized_money(Browser $browser)
    {
        $browser->click('Disburse Prize Money');
    }

    public function click_transfer_to_enrollment(Browser $browser)
    {
        $browser->click('Transfer to Enrollment');
    }

    public function uncheck_deduction(Browser $browser)
    {
        $browser->uncheck('deduct_tbc');
    }

    public function check_deduction(Browser $browser)
    {
        $browser->check('deduct_tbc');
    }

    public function click_transfer(Browser $browser)
    {
        $browser->clickLink('Transfer');
    }

    public function check_if_disbursed(Browser $browser, $value)
    {
    }

    public function pause(Browser $browser, $value)
    {
        $browser->pause($value);
    }

    public function remove_disbursement(Browser $browser)
    {
        $browser->press('Remove Disbursement');
    }

    public function assert_disbursed_amount(Browser $browser, $value)
    {
        $browser->assertSee($value);
    }

    /****** End of Prized Money disbursement  - Prabu Kannan*******/

    public function go_to_transfer_to_enrollment(Browser $browser)
    {
        $browser->click('Transfer to Enrollment');
    }

    

    /*
      public function fill_prize_money_adjustment_details(Browser $browser, $details){
        foreach($details as $key => $value){
          switch ($key) {
            case 'adjustment amount':
              $browser->pause(2000)
         //             ->clear('#prize_money')
                      ->type('#prize_money',$value);
              break;
            case 'receipt date':
              $browser->type('#receipt_date',$value);
              break;
            case 'receipt time':
              $browser->type('#receipt_time',$value);
              break;
            default:
                         # code...
              break;
        }
       }
      }
      */

    public function go_to_prize_money_adjustment_and_fill_details(Browser $browser, $subscriber, $link, $adjusted_amount, $receipt_date, $receipt_time)
    {
        $browser->type('enrollment_lookup', $subscriber)
              ->pause(1000)
              ->waitForLink($link, 60)
              ->clickLink($link)
             ->clear('prize money adjustment amount')
              ->type('prize money adjustment amount', $adjusted_amount)
              ->type('#tab-transfer-to-enrollment #receipt_date', $receipt_date)
              ->type('#tab-transfer-to-enrollment #receipt_time', $receipt_time)
              ->pause(1000)
              ->press('Transfer');
    }

    public function fill_subscriber_to_be_adjusted(Browser $browser, $subscriber)
    {
        $browser->type('enrollment_lookup', $subscriber)
      ->pause(1000);
    }

    public function prize_money_adjustment_without_deduction(Browser $browser, $enrollment, $adjusted_amount, $receipt_date)
    {
        $time = date('h:i');
        $browser
                    ->go_to_enrollment_with_id($enrollment)
                    ->clickLink('Prize Money')
                    ->click_disburse_prized_money()
                    ->go_to_transfer_to_enrollment()
                    ->uncheck('#transfer_form > div:nth-child(3) > div > div > div > div > input[type=checkbox]')
                    ->clear('prize money adjustment amount')
                    ->type('prize money adjustment amount', $adjusted_amount)
                    ->type('#transfer_form > div:nth-child(5) > div > input.form-control.entity-search', $enrollment->name)
                    ->waitForLink($enrollment->name, 60)
                    ->clickLink($enrollment->name)
                    ->type('#tab-transfer-to-enrollment #receipt_date', $receipt_date)
                    ->type('#tab-transfer-to-enrollment #receipt_time', $time)
                    ->press('Transfer');
    }

    public function select_subscriber_to_be_adjusted(Browser $browser, $link)
    {
        $browser->waitForLink($link, 60)
                ->clickLink($link);
    }

    public function transfer_prize_money_adjustment_amount(Browser $browser)
    {
        $browser->press('Transfer');
    }

    public function uncheck_deduction_in_prize_money_adjustment(Browser $browser)
    {
        $browser->uncheck('uncheck deduction in prize money adjustment');
    }

    public function press_disburse_prized_money(Browser $browser)
    {
        $browser->press('Disburse Prize Money');
    }

    public function assert_document_charges_after_prize_money_disbursement(Browser $browser)
    {
        $browser->assertSee('Documentation Charge', 'Not Paid');
    }

    public function go_through_central_search(Browser $browser, $keyword, $target)
    {
        $browser->type('#search-autocomplete', $keyword)
            ->pause(12000)
            ->clickLink($target);
    }

    public function assert_prize_money_disbursed(Browser $browser, $disbursed_amount)
    {
        $browser->assertSee('Prize money '.$disbursed_amount.' disbursed.');
    }

    public function remove_disbursement_one(Browser $browser)
    {
        $browser->press('remove_disbursement_one');
    }

    public function assert_disbursement_deleted(Browser $browser)
    {
        $browser->assertSee('removed');
    }

    public function add_documentation_charge(Browser $browser)
    {
        $browser
      ->clickLink('Add Charge')
      ->check('#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(3) > div > div:nth-child(2) > div > div > input[type=checkbox]')
      ->press('Add Charges')
      ->assertSee('Charges added successfully.');
    }

    public function click_subscriber_name(Browser $browser, $subscriber_name)
    {
        $browser->clickLink($subscriber_name);
    }

    // public function go_to_enrollment(Browser $browser, $keyword, $subscriber_name) {
//     $browser->type('#search-autocomplete', $keyword)
//         ->waitForLink($keyword, 60)
//         ->clickLink($keyword)
//     //  ->clickLink('Enrollments')
//         ->clickLink($subscriber_name);

//       }

    public function go_to_group(Browser $browser, $keyword)
    {
        $browser->type('#search-autocomplete', $keyword)
        ->waitForLink($keyword, 60)
        ->clickLink($keyword);
    }

    public function go_to_prize_money_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('Prize Money tab in Enrollment Ledger');
    }

    public function other_charges_tab_in_enrollment_ledger(Browser $browser)
    {
        $browser->click('Other charges tab in enrollment ledger');
    }

    public function elements()
    {
        return [
            /*******PrizedMoneyDisbursements - Prabu Kannan*******/
            'Disburse Prize Money' => '#prize_money_tab > div.row.mb-2 > div > div > div.col-md-6.text-right > a',

            'Transfer to Enrollment' => '#main-container > div.content > div > div > div > div.block-content > div > div > div.block > ul > li:nth-child(2) > a',

            'disbursedAmount' => '#prize_money_tab > div:nth-child(2) > div > table > tbody > tr.table-info > td.text-right',

            'Remove Disbursement' => 'tr:nth-child(2) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)',

            'uncheck deduction in prize money adjustment' => '#tab-transfer-to-enrollment > form > div:nth-child(3) > div > div > div > div > input[type=checkbox]',

            'prize money adjustment amount' => 'main#main-container #tab-transfer-to-enrollment input[name="amount"]',

            'remove_disbursement_one' => '#prize_money_tab > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(8) > form > button',

            'Enrollments' => '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a',

            'Prize Money tab in Enrollment Ledger' => '#enrollment_ledger_tabs > li:nth-child(2) > a',

            'Other charges tab in enrollment ledger' => '#enrollment_ledger_tabs > li:nth-child(3) > a',
        ];
    }
}
