<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;
class employeesPage extends subscribersPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function go_employees(Browser $browser)
    {
        $browser->click('Go Employees');
    }

    public function select_the_employee(Browser $browser, $employee_name)
    {
         $browser->clickLink($employee_name);
    }

    public function go_to_employee(Browser $browser, $branch_name, $employee_name)
    {
        $browser->go_to_branches_page_from_menu()
    ->click_branch_name_in_dashboard($branch_name)
    ->click_employees_tab_in_branch_ledger()
    ->select_the_employee($employee_name);
    }

    public function click_new_employee_button_in_employees_page(Browser $browser)
    {
        $browser->press('New Employee');
    }

    public function assert_to_see_employee_name(Browser $browser, $employee_name)
    {
        $browser->assertsee($employee_name);
    }

    public function assert_to_confirm(Browser $browser, $Text)
    {
        $browser->assertsee($Text);
    }

    public function fill_details_of_employee(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
        case 'fullname':
            if (empty($value)) {
                $browser->type('#name', 'David Danny');
            } else {
                $browser->type('#name', $value);
            }

        break;
        case 'branch':
            if (empty($value)) {
                $browser->select('select_subscriber_branch', '9');
            } else {
                $browser->select('select_subscriber_branch', $value);
            }

        break;
        case 'gender':
            if (empty($value)) {
                $browser->select('#gender', 'Male');
            } else {
                $browser->select('#gender', $value);
            }

        break;
        case 'dob':
            if (empty($value)) {
                $browser->type('#dob', '15081945');
            } else {
                $browser->type('#dob', $value);
            }

        break;
        case 'mobile_primary':
        $browser->type('#mobile_primary', $value);
        break;
        case 'mobile_alternate':
            if (empty($value)) {
                $browser->type('#mobile_alternate', '9999888890');
            } else {
                $browser->type('#mobile_alternate', $value);
            }

        break;
        case 'pan_no':
            if (empty($value)) {
                $browser->type('#pan_no', 'AMZKK5986F');
            } else {
                $browser->type('#pan_no', $value);
            }

        break;
        case 'ration_no':
            if (empty($value)) {
                $browser->type('#ration_no', 'PK0012101');
            } else {
                $browser->type('#ration_no', $value);
            }

        break;
        case 'aadhaar_no':
            if (empty($value)) {
                $browser->type('#aadhaar_no', '1234567890123');
            } else {
                $browser->type('#aadhaar_no', $value);
            }

        break;
        case 'address_present':
            if (empty($value)) {
                $browser->type('#address_present', 'No 1');
            } else {
                $browser->type('#address_present', $value);
            }

        break;
        case 'city_present':
            if (empty($value)) {
                $browser->type('#city_present', 'Sharjah');
            } else {
                $browser->type('#city_present', $value);
            }

        break;
        case 'district_present':
             if (empty($value)) {
                 $browser->type('#district_present', 'Dubai');
             } else {
                 $browser->type('#district_present', $value);
             }

        break;
        case 'state_present':
             if (empty($value)) {
                 $browser->type('#state_present', 'Felicity');
             } else {
                 $browser->type('#state_present', $value);
             }

        break;
        case 'pincode_present':
             if (empty($value)) {
                 $browser->type('#pincode_present', '635962');
             } else {
                 $browser->type('#pincode_present', $value);
             }

        break;
        case 'address_permanent':
             if (empty($value)) {
                 $browser->type('#address_permanent', 'No 7859');
             } else {
                 $browser->type('#address_permanent', $value);
             }

        break;
        case 'city_permanent':
             if (empty($value)) {
                 $browser->type('#city_permanent', 'New York');
             } else {
                 $browser->type('#city_permanent', $value);
             }

        break;
        case 'district_permanent':
             if (empty($value)) {
                 $browser->type('#district_permanent', '5th Avenue');
             } else {
                 $browser->type('#district_permanent', $value);
             }

        break;
        case 'state_permanent':
             if (empty($value)) {
                 $browser->type('#state_permanent', 'Drunken');
             } else {
                 $browser->type('#state_permanent', $value);
             }

        break;
        case 'pincode_permanent':
             if (empty($value)) {
                 $browser->type('#pincode_permanent', '789456');
             } else {
                 $browser->type('#pincode_permanent', $value);
             }

        break;
        case 'source_of_income':
             if (empty($value)) {
                 $browser->type('#source_of_income', 'Dog Chasing');
             } else {
                 $browser->type('#source_of_income', $value);
             }

        break;
        case 'monthly_income':
             if (empty($value)) {
                 $browser->type('#monthly_income', '100000');
             } else {
                 $browser->type('#monthly_income', $value);
             }

        break;
        case "father's_name":
             if (empty($value)) {
                $browser->type('#father_name', 'Appa');
            } else {
                $browser->type('#father_name', $value);
            }

        break;
        case "mother's_name":
             if (empty($value)) {
                $browser->type('#mother_name', 'Amma');
            } else {
                $browser->type('#mother_name', $value);
            }

        break;
        case "spouse's_name":
             if (empty($value)) {
                $browser->type('#spouse_name', 'Spouse');
            } else {
                $browser->type('#spouse_name', $value);
            }

        break;
        default:
        // code...
        break;
      }
        }
    }

    /******* New Employee Addition - Prabu Kannan*******/

    /*******User Access Provision *******/
    public function click_manage_user_access(Browser $browser)
    {
        $browser->clicklink('Details')
            ->clickLink('Manage User Access');
    }

    public function click_provision_user_access(Browser $browser)
    {
        $browser->clicklink('Details')
                ->clickLink('Provision User Access');
    }

    public function check_group_admin(Browser $browser)
    {
        $browser->check('Group Admin Checkbox');
    }

    public function uncheck_group_admin(Browser $browser)
    {
        $browser->uncheck('Group Admin Checkbox');
    }

    /*******User Access Provision *******/

    /*******Employee Edit Test *******/
    public function edit_details(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
        case 'fullname':
        $browser->type('#name', $value);
        break;
        case 'branch':
        $browser->select('branch', $value);
        break;
        case 'gender':
        $browser->select('#gender', $value);
        break;
        case 'dob':
        $browser->type('#dob', $value);
        break;
        case 'mobile_primary':
        $browser->type('#mobile_primary', $value);
        break;
        case 'mobile_alternate':
        $browser->type('#mobile_alternate', $value);
        break;
        case 'pan_no':
        $browser->type('#pan_no', $value);
        break;
        case 'ration_no':
        $browser->type('#ration_no', $value);
        break;
        case 'aadhaar_no':
        $browser->type('#aadhaar_no', $value);
        break;
        case 'address_present':
        $browser->type('#address_present', $value);
        break;
        case 'city_present':
        $browser->type('#city_present', $value);
        break;
        case 'district_present':
        $browser->type('#district_present', $value);
        break;
        case 'state_present':
        $browser->type('#state_present', $value);
        break;
        case 'pincode_present':
        $browser->type('#pincode_present', $value);
        break;
        case 'address_permanent':
        $browser->type('#address_permanent', $value);
        break;
        case 'city_permanent':
        $browser->type('#city_permanent', $value);
        break;
        case 'district_permanent':
        $browser->type('#district_permanent', $value);
        break;
        case 'state_permanent':
        $browser->type('#state_permanent', $value);
        break;
        case 'pincode_permanent':
        $browser->type('#pincode_permanent', $value);
        break;
        case 'editEmployeeBranch':
        $browser->select('editEmployeeBranch', $value);
        break;
        case 'employeeBranch':
        $browser->select('employeeBranch', $value);
        break;
        case 'source of income':
        $browser->type('#source_of_income', $value);
        break;
        case 'monthly income':
        $browser->type('#monthly_income', $value);
        break;
        case 'father name':
        $browser->type('#father_name', $value);
        break;
        case 'mother name':
        $browser->type('#mother_name', $value);
        break;
        case 'spouse name':
        $browser->type('#spouse_name', $value);
        break;
        default:
            // code...
        break;
      }
        }
    }

    /*******Employee Edit Test *******/
    public function change_password(Browser $browser, $new_password)
    {
        $browser->clickLink('Details')
                ->click_manage_user_access()
                ->type('#password', $new_password)
                ->save();
    }

    public function change_password_for_this_employee(Browser $browser, $employee, $new_password)
    {
        $browser->search_and_go_to_employee($employee)
                ->change_password($new_password);
    }

    public function add_group_admin_rights(Browser $browser, $branch, $employee_name, $new_password)
    {//password changing to avoid future test failures
        $browser->go_to_employee($branch, $employee_name)
      ->click('Details in Employee Page')
      ->click_manage_user_access()
      ->uncheck_group_admin()
      ->type('#password', $new_password)
      ->save()
      ->assert_to_confirm('User details saved successfully.');
    }

    public function remove_group_admin_rights(Browser $browser, $branch, $employee_name, $new_password)
    {//password changing to avoid future test failures
        $browser->go_to_employee($branch, $employee_name)
      ->click('Details in Employee Page')
      ->click_manage_user_access()
      ->check_group_admin()
      ->type('#password', $new_password)
      ->save()
      ->assert_to_confirm('User details saved successfully.');
    }

    public function transfer_all_enrollments_from_one_employee_to_other_employee(Browser $browser, $branch_name,$employee_name,$keyword, $target)
    {
        $browser->go_to_employee($branch_name, $employee_name)->press('Press Transfer Button')
                ->type('Employee Input Field', $keyword)
                ->waitForLink($target, 60)
                ->clickLink($target)
                ->press('Transfer')
                ->assertsee('enrollments transferred');
    }

    public function go_to_3rd_page(Browser $browser)
    {
        $browser->visit('/branches/1/employees?page=3');
    }

    public function remove_employee(Browser $browser)
    {
        $browser->press('button#btnGroupTabs1')
            ->clickLink('Remove Employee');
    }

    public function dont_transfer_enrollments_when_employee_not_selected_from_dropdown_list(Browser $browser, $branch_name,$employee_name,$keyword)
    {
        $browser->go_to_employee($branch_name, $employee_name)->press('Press Transfer Button')
                ->type('Employee Input Field', $keyword)
                ->press('Transfer')
                ->assertDontsee('enrollments transferred');
    }

    public function fill_in_credentials(Browser $browser, $email, $password)
    {
        $browser->type('#email_prefix', $email)
            ->type('#password', $password);
    }

    public function click_provision_button(Browser $browser)
    {
        $browser->press('Provision');
    }

    public function delete_user_access(Browser $browser)
    {
        $browser->clickLink('Details')
                ->clickLink('Manage User Access')
                ->press('Delete')
                ->acceptDialog()
                ->assertSee('User removed successfully.');
    }

    public function delete_user_access_for(Browser $browser, $branch_name, $employee_name)
    {
        $browser->go_to_employee($branch_name, $employee_name)
                ->clickLink('Details')
                ->clickLink('Manage User Access')
                ->press('Delete')
                ->acceptDialog()
                ->assertSee('User removed successfully.');
    }

    public function provision_user_access(Browser $browser, $userid, $password)
    {
        $browser->clickLink('Details')
        ->click_provision_user_access()
                        ->fill_in_credentials($userid, $password)
                        ->check_group_admin()
                        ->click_provision_button()
                        ->assert_to_confirm('User provisioned successfully.');
    }

    public function elements()
    {
        return [
            /*******User Access Provision - Prabu Kannan*******/
            'Group Admin Checkbox' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > div:nth-child(2) > div > div > input[type=checkbox]',
            /*******User Access Provision - Prabu Kannan*******/

            /*******Employee Edit Test  - Prabu Kannan*******/
            'editEmployeeBranch' => '#main-container > div.content > div > div > div > div.block-content > div > div > div > form > div:nth-child(4) > div > select',
            /*******Employee Edit Test  - Prabu Kannan*******/
            /*******New Employee Addition - Prabu Kannan*******/
            // 'Employees'               =>  '#main-container > div.content > div:nth-child(2) > div > div > div.block-content > div > div:nth-child(4) > a > div > div.font-size-sm.font-w600.text-uppercase.text-muted',
            'New Employee' => '#main-container > div.content > div > div > div > div.block-header.border-bottom > a',
            'employeeBranch' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(3) > div > select',
            /*******New Employee Addition - Prabu Kannan*******/
            'Press Transfer Button' => '#managing_enrollments_tab > p > a.btn.btn-sm.btn-primary.js-tooltip-enabled',
            'Employee Input Field' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search',
            'Go Employees' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content:nth-child(2) div.row:nth-child(1) div.col-md-6.col-12:nth-child(1) div.block.block-transparent div.block-content div.row div.col-md-4.col-4:nth-child(5) a.block.block-rounded.block-link-pop.border-left.border-primary.border-4x div.block-content.block-content-full > div.font-size-sm.font-w600.text-uppercase.text-muted',

            'Details in Employee Page' => '#employee_tabs > li:nth-child(4) > a'
        ];
    }
}
