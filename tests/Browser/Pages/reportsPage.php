<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;

class reportsPage extends collectionsPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;


    public function click_business(Browser $browser)
    {
        $browser->clickLink('Business')
        ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a > span');
    }

    public function select_business_groupwise(Browser $browser)
    {
        $browser->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(7) > a > span')
        ->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(7) > ul > li:nth-of-type(2) > a > span');
    }

    public function click_introducer_commission_in_menu_group_wise(Browser $browser)
    {
        $browser->click('Introducer Comm.')
        ->clickLink('Introducer Wise');
    }

    public function enter_enrollment_date(Browser $browser, $from_date, $to_date)
    {
        $browser->type('#enrollment_from', $from_date)
        ->type('#enrollment_to', $to_date);
    }

    public function select_collection(Browser $browser)
    {
        $browser->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(5) > a > span')
        ->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(5) > ul > li:nth-of-type(2) > a > span');
    }

    public function select_collection_summary(Browser $browser)
    {
        $browser->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(5) > a > span')
        ->click('nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(5) > ul > li > a > span');
    }

    public function collection_date(Browser $browser, $from_date, $to_date, $to_time, $pay_type)
    {
        $browser->type('input#from_date', $from_date)
        ->type('input#to_date', $to_date)
        ->type('input#to_time', $to_time)
        ->select('#payment_type', $pay_type);
    }

    public function generate_report(Browser $browser)
    {
        $browser->press('main#main-container > div:nth-of-type(2) > div > div > div > div > form > div > div:nth-of-type(6) > button');
    }

    public function go_to_employee_wise_tbc(Browser $browser)
    {
        $browser->clickLink('To Be Collected')
        ->clickLink('Subscriber Wise');
    }

    public function fill_subscriber_tbc_report_inputs(Browser $browser, $details)
    {
        foreach ($details as $key => $value) {
            switch ($key) {
                case 'branch':
                $browser->select('select#branch_id', $value);
                break;
                case 'employee':
                $browser->select('select#employee_id', $value);
                break;
                case 'payment days':
                $browser->type('input#payment_days', $value);
                break;
                case 'TBC%':
                $browser->type('input#to_be_collected_percent', $value);
                break;
                case 'prized status':
                $browser->select('#prized_status', $value);
                break;
                default:
                    // code...
                break;
            }
        }
    }

    public function assert_that_subscriber_wise_tbc_report_is_generated(Browser $browser)
    {
        $browser->assertSee('Additional columns available in Export: Branch, Mobile, Last Auction Date, Chit Value, Alt. Group Name, Enrollment Date, Address (Present), Address (Permanent), IDs (Branch, Subscriber, Group, Enrollment)');
    }

    public function go_to_group_wise_ATO_report(Browser $browser)
    {
        $browser->clickLink('Business')
                ->clickLink('ATO - Group Wise');
    }

    public function select_the_branch_by_providing_branch_id(Browser $browser, $branch)
    {
        $browser->select('#branch_id', $branch);
    }

    public function select_from_and_to_dates(Browser $browser, $from_date, $to_date)
    {
        $browser->type('#first_auction_from', $from_date)
        ->type('#first_auction_to', $to_date);
    }

    public function select_auctions_datewise(Browser $browser)
    {
        $browser->click('Auctions')
            ->click('Auction Date Wise');  //click on Auction Date Wise
    }

    public function enter_from_date(Browser $browser, $from_date)
    {
        $browser->type('#from_date', $from_date);   //Enter from Dates on reports
    }

    public function enter_to_date(Browser $browser, $to_date)
    {
        $browser->type('#to_date', $to_date);   //Enter to Dates on reports
    }

    public function clear_date_Filters(Browser $browser)
    {
        $browser->clear('#from_date')
            ->clear('input#to_date');   //Clear Dates in Filters on reports
    }

    public function select_status(Browser $browser, $status)
    {
        $browser->select('#group_status', $status);
    }

    public function click_group_name_in_current_page(Browser $browser, $group_name)
    {
        $browser->clicklink($group_name);
    }

    
  

    public function exit(Browser $browser)
    {
        $browser->assertDontSee('DNC');
    }

    public function select_vacancy_groupwise_report(Browser $browser)
    {
        $browser->clicklink('Vacancy')
            ->click('Vacancy Group Wise');
    }

    public function select_vacancy(Browser $browser, $vacancy)
    {
        $browser->select('select#result_type', $vacancy);
    }

    public function select_dummy(Browser $browser, $dummy)
    {
        $browser->select('select#result_type', $dummy);
    }

    public function goto_undisbursed_prize_money_report(Browser $browser)
    {
        $browser->clicklink('Undisbursed Pr. Mn.')
            ->click('Group Wise');
    }

    public function goto_tobe_collected_groupwise_report(Browser $browser)
    {
        $browser->clicklink('To Be Collected')
            ->click('Group Wise');
    }

    public function go_to_ato_subscriber_wise_business_report(Browser $browser)
    {
        $browser->clickLink('Business')
            ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a > span');
    }

    public function go_to_ETO_subscriber_wise_business_report(Browser $browser)
    {
        $browser->clickLink('Business')
            ->click('#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(4) > a > span');
    }

    public function go_to_subscriber_wise_ETO_report(Browser $browser)
    {
        $browser->clickLink('Business')
            ->clickLink('ETO - Subscriber Wise');
    }

    public function ETO_enrollement_date_between(Browser $browser, $status, $enroll_from, $enroll_to)
    {
        $browser->select('#group_status', $status)
            ->type('#enrollment_from', $enroll_from)
            ->type('#enrollment_to', $enroll_to);
    }

    public function select_enrollments(Browser $browser)
    {
        $browser->clicklink('Enrollments');
    }

    public function select_Due_for_renewal(Browser $browser)
    {
        $browser->clicklink('Due for Renewal');
    }

    public function select_interactions(Browser $browser)
    {
        $browser->clicklink('Interactions');
    }

    public function select_interactions_subscriber_wise(Browser $browser)
    {
        $browser->clicklink('Susbcriber Wise');
    }

    public function check_possibly_vacant(Browser $browser)
    {
        $browser->check('input#is_possibly_vacant_status')
            ->uncheck('input#is_recovery_eligible_status')
            ->uncheck('input#is_legal_eligible_status');
    }

    public function check_eligible_for_recovery(Browser $browser)
    {
        $browser->uncheck('input#is_possibly_vacant_status')
            ->check('input#is_recovery_eligible_status')
            ->uncheck('input#is_legal_eligible_status');
    }

    public function check_eligible_for_legal_action(Browser $browser)
    {
        $browser->uncheck('input#is_possibly_vacant_status')
            ->uncheck('input#is_recovery_eligible_status')
            ->check('input#is_legal_eligible_status');

        }

    public function goto_subscriber_wise_tbc_report(Browser $browser)
    {
        $browser->clickLink('To Be Collected')
            ->click('Subscriber Wise Tbc');
    }

    public function Min_Payment_days(Browser $browser, $min_payment_days)
    {
        $browser->type('input#payment_days', $min_payment_days);
    }

    public function generate_groupwise_ATO_report(Browser $browser, $data)
    {
        $browser->go_to_group_wise_ATO_report()
            ->select_the_branch_by_providing_branch_id($data->branch->branch_id)
            ->select_status($data->status)
            ->select_from_and_to_dates($data->date, $data->date)
            ->click_generate_in_reports()
            ->pause(1000)
            ->assertSee('Export')
            ->export_the_report();
    }

    public function go_to_subscriber_wise_ato_report(Browser $browser)
    {
        $browser->clickLink('Business')
            ->clickLink('ATO - Subscriber Wise');
    }

    public function choose_branch(Browser $browser, $branch)
    {
        $browser->select('#branch_id', $branch);
    }

    public function elements()
    {
        return [
            //'Business'          =>  'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(7) > a',
            'Group Wise' => '#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(7) > ul > li:nth-of-type(2) > a > span',
            'Business Group Wise' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(7) > ul > li:nth-of-type(2) > a > span',
            'Subscriber Wise Business' => '#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(3) > a > span',
            'Auctions' => '#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li:nth-child(11) > a > span',
            'Auction Date Wise' => '#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li > a > span',
            'enrollment' => 'main#main-container > div:nth-of-type(2) > div:nth-of-type(2) > div > div > ul > li:nth-of-type(2) > a',
            'Vacancy Group Wise' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(10) > ul > li > a > span',
            'Subscriber Wise Tbc' => '#sidebar > div.simplebar-scroll-content > div > div.content-side.content-side-full > ul > li.nav-main-item.open > ul > li:nth-child(1) > a',

            'Group Wise IntroducerIntroducer Commission' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(12) > ul > li:nth-of-type(2) > a > span',
            'ETO subscriber wise' => 'nav#sidebar > div:nth-of-type(3) > div > div:nth-of-type(2) > ul > li:nth-of-type(6) > ul > li:nth-of-type(4) > a > span',
        ];
    }
}
