<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;
use Tests\Browser\Pages\Traits\documents;

class subscribersPage extends auctionsPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function url()
    {
        //  return env('APP_URL');
    }

    public function select_subscribers(Browser $browser){
        $browser->clickLink('Subscribers');
    }

    public function select_subscriber_branch(Browser $browser, $subscriber_branch){
        $browser->select('select_subscriber_branch', $subscriber_branch);
    }

    public function click_subscriber_detials_tab(Browser $browser){
        $browser->clickLink('Subscriber Details Tab');
    }

    public function save_subscriber(Browser $browser){
        $browser->press('Save');
    }

    public function assert_subscriber_code_before_creation(Browser $browser, $subscriber_code){
        $browser->assertDontSee($subscriber_code);
    }

    public function go_to_subscribers_page_of_branch(Browser $browser, $branch){
        $browser->clickLink('Branches')
                ->clickLink($branch)
                ->clickLink('Subscribers');
    }
    

    public function go_to_subscribers(Browser $browser, $branch, $subscriber){
        $browser->clickLink('Branches')
                ->clickLink($branch)
                ->clickLink('Subscribers')
                ->clickLink($subscriber);
    }

    public function elements(){
        return [
            'branch' => 'div.form-group.row:nth-child(4) div.col-sm-4',
            'branch_id' => '#main-container div:nth-child(4) > div > select',
            'select_subscriber_branch' => 'div.form-group.row:nth-child(4) div.col-sm-4',
            'Primary Subscriber' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(2) > div > input.form-control.entity-search',
            'Subscriber Details Tab' => '#enrollment_ledger_tabs > li:nth-child(7) > a',
        ];
    }
}