<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\documents;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;

class auctionsPage extends groupsPage
{
    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

    public function add_first_auction_entry(Browser $browser, $group_name, $auction_date)
    {
        $browser->go_to_group($group_name)
                    ->go_to_auctions_tab_in_group_ledger()
                    ->click('#main-container > div.content > div:nth-child(2) > div > div > ul > li.nav-item.ml-auto > div > a')
                    ->type('#auction_date', $auction_date)
                    ->press('Save');
    }

    public function add_auction_entry(Browser $browser, $auction_date, $bid_amount)
    {
        $browser->go_to_auctions_tab_in_group_ledger()
                 ->click('#main-container > div.content > div:nth-child(2) > div > div > ul > li.nav-item.ml-auto > div > a')
                 ->type('#gross_bid_amount', $bid_amount)
                 ->type('#auction_date', $auction_date)
                 ->press('Save');
    }

    public function add_auction_entry_for_liened_enrollment(Browser $browser, $auction_date, $winning_enrollment_id, $bid_amount)
    {
        $browser->go_to_auctions_tab_in_group_ledger()
            ->press('Add Auction')
            ->type('#auction_date', $auction_date)
            ->select('#winning_enrollment_id', $winning_enrollment_id)
            ->type('#gross_bid_amount', $bid_amount)
            ->press('Save');
    }

    public function add_auction_entry_for_locked_subscriber(Browser $browser, $group, $auction_date, $winning_enrollment_id, $bid_amount)
    {
        $browser->go_to_group($group->name)
                ->go_to_auctions_tab_in_group_ledger()
                ->press('Add Auction')
                ->type('#auction_date', $auction_date)
                ->select('#winning_enrollment_id', $winning_enrollment_id)
                ->type('#gross_bid_amount', $bid_amount)
                ->press('Save');
    }

    public function add_auction_entry_for_unlocked_subscriber(Browser $browser, $group, $auction_date, $winning_enrollment_id, $bid_amount)
    {
        $browser->go_to_group($group->name)
            ->go_to_auctions_tab_in_group_ledger()
            ->press('Add Auction')
            ->type('#auction_date', $auction_date)
            ->select('#winning_enrollment_id', $winning_enrollment_id)
            ->type('#gross_bid_amount', $bid_amount)
            ->press('Save');
    }

    public function go_to_winner_enrollment_ledger(Browser $browser, $auction_winner_ledger)
    {
        $browser->clickLink($auction_winner_ledger);
    }

    public function remove_Prized_auction(Browser $browser)
    {
        $browser->press('Remove Auction')
            ->pause(2000);
    }

    public function remove_auction_number_twenty(Browser $browser)
    {
        $browser->click('#btabs-static-enrollments > table > tbody > tr:nth-child(20) > td:nth-child(9) > form > button > i')
            ->pause(2000);
    }

    public function click_subscriber_in_current_page(Browser $browser, $subscriber_name)
    {
        $browser->clickLink($subscriber_name);
    }

    public function remove_document_charge_receipt(Browser $browser)
    {
        $browser->clickLink('Document Charge Receipt');
        $window = collect($browser->driver->getWindowHandles())->last();
        $browser->driver->switchTo()->window($window);
        $browser->clickLink('Delete');
    }

    public function delete_the_document_charge_receipt(Browser $browser)
    {
        $browser->clickLink('Delete');
    }

    public function remove_documentation_charge_row_two(Browser $browser)
    {
        $browser->press('Remove Documentation Charge row two')
            ->pause(2000);
    }

    public function remove_other_charges_row_one(Browser $browser)
    {
        $browser->press('Remove other charges row one')
            ->pause(1000);
    }

    public function delete_the_prize_money_adjustment_receipt(Browser $browser)
    {
        $browser->clickLink('Delete');
    }

    public function remove_other_charges_row_two(Browser $browser)
    {
        $browser->press('Remove Other Charges Row Two')
            ->pause(1000);
    }

    public function remove_alerts_after_auctions(Browser $browser)
    {
        $browser->press('Remove Alerts After Auction');
    }

    public function remove_particular_auction(Browser $browser, $auction_number)
    {
        $browser->click('#btabs-static-enrollments > table > tbody > tr:nth-child('.$auction_number.') > td:nth-child(9) > form > button > i')
                ->waitForText('removed.',120);
    }

    public function get_particular_auctions_disbursement_amount(Browser $browser, $disbursement_amount)
    {
        $browser->text('#btabs-static-enrollments > table > tbody > tr:nth-child('.$disbursement_amount.') > td:nth-child(7)')
            ->pause(2000);
    }

    public function remove_the_last_auction(Browser $browser, $group)
    {
        $browser->visit('/groups/'.$group->id.'/auctions');
        $el = $browser->elements('#btabs-static-enrollments > table > tbody > tr> td:nth-child(9) > form > button > i');
        $auction_number = count($el);
        $browser->remove_particular_auction($auction_number)
            ->assert_auction_removed();
    }

    public function elements()
    {
        return [
            'Bid Amount' => 'input[name="bid_amount"]',
            'Add Auction' => '#main-container > div.content > div:nth-child(2) > div > div > ul > li.nav-item.ml-auto > div > a',
            'Remove Auction' => '#btabs-static-enrollments > table > tbody > tr:nth-child(3) > td:nth-child(9) > form > button > i',
            'Reove Auction 18' => 'tr:nth-child(18) > td:nth-child(9) > form > button > i',
            'Remove Documentation Charge' => '#main-container > div.content > div > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',
            'Other Charges segment' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) main:nth-child(5) div.content div.row div.col-lg-12 > div.block.block-rounded.block-bordered:nth-child(4)',
            'Document Charge Row' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(4) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tbody:nth-child(2) > tr:nth-child(2)',
            'Document Charge Receipt' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(4) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tbody:nth-child(2) tr:nth-child(2) td.text-center:nth-child(8) small:nth-child(1) > a:nth-child(1)',
            'remove document charge in other charges column' => '#main-container > div.content > div > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',
            'Remove Disbursement' => 'tr:nth-child(2) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)',
            'Remove Documentation Charge row two' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',

            'Remove other charges row one' => '#other_charges_tab > div > div > table > tbody > tr:nth-child(1) > td:nth-child(9) > form > button',

            'Disbursed Amount in Auctions Page Row Four' => '#btabs-static-enrollments > table > tbody > tr:nth-child(4) > td:nth-child(7)',
            'Disbursement Removal Elements' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div:nth-child(2) > div > table > tbody > tr > td:nth-child(8) > form > button',
            'Number Of Auctions' => '#btabs-static-enrollments > table > tbody > tr > td:nth-child(9) > form > button',
            'Receipt Number in Other Charges Row Two' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(8) > small > a',
            'Remove Auction Six' => '#btabs-static-enrollments > table > tbody > tr:nth-child(6) > td:nth-child(9) > form > button',

            'Pending Days for Prized - Policy 1B' => '#settlements-table > tbody > tr:nth-child(5) > td:nth-child(8), #settlements-table > tbody > tr:nth-child(6) > td:nth-child(8)',

            'Due Amount for Prized - Policy 1B' => '#settlements-table > tbody > tr:nth-child(5) > td:nth-child(3), #settlements-table > tbody > tr:nth-child(6) > td:nth-child(3)',

            'New Penalty for Prized - Policy 1B' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',

            'Pending Days for NP Policy 2' => '#settlements-table > tbody > tr:nth-child(3) > td:nth-child(8)',

            'Due Amount for NP Policy 2' => 'settlements-table > tbody > tr:nth-child(3) > td:nth-child(3)',

            'New Penalty for NP Policy 2' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',

            'Add Auction duplicate' => '#main-container > div.content > div:nth-child(3) > div > div > ul > li.nav-item.ml-auto > div > a',

            'Remove Alerts After Auction' => '#main-container > div.content > div:nth-child(1) > div > div > button',
            'Bid Amount' => 'input[name="bid_amount"]',
            // 'Add Auction' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row:nth-child(2) div.col-lg-12 div.block.block-rounded.block-bordered ul.nav.nav-tabs.nav-tabs-alt.nav-tabs-block.align-items-center li.nav-item.ml-auto:nth-child(4) div.btn-group.btn-group-sm.pr-2 > a.btn.btn-primary',
            'Remove Auction' => '#btabs-static-enrollments > table > tbody > tr:nth-child(3) > td:nth-child(9) > form > button > i',
            'Remove Documentation Charge' => '#main-container > div.content > div > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',
            'Other Charges segment' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) main:nth-child(5) div.content div.row div.col-lg-12 > div.block.block-rounded.block-bordered:nth-child(4)',
            'Document Charge Row' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(4) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tbody:nth-child(2) > tr:nth-child(2)',
            'Document Charge Receipt' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(4) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tbody:nth-child(2) tr:nth-child(2) td.text-center:nth-child(8) small:nth-child(1) > a:nth-child(1)',
            'remove document charge in other charges column' => '#main-container > div.content > div > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',
            'Remove Disbursement' => 'tr:nth-child(2) td:nth-child(8) form.d-inline >.ml-2:nth-child(3)',

            'Remove Documentation Charge row one' => '#other_charges_tab > div > div > table > tbody > tr:nth-child(1) > td:nth-child(9) > form > button',

            'Remove Documentation Charge row two' => '#other_charges_tab > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button',

            'Disbursed Amount in Auctions Page Row Four' => '#btabs-static-enrollments > table > tbody > tr:nth-child(4) > td:nth-child(7)',
            'Disbursement Removal Elements' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div:nth-child(2) > div > table > tbody > tr > td:nth-child(8) > form > button',
            'Number Of Auctions' => '#btabs-static-enrollments > table > tbody > tr > td:nth-child(9) > form > button',
            'Receipt Number in Other Charges Row Two' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(8) > small > a',
            'Remove Other Charges Row Two' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button > i',

            'Pending Days for NP 1B' => '#settlements-table > tbody > tr:nth-child(8) > td:nth-child(8)',
            'Due Amount for NP 1B' => '#settlements-table > tbody > tr:nth-child(8) > td:nth-child(3)',
            'New Penalty for NP 1B' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',
            'Pending Days for Prized - Policy 1B' => '#settlements-table > tbody > tr:nth-child(5) > td:nth-child(8), #settlements-table > tbody > tr:nth-child(6) > td:nth-child(8)',
            'Due Amount for Prized - Policy 1B' => '#settlements-table > tbody > tr:nth-child(5) > td:nth-child(3), #settlements-table > tbody > tr:nth-child(6) > td:nth-child(3)',
            'New Penalty for Prized - Policy 1B' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',
            'Pending Days for NP Policy 2' => '#settlements-table > tbody > tr:nth-child(3) > td:nth-child(8)',
            'Due Amount for NP Policy 2' => 'settlements-table > tbody > tr:nth-child(3) > td:nth-child(3)',
            'New Penalty for NP Policy 2' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',
            'Remove Auction Seven' => '#btabs-static-enrollments > table > tbody > tr:nth-child(7) > td:nth-child(9) > form > button',

            'Other Charges Not Paid' => '#other_charges_tab > div > div > table > tbody > tr > td:nth-child(9) > form > button',

            'To Be Collected Total in Enrollment Ledger' => '#settlements-table > tbody > tr.table-info > td.text-right.bg-warning',

            'Prized Money Disbursed in Enrollment Ledger' => '#prize_money_tab > div.row.mb-2 > div > table > tbody > tr.table-info > td.text-right',

            'Undisbursed Prized Money in Enrollment Ledger' => '#prize_money_tab > div.row.mb-2 > div > table > tbody > tr.table-warning > td.text-right',

            'Number of Other Charges in Enrollment Ledger' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr > td:nth-child(9) > form > button > i',

            'Number of Disbursements' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div:nth-child(2) > div > table > tbody > tr > td:nth-child(8) > form > button',

            'Enrollments' => '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(2) > a',

            'FL in Enrollment Ledger for Non Prized Subscriber' => '#main-container > div.content > div:nth-child(2) > div:nth-child(4) > div > div > div.font-size-h4.font-w400.text-warning',

            'FL in Enrollment Ledger for Prized Subscriber' => '#main-container > div.content > div:nth-child(2) > div:nth-child(7) > div > div > div.font-size-h4.font-w400.text-warning',

            'Disburse Prize Money' => 'main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > a',

            'Receipt Number in Other Charges Row 2' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(8) > small > a',

            'Other charges tab in enrollment ledger' => '#enrollment_ledger_tabs > li:nth-child(3) > a',

            'Prize Money tab in enrollment ledger' => '#enrollment_ledger_tabs > li:nth-child(2) > a',

            'Drop down in Collection' => '#dropdown-default-outline-primary',

            'Regenerate Settlements' => '#ledger_tab > div.text-right > div > div > a:nth-child(2)',
        ];
    }
}
