<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\documents;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;

class collectionsPage extends subscribersPage
{
  use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty, documents;

  public function remove_refund_collection(Browser $browser)
  {
    $browser->clicklink('#settlements-table > tbody > tr:nth-child(2) > td:nth-child(2) > a')
    ->click('Delete')
    ->assertsee('deleted');
  }

  public function assert_transactor_in_datewise_collection_report(Browser $browser, $subscriber_name, $group_name, $receipt_amount, $for_branch, $by_branch)
  {
    $browser->assertSee($subscriber_name, $group_name, $receipt_amount, $for_branch, $by_branch);
  }

  public function assert_cash_collection_in_report(Browser $browser, $receipt_date, $subscriber_name, $group_name, $ticket_number, $receipt_amount, $for_branch, $by_branch)
  {
    $browser->assertSee($receipt_date, $subscriber_name, $group_name, $ticket_number, $receipt_amount, $for_branch, $by_branch);
  }

  /*Edit Receipt */
  public function click_edit_details_in_receipt(Browser $browser)
  {
    $browser->clickLink('Edit Details');
  }

  public function modify_receipt_details_in_receipt_entry_form(Browser $browser, $details)
  {
    foreach ($details as $key => $value) {
      switch ($key) {
        case 'receipt date':
        $browser->type('#receipt_date', $value);
        break;
        case 'receipt time':
        $browser->type('#receipt_time', $value);
        break;
        case 'mode of payment':
        $browser->select('#type', $value);
        break;
        case 'reference no':
        $browser->type('#reference_no', $value);
        break;
        case 'bank':
        $browser->type('#bank_name', $value);
        break;
        case 'branch':
        $browser->type('#branch_name', $value);
        break;
        case 'processed date':
        $browser->type('#processed_date', $value);
        break;
        case 'processed time':
        $browser->type('#processed_time', $value);
        break;
        case 'realized date':
        $browser->type('#realized_date', $value);
        break;
        case 'realized time':
        $browser->type('#realized_time', $value);
        break;
        case 'notes':
        $browser->type('#notes', $value);
        break;
      }
    }
  }

  /**select change log on receipt to know revision history */

  public function add_other_charges_receipt_entry_in_enrollment_ledger(Browser $browser, $amount)
  {
    $browser
    ->clickLink('Other Charges')
    ->clickLink('Add Charge')
    ->type('other_charges_input', $amount)
    ->press('Add Charges')
    ->assertSee('Charges added successfully.');
  }

  public function add_registration_charge_entry_in_enrollment_ledger(Browser $browser)
  {
    $browser
    ->clickLink('Add Charge')
    ->check('select_registration_charge_check_box')
    ->press('Add Charges')
    ->assertSee('Charges added successfully.');
  }

  public function remove_other_charges_collection_receipt_in_other_charges_tab_in_enrollment_ledger(Browser $browser)
  {
    $browser->clickLink('CHG-0000');
    $browser->clickLink('Delete');
  }

  public function remove_other_charges_second_row(Browser $browser)
  {
    $browser->press('other_charges_remove_row_two')
    ->assertSee('Charge removed successfully.');
  }

  public function remove_other_charges_row_one(Browser $browser)
  {
    $browser
    ->click('other_charges_remove_row_one')
    ->assertSee('Charge removed successfully.');
  }

  public function remove_not_paid_other_charges_in_enrollment_ledger(Browser $browser)
  {
    $browser
    ->click('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(3) > div:nth-of-type(2) > div > div > table > tbody > tr > td:nth-of-type(9) > form > button > i')
    ->assertSee('Charge removed successfully.');
  }

  public function add_cancellation_charge_entry_in_enrollment_ledger(Browser $browser)
  {
    $browser
    ->clickLink('Add Charge')
    ->check('select_cancellation_charge_check_box')
    ->press('Add Charges')
    ->assertSee('Charges added successfully.');
  }

  public function remove_cancellation_charge_row_one(Browser $browser)
  {
    $browser->press('remove_cancellation_charge_row_one')
    ->pause('2000');
  }


  /******************************** Elements ********************************/        
  public function elements()
  {
    return [
      'Transactor' => 'main#main-container > div:nth-of-type(2) > div > div > div > div:nth-of-type(2) > div > div > form > div:nth-of-type(5) > div > input',
      
      'enter_amount' => 'input[name="amount"]',
      
      'Receipt_Number' => '#main-container > div:nth-child(1) > div > div > div.col-8 > h1',
      
      'delete_button' => '#main-container > div:nth-child(2) > div:nth-child(1) > div > div > ul > li.nav-item.ml-auto > div > a.btn.btn-danger',
      
      'other_charges_input' => 'input[name="charges[Other Charges]"]',
      
      'collection_amount_field' => 'input[name="amount"]',
      
      'first_charge_collection_receipt' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(8) > small > a',
      
      'other_charges_remove_row_two' => 'tr:nth-child(2) > td:nth-child(9) > form > button > i',
      
      'Add Collection' => '#ledger_tab > div.text-right > div > a.btn.btn-primary',
      
      'receipt_amount' => 'input[name="amount"]',
      
      'Receipt_Number' => '#main-container > div:nth-child(1) > div > div > div.col-8 > h1',
      
      'remove_auction' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row:nth-child(2) div.col-lg-12 div.block.block-rounded.block-bordered div.block-content.tab-content div.tab-pane.active table.table.text-center.table-sm tbody:nth-child(2) tr:nth-child(3) td:nth-child(9)',
      
      'AddCollection1' => '##main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-header.border-bottom > a',
      
      'AddCollection2' => '#main-container > div.content > div > div > div:nth-child(2) > div.block-header.border-bottom > div > a',
      
      'Save Receipt' => '#button-strip > paper-button.action-button',
      
      'other_Charges_Receipt' => '#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(3) > div:nth-of-type(2) > div > div > table > tbody > tr > td:nth-of-type(8) > small > a',
      
      'other_charges' => '#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(3) > div:nth-of-type(2) > div > div > table > tbody > tr > td:nth-of-type(9) > form > button > i',
      
      'Notification Box' => '#main-container > div.content > div > div > div:nth-child(6)',
      
      'Bonus Assertion' => 'tr:nth-child(4) > td.text-right.table-success.ledger-settlement-info:nth-child(6)',
      
      'Registration Charge Type' => '#main-container > div.content > div > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(1)',
      
      'Other Charges Receipt' => '#main-container > div.content > div > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(8) > small > a',
      
      'Registration Charge Status' => '#main-container > div.content > div > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(3) > span',
      
      'Remove Registration Charge' => 'tr:nth-child(1) > td:nth-child(9) > form > button > i',

      'select_registration_charge_check_box' => 'form > div:nth-child(3) > div > div:nth-child(1) > div > div > input[type=checkbox]',
      
      'select_cancellation_charge_check_box' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(3) > div > div:nth-child(3) > div > div > input[type=checkbox]',
      
      'Cancellation Charge Receipt Number' => '#other_charges_tab > div > div > table > tbody > tr:nth-child(1) > td:nth-child(8) > small > a',
      
      'remove_cancellation_charge' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(3) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tr:nth-child(1) td:nth-child(9) form.d-inline button.btn.btn-sm.btn-light.js-tooltip-enabled.p-0.ml-2:nth-child(3) > i.fa.fa-fw.fa-times',
      
      'press add collection' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(2) div.block-header.border-bottom div.btn-group.btn-group-sm.d-print-none > a.btn.btn-primary',
      
      'remove documentation charge' => 'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row div.col-lg-12 div.block.block-rounded.block-bordered:nth-child(3) div.block-content div.row.mb-2 div.col-md-12 table.table.table-sm tr:nth-child(2) td:nth-child(9) form.d-inline button.btn.btn-sm.btn-light.js-tooltip-enabled.p-0.ml-2:nth-child(3) > i.fa.fa-fw.fa-times',
      
      'other charges table' => '#main-container > div.content > div > div > div:nth-child(2)',
      
      'Other Charge Field' => '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(3) > div > input',
      
      'manual other charges row' => '#main-container > div.content > div > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(2)',
      
      'remove manual other charges' => '#main-container > div.content > div > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(9) > form > button > i',
      
      'remove alerts' => '#main-container > div.content > div > div > div.row > div > div > button',
      
      'Alternate Receipt Number' => '#main-container > div:nth-child(1) > div > div > div.col-8 > small',
      
      'Amount Field In Receipt' => '#collection-details > div > div > div > div:nth-child(4) > div',
      
      'other_charges_remove_row_one' => 'tr:nth-child(1) > td:nth-child(9) > form > button > i',

      'Pending Days for NP 1C' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(8)',

      'Due Amount for NP 1C' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(3)',

      'New Penalty for NP 1C' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',

      'Pending Days for Prized - Policy 1C' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(8)',

      'Due Amount for Prized - Policy 1C' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(3)',

      'New Penalty for Prized - Policy 1C' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',

      'remove_cancellation_charge_row_one' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(9) > form > button',

      'Regenerate Settlements' => '#ledger_tab > div.text-right > div > div > a:nth-child(2)',

      'Receipt Amount' => '#collection-details > div > div > div > div:nth-child(4) > div > div > label',

      'Other_charges_receipt_location' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(3) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(8) > small > a',

      'Pending Days for NP 1A' => '#settlements-table > tbody > tr:nth-child(6) > td:nth-child(8)',

      'Due Amount for NP 1A' => '#settlements-table > tbody > tr:nth-child(6) > td:nth-child(3)',

      'New Penalty for NP 1A' => '#settlements-table > tbody > tr.table-info > td:nth-child(7)',

      'Pending Days for Prized - Policy 1A' => '#settlements-table > tbody > tr:nth-child(14) > td:nth-child(5)',

      'Due Amount for Prized - Policy 1A' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(3)',

      'New Penalty for Prized - Policy 1A' => '#settlements-table > tbody > tr:nth-child(13) > td:nth-child(7)',
      'Current TBC Penalty' => '#settlements-table > tbody > tr:nth-child(21) > td:nth-child(8)',

      'Number of Other Charges Removal Button in Enrollment Ledger' => '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr > td:nth-child(9) > form > button',

      'Policy Details'  =>  'div.sidebar-o.sidebar-dark.enable-page-overlay.side-scroll.side-trans-enabled.main-content-boxed:nth-child(1) div.content div.row:nth-child(2) div.col-lg-12 div.block.block-rounded.block-bordered div.block-content.tab-content div.tab-pane.active div.block-content div.row div.col-lg-12 div.form-group.row.mb-0:nth-child(1) div.col-4:nth-child(2) > input.form-control-plaintext',

      'Pending Days for NP 1B'  =>  '#settlements-table > tbody > tr:nth-child(8) > td:nth-child(8)',
      
      'Due Amount for NP 1B'    =>  '#settlements-table > tbody > tr:nth-child(8) > td:nth-child(3)',
      
      'New Penalty for NP 1B'   =>  '#settlements-table > tbody > tr.table-info > td:nth-child(7)',
      
      'To Be Collected Total in Enrollment Ledger'    =>  '#settlements-table > tbody > tr.table-info > td.text-right.bg-warning',

      'Total Penalty in Enrollment Ledger'    =>  '#settlements-table > tbody > tr.table-info > td:nth-child(7)',
      'Agent Commission Policy 1A'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(1) > td:nth-child(10)',

      'Agent Commission Policy 1B'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(3) > td:nth-child(10)',

      'Agent Commission Policy 1C'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(5) > td:nth-child(10)',

      'Agent Commission Policy 2'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(7) > td:nth-child(10)',

      'Ticket Numbers in Enrollments Tab' =>  '#btabs-static-enrollments > table > tbody > tr > td:nth-child(1)',

      'Number Of Auctions'    =>  '#btabs-static-enrollments > table > tbody > tr > td:nth-child(9) > form > button',

      'Enrollments' =>  '#main-container > div.content > div:nth-child(2) > div > div > ul > li:nth-child(1) > a',

      'Enrollment Scores of active enrollments array' =>  '#btabs-static-enrollments > table > tbody > tr > td:nth-child(1) > small > span.text-success > span, #btabs-static-enrollments > table > tbody > tr > td:nth-child(2) > small > span.text-success > span,
#btabs-static-enrollments > table > tbody > tr > td:nth-child(1) > small > span.text-warning > span, #btabs-static-enrollments > table > tbody > tr > td:nth-child(2) > small > span.text-warning > span,
#btabs-static-enrollments > table > tbody > tr > td:nth-child(1) > small > span.text-danger > span, #btabs-static-enrollments > table > tbody > tr > td:nth-child(2) > small > span.text-danger > span',

      'Group Score in Enrollments Page' =>  '#main-container > div.content > div:nth-child(1) > div:nth-child(1) > div > div > div.font-size-h4.font-w400 > span > span',

      'Employee Commission Policy 1A'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(1) > td:nth-child(11)',

      'Employee Commission Policy 1B'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(3) > td:nth-child(11)',

      'Employee Commission Policy 1C'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(5) > td:nth-child(11)',

      'Employee Commission Policy 2'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(7) > td:nth-child(11)',

      'Subscriber Commission Policy 1A'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(1) > td:nth-child(12)',

      'Subscriber Commission Policy 1B'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(3) > td:nth-child(12)',

      'Subscriber Commission Policy 1C'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(5) > td:nth-child(12)',

      'Subscriber Commission Policy 2'    =>  '#main-container > div.content > div > div > div > div.block-content > table > tbody > tr:nth-child(7) > td:nth-child(12)',


      'Available Balance In Drop Chit Enrollment'=>'#settlements-table > tbody > tr.table-info > td.text-right.bg-success',

      'Add Cancellation Charge'=>'#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(3) > div > div:nth-child(3) > div > div > input[type=checkbox]',

      'Other Charges Popup In receipt Entry'=> '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(4) > div > div > div.flex-fill.ml-3 > p',

      'Status of an Other Charge Row One'=>'#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(1) > td:nth-child(3) > span',

      'PAN Number in Subscriber Ledger' =>  'form #pan_no',

      'Other_Charges_Row_two' =>  '#main-container > div.content > div:nth-child(3) > div > div:nth-child(2) > div.block-content > div > div > table > tbody > tr:nth-child(2) > td:nth-child(8) > small > a',

      'Choose File' =>  '#main-container > div.content > div > div > div > div.block-content > div > div > form > div:nth-child(5) > div > input',

      'Other charges tab in enrollment ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(3) > a',

      'Toggle Settlement Information'  =>  '#toggle-settlement-information',

      'Drop down in Collection' => '#dropdown-default-outline-primary',

      'Ledgers tab in enrollment ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(1) > a',

      'Prize Money tab in enrollment ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(2) > a',

      'Details tab in employee ledger'    =>    '#employee_tabs > li:nth-child(4) > a',

      'Details tab in subscriber ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(7) > a',

      'Notifications tab in enrollment ledger'  =>  '#enrollment_ledger_tabs > li:nth-child(9) > a',

      'Document tab in subscriber ledger' =>  '#enrollment_ledger_tabs > li:nth-child(2) > a',

      'Todays Collections'   => '#dashboard_tabs > li.nav-item.ml-auto > div > a:nth-child(2)',

      'First name in agents list'  =>  '#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child(1) > td.font-w600 > a',

      'First name in subscribers list'  =>  '#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child(1) > td.font-w600 > a',

      'First name in employees list'  =>  '#main-container > div.content > div > div > div > div.block-content > div > div > table > tbody > tr:nth-child(1) > td.font-w600 > a',

      'First name in enrollment list'  =>  '#DataTables_Table_0 > tbody > tr:nth-child(1) > td:nth-child(2) > a',

      'Enrollment Name in Receipts' =>  '#collection-details > div > div:nth-child(2) > div > div:nth-child(1) > label:nth-child(2) > a',


    ];
  }

}
/******************************** Elements ********************************/  































































