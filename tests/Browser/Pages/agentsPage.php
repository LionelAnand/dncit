<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

use Tests\Browser\Pages\Traits\common;
use Tests\Browser\Pages\Traits\collection;
use Tests\Browser\Pages\Traits\commission;
use Tests\Browser\Pages\Traits\subscriber;
use Tests\Browser\Pages\Traits\agent;
use Tests\Browser\Pages\Traits\prizemoney;
use Tests\Browser\Pages\Traits\employee;
use Tests\Browser\Pages\Traits\report;
use Tests\Browser\Pages\Traits\auction;
use Tests\Browser\Pages\Traits\guarantor;
use Tests\Browser\Pages\Traits\group;
use Tests\Browser\Pages\Traits\penalty;
use Tests\Browser\Pages\Traits\othercharges;

class agentsPage extends branchesPage
{

    use common, group, collection, othercharges, commission, subscriber, agent, prizemoney, employee, report, auction, guarantor, penalty;

    public function elements()
    {
        return [

        ];
    }
}