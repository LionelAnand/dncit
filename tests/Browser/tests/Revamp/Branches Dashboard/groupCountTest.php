<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class groupCountTest extends DuskTestCase
{
    /**
     * This test will compare with No of group on Dashboard for particular branch  
     * and No of group on it's group wise business report.
     *
     * @return void
     */
    public function testgroupCount()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Porur')
                ->pause(1000);
            $no_of_group_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div > div:nth-of-type(2) > div > div:nth-of-type(3) > a > div > div:nth-of-type(2)');
            $browser->visit(new reportsPage)
                ->go_to_group_wise_ATO_report()
                ->select_the_branch_by_providing_branch_id(13)
                ->select_status('Running')
                ->click_generate_in_reports()
                ->pause(3000);
            $no_of_group_on_groupwise_business_report = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > h3');
            $no_of_group_on_groupwise_business_report = str_replace(' GROUPS', '', $no_of_group_on_groupwise_business_report) * 1;
            $this->assertEquals($no_of_group_on_dashboard, $no_of_group_on_groupwise_business_report);
        });
    }
}
