<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class verifyVacancyOnDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testverifyVacancyOnDashboard()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->pause(1000);
            $no_of_vacancy_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(7) > a > div > div:nth-of-type(2)');
            $browser->visit(new reportsPage)
                ->select_vacancy_groupwise_report()
                ->assertsee('Vacancy')
                ->choose_branch('14')
                ->select_vacancy('Vacant only')
                ->click_generate_in_reports()
                ->pause(2000);
            $no_of_vacancy_on_report = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(2) > table > tbody > tr:nth-of-type(4) > td:nth-of-type(9)');
            $this->assertEquals($no_of_vacancy_on_dashboard, $no_of_vacancy_on_report);
        });
    }
}
