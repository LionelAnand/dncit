<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class verifyUPMonDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testverifyUPMonDashboard()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->pause(1000);
            $UPM_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(5) > a > div > div:nth-of-type(2)');
            $browser->visit(new reportsPage)
                ->goto_undisbursed_prize_money_report()
                ->choose_branch('14')
                ->click_generate_in_reports()
                ->pause(2000);
            $UPM_group_wise_report = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(2) > table > tbody > tr:nth-of-type(11) > td:nth-of-type(3)');
            $this->assertEquals($UPM_on_dashboard, $UPM_group_wise_report);
        });
    }
}
