<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class verifyCollectionOnDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testVerifyCollectionOnDashboardTest()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->assertsee('Coimbatore Branch')
                ->pause(1000);
            $collection_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(4) > a > div > div:nth-of-type(2)');
            $collection_on_dashboard = str_replace(',', '', $collection_on_dashboard) * 1;
            $browser->visit(new reportsPage)
                ->select_collection_summary()
                ->assertsee('Collections Summary')
                ->click_branch_name_in_dashboard('14')
                ->enter_from_date('01112019')
                //->select_status('Running')
                ->click_generate_in_reports()
                ->pause(1000);
            $collection_on_collection_report = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(2) > table > tbody > tr:nth-of-type(6) > td:nth-of-type(6)');
            $collection_on_collection_report = str_replace(',', '', $collection_on_collection_report) * 1;
            $Other_charges_debited = $browser->text('#main-container > div.content > div > div > div:nth-child(2) > div.block-content > table > tbody > tr.table-info > td.bg-warning');
            $collection_on_collection_report = ($collection_on_collection_report - $Other_charges_debited);
            $this->assertEquals($collection_on_dashboard, $collection_on_collection_report);
        });
    }
}
