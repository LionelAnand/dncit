<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class verifyNewBusinessOnDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testverifyNewBusinessOnDashboard()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->pause(1000);
            $new_business_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > a > div > div:nth-of-type(2)');
            $browser->visit(new reportsPage)
                ->go_to_group_wise_ATO_report()
                ->choose_branch('14')
                ->select_status('New')
                ->click_generate_in_reports()
                ->pause(2000);
            $new_business_on_report = $browser->text('main#main-container > div:nth-of-type(2) > div > div > div:nth-of-type(2) > div:nth-of-type(2) > table > tbody > tr > td:nth-of-type(2)');
            $this->assertEquals($new_business_on_dashboard, $new_business_on_report);;
        });
    }
}
