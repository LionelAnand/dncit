<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class verifyATOInDashboardTest extends DuskTestCase
{
    /**
     * Verify ATO on Dashboard with ATO on Business group wise Report 
     *
     * @return void
     */
    public function testverifyATOInDashboard()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->assertsee('Coimbatore Branch')
                ->pause(1000);
            $ato_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div > a > div > div:nth-of-type(2)');
            $browser->visit(new reportsPage)
                ->select_business_groupwise()
                ->assertsee('ATO Group Wise')
                ->choose_branch('14')
                ->select_status('Running')
                ->click_generate_in_reports()
                ->pause(1000);
            $ato_on_business_report = $browser->text('#main-container > div.content > div > div > div:nth-child(2) > div.block-content > table > tbody > tr.text-center.font-weight-bold > td:nth-child(2)');
            $this->assertEquals($ato_on_dashboard, $ato_on_business_report);
        });
    }
}
