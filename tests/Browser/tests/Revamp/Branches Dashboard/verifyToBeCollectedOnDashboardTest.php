<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\groupsPage;

class verifyToBeCollectedOnDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */

    public function testverifyToBeCollectedOnDashboard()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new groupsPage)
                ->click_branch_name_in_dashboard('Coimbatore')
                ->pause(1000);
            $to_be_collected_on_dashboard = $browser->text('main#main-container > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(2) > div > div:nth-of-type(3) > a > div > div:nth-of-type(2)');
            //         dump($to_be_collected_on_dashboard);
            $browser->visit(new reportsPage)
                ->goto_tobe_collected_groupwise_report()
                ->choose_branch('14')
                ->click_generate_in_reports()
                ->pause(2000);
            $to_be_collected_on_groupwise_report = $browser->text('#main-container > div.content > div > div > div:nth-child(2) > div.block-content > table > tbody > tr.table-info.font-weight-bold > td.bg-warning');
            //       dump($to_be_collected_on_groupwise_report);
            $this->assertEquals($to_be_collected_on_dashboard, $to_be_collected_on_groupwise_report);;
        });
    }
}
