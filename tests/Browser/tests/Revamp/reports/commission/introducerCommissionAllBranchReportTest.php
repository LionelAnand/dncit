<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\comissionsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\loginPage;

class introducerCommissionAllBranchReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testintroducerCommissionAllBranchReport()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");

            $browser->visit(new loginPage)
                ->sign_in()
                ->visit(new comissionsPage)
                ->click_introducer_commission_in_menu()
                ->click_group_wise_from_introducer_commission_in_menu()
                ->click_branch_name_in_dashboard('All Branches')
                ->click_generate_in_reports()
                ->assertsee('Export')
                ->export_the_report()
                ->pause(1000);
        });
    }
}
