<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;

class SubscriberCommissionDisbursementPolicy2Test extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSubscriberCommissionDisbursementPolicy2Test()
    {
        /**************************** Test Name ****************************/
        dump('Subscriber Commission Disbursement Policy 2 Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
        $employee = (object)[]; $introducer = (object)[]; $commission = (object)[]; $enrollment = (object)[];

            $date = $browser->get_todays_date()->date;

            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch);

            /***** Get Commission Percentage for policy 2 *****/
            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('subscriber', '2','D1', '7')->commission_percentage;

           /**** Pick random subscriber, employee and get their details *****/
           $browser->pick_subscriber($subscriber)->pick_subscriber($introducer)->pick_employee($employee);

            $group->scheme = 3; $group->policy = 2;

            /******************** Create a new group in Policy 1B ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $subscriber, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            $browser->visit(new comissionsPage)->visit($group->url);
   
            /***** Assert Commission Details before disbursement *****/
            $browser->go_to_enrollment_with_id($enrollment)->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($subscriber->name, $group->chit_value);
   
            /***** Get Commission Disbursement details before disbursement *****/
            $browser->get_details_from_introducer_commission_tab_in_enrollment_ledger($commission);

            $commission->chit_value = $browser->format_amount($commission->chit_value)->format_amount;

            $commission->commission = $browser->format_amount($commission->commission)->format_amount;

            $commission->tds = $browser->format_amount($commission->tds)->format_amount;

            $commission->disbursable = $browser->format_amount($commission->disbursable)->format_amount;

            $commission->undisbursed = $browser->format_amount($commission->undisbursed)->format_amount;

            /***** Check Chit value matches with Group and Enrollment Ledger *****/
            $this->assertEquals($group->chit_value, $commission->chit_value);

            /***** Subscriber Commission Calculation *****/
            $subscriber_commission = $commission->chit_value * $commission->percentage / 100;
            $this->assertEquals($subscriber_commission, '100');

            /***** Assert Commission details for subscriber with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '20', $commission->disbursable_commission = '80', $commission->undisbursed_commission = '80');
                $amount = '80';
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '5', $commission->disbursable_commission = '95', $commission->undisbursed_commission = '95');
                $amount = '95';
            }

            /***** Disburse Introducer Commission *****/
            $browser->click_disburse_introducer_commission_in_introducer_commission_tab()
            ->enter_introducer_commission_disbursement_details($amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Subscriber Commission Disbursement Policy 2 Test')->press('Save')->assert_that_introducer_commission_has_been_disbursed()->go_to_introducer_commission_tab_in_enrollment_ledger();
            
            /***** Assert Commission details for subscriber after disbursement with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_after_disbursement_with_pan_card($commission->disbursable_commission = '80', $commission->undisbursed_commission = '0');
            }

            else 
            {
                $browser->assert_commission_details_after_disbursement_without_pan_card($commission->disbursable_commission = '95', $commission->undisbursed_commission = '0');
            }
            
            /***** Delete the commission disbursed receipt *****/
            $browser->remove_introducer_commission_disbursements()->assert_commission_deleted();

        });
    }
}
