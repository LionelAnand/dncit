<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;

class IntroducerNameSwapOnComissionDisbursementTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testIntroducerNameSwapOnComissionDisbursement()
    {
        /**************************** Test Name ****************************/
        dump('Introducer Name Swap On Comission Disbursement Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; $enrollment = (object)[];
            $employee = (object)[]; $introducer = (object)[]; $agent = (object)[]; $commission = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch);

            /***** Get Agent Commission Percentage for policy 1A *****/
            $commission->agent = $browser->go_to_policy_page()->commission_percentage('agent', '1A','D1', '1')->commission_percentage;

            /***** Get Employee Commission Percentage for policy 1A *****/
            $commission->employee = $browser->go_to_policy_page()->commission_percentage('employee', '1A','D1', '1')->commission_percentage;

           /**** Pick random subscriber, Employee and Agent and get their details *****/
           $browser->pick_subscriber($subscriber)->pick_employee($employee)->pick_employee($introducer)->pick_agent($agent);
    
           $group->scheme = 20; $group->policy = 1;

            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            $browser->visit(new comissionsPage)->visit($group->url)->go_to_enrollments_tab_in_enrollment_ledger();

            /***** Assert Commission Details before disbursement *****/
            $browser->click_subscriber_to_go_to_enrollment_ledger($subscriber->name)->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($employee->name, $group->chit_value);
            $enrollment_url = $browser->get_url()->url;

            /***** Get Commission Disbursement details before disbursement *****/
            $browser->get_details_from_introducer_commission_tab_in_enrollment_ledger($commission);

            $commission->chit_value = $browser->format_amount($commission->chit_value)->format_amount;

            $commission->commission = $browser->format_amount($commission->commission)->format_amount;

            $commission->tds = $browser->format_amount($commission->tds)->format_amount;

            $commission->disbursable = $browser->format_amount($commission->disbursable)->format_amount;

            $commission->undisbursed = $browser->format_amount($commission->undisbursed)->format_amount;

            /***** Check Chit value matches with Group and Enrollment Ledger *****/
            $this->assertEquals($group->chit_value, $commission->chit_value);

            /***** Agent Commission Calculation *****/
            $agent_commission = $commission->chit_value * $commission->agent / 100;
            $this->assertEquals($agent_commission, '10000');

            /***** Assert Commission details for agents with and without pan card *****/
            if ($agent->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '2,000', $commission->disbursable_commission = '8,000', $commission->undisbursed_commission = '8,000');
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '500', $commission->disbursable_commission = '9,500', $commission->undisbursed_commission = '9,500');
            }
            /***** Swap Introducer and assert commission *****/
            $browser->visit(new groupsPage)->edit_enrollment($group, $subscriber, $employee, $employee, $enrollment);

            /***** Assert Commission Details after swapping introducer *****/
            $browser->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($employee->name, $group->chit_value);

            /***** Employee Commission Calculation *****/
            $employee_commission = $commission->chit_value * $commission->employee / 100;
            $this->assertEquals($employee_commission, '7500');
            $browser->visit(new comissionsPage)->visit($enrollment_url)->go_to_introducer_commission_tab_in_enrollment_ledger();

            /***** Assert Commission details for employee with and without pan card *****/
            if ($employee->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '1,500', $commission->disbursable_commission = '6,000', $commission->undisbursed_commission = '6,000');

                $amount = '6000';
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '375', $commission->disbursable_commission = '7,125', $commission->undisbursed_commission = '7,125');

                $amount = '7125';
            }
            /***** Assert Commission details for employee with and without pan card *****/

            /***** Disburse Introducer Commission *****/
            $browser->click_disburse_introducer_commission_in_introducer_commission_tab()
            ->enter_introducer_commission_disbursement_details($amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Introducer Name Swap On Comission Disbursement Test')->press('Save')->assert_that_introducer_commission_has_been_disbursed()->go_to_introducer_commission_tab_in_enrollment_ledger();
            
            /***** Assert Commission details for employee after disbursement with and without pan card *****/
            if ($employee->pan == '')
            {
                $browser->assert_commission_details_after_disbursement_with_pan_card($commission->disbursable_commission = '6,000', $commission->undisbursed_commission = '0');
            }

            else 
            {
                $browser->assert_commission_details_after_disbursement_without_pan_card($commission->disbursable_commission = '7,125', $commission->undisbursed_commission = '0');
            }
            /***** Assert Commission details for employee after disbursement with and without pan card *****/
            
            /***** Delete the commission disbursed receipt *****/
            $browser->remove_introducer_commission_disbursements()->assert_commission_deleted();

        });
}
}
