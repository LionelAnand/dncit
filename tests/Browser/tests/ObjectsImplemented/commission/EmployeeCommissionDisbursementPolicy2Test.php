<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;

class EmployeeCommissionDisbursementPolicy2Test extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEmployeeCommissionDisbursementPolicy2()
    {

        /**************************** Test Name ****************************/
        dump('Employee Commission Disbursement Policy 2 Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            $employee = (object)[]; $introducer = (object)[]; $commission = (object)[]; $enrollment = (object)[];

            $date = $browser->get_todays_date()->date;

            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch);

            /***** Get Commission Percentage for policy 2 *****/
            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('employee', '2','D1', '7')->commission_percentage;

            /**** Pick random subscriber, Employee and Agent and get their details *****/
            $browser->pick_subscriber($subscriber)->pick_employee($employee)->pick_employee($introducer);

            $group->scheme = 19; $group->policy = 2;

            /******************** Create a new group in Policy 2 ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            $browser->visit(new comissionsPage)->visit($group->url);

            /***** Assert Commission Details before disbursement *****/
            $browser->go_to_enrollment_with_id($enrollment)->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($employee->name, $group->chit_value);

            /***** Get Commission Disbursement details before disbursement *****/
            $browser->get_details_from_introducer_commission_tab_in_enrollment_ledger($commission);

            $commission->chit_value = $browser->format_amount($commission->chit_value)->format_amount;

            $commission->commission = $browser->format_amount($commission->commission)->format_amount;

            $commission->tds = $browser->format_amount($commission->tds)->format_amount;

            $commission->disbursable = $browser->format_amount($commission->disbursable)->format_amount;

            $commission->undisbursed = $browser->format_amount($commission->undisbursed)->format_amount;

            /***** Check Chit value matches with Group and Enrollment Ledger *****/
            $this->assertEquals($group->chit_value, $commission->chit_value);

            /***** Employee Commission Calculation *****/
            $employee_commission = $commission->chit_value * $commission->percentage / 100;
            $this->assertEquals($employee_commission, '750');

            /***** Assert Commission details for employee with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '150', $commission->disbursable_commission = '600', $commission->undisbursed_commission = '600');
                $amount = '600';
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '37.5', $commission->disbursable_commission = '712.5', $commission->undisbursed_commission = '712.5');
                $amount = '712';
            }

            /***** Disburse Introducer Commission *****/
            $browser->click_disburse_introducer_commission_in_introducer_commission_tab()
            ->enter_introducer_commission_disbursement_details($amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Employee Commission Disbursement Policy 2 Test')->press('Save')->assert_that_introducer_commission_has_been_disbursed()->go_to_introducer_commission_tab_in_enrollment_ledger();
            
            /***** Assert Commission details for employee after disbursement with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_after_disbursement_with_pan_card($commission->disbursable_commission = '600', $commission->undisbursed_commission = '0');
            }

            else 
            {
                $browser->assert_commission_details_after_disbursement_without_pan_card($commission->disbursable_commission = '712.5', $commission->undisbursed_commission = '0.5');
            }
            
            /***** Delete the commission disbursed receipt *****/
            $browser->remove_introducer_commission_disbursements()->assert_commission_deleted();

        });
}
}
