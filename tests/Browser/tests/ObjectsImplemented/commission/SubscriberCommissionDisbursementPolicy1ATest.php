<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;

class SubscriberCommissionDisbursementPolicy1ATest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSubscriberCommissionDisbursementPolicy1ATest()
    {
        /**************************** Test Name ****************************/
        dump('Subscriber Commission Disbursement Policy 1A Test');
        /**************************** Test Name ****************************/
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; $enrollment = (object)[];
        $employee = (object)[]; $introducer = (object)[]; $commission = (object)[];
        $enrollment = (object)[];
        /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch);

            /***** Get Commission Percentage for policy 1A *****/
            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('subscriber', '1A','D1', '1')->commission_percentage;
            /***** Get Commission Percentage for policy 1A *****/

            /**** Pick random subscriber, employee and get their details *****/
            $browser->pick_subscriber($subscriber)->pick_subscriber($introducer)->pick_employee($employee);

            $group->scheme = 20; $group->policy = 1;

            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            $browser->visit(new comissionsPage)->visit($group->url);

            /***** Assert Commission Details before disbursement *****/
            $browser->go_to_enrollment_with_id($enrollment)->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($subscriber->name, $group->chit_value);

            /***** Get Commission Disbursement details before disbursement *****/
            $browser->get_details_from_introducer_commission_tab_in_enrollment_ledger($commission);

            $commission->chit_value = $browser->format_amount($commission->chit_value)->format_amount;

            $commission->commission = $browser->format_amount($commission->commission)->format_amount;

            $commission->tds = $browser->format_amount($commission->tds)->format_amount;

            $commission->disbursable = $browser->format_amount($commission->disbursable)->format_amount;

            $commission->undisbursed = $browser->format_amount($commission->undisbursed)->format_amount;

            /***** Check Chit value matches with Group and Enrollment Ledger *****/
            $this->assertEquals($group->chit_value, $commission->chit_value);

            /***** Subscriber Commission Calculation *****/
            $subscriber_commission = $commission->chit_value * $commission->percentage / 100;
            $this->assertEquals($subscriber_commission, '1000');

            /***** Assert Commission details for subscriber with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '200', $commission->disbursable_commission = '800', $commission->undisbursed_commission = '800');
                $amount = '800';
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '50', $commission->disbursable_commission = '950', $commission->undisbursed_commission = '950');
                $amount = '950';
            }

            /***** Disburse Introducer Commission *****/
            $browser->click_disburse_introducer_commission_in_introducer_commission_tab()
            ->enter_introducer_commission_disbursement_details($amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Subscriber Commission Disbursement Policy 1A Test')->press('Save')->assert_that_introducer_commission_has_been_disbursed()->go_to_introducer_commission_tab_in_enrollment_ledger();
            
            /***** Assert Commission details for subscriber after disbursement with and without pan card *****/
            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_after_disbursement_with_pan_card($commission->disbursable_commission = '800', $commission->undisbursed_commission = '0');
            }

            else 
            {
                $browser->assert_commission_details_after_disbursement_without_pan_card($commission->disbursable_commission = '950', $commission->undisbursed_commission = '0');
            }
            
            /***** Delete the commission disbursed receipt *****/
            $browser->remove_introducer_commission_disbursements()->assert_commission_deleted();

        });
    }
}
