<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\agentsPage;

class editAgentsTest extends DuskTestCase
{

    public function testAddAndEditAgent()
    {

        /**************************** Test Name ****************************/
        dump('Add And Edit Agent Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[];  
            /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            $branch = $browser->get_branch_name()->name;

            $browser->click_branch_name_in_branches_page($branch)->click_agents_in_branch_ledger();

            $browser->create_a_new_agent('Goldberg', '3184679255', 'JDOBM65656', '654832196548')->assert_agent_created_details('Goldberg', '-AGT-', 'AGENT. SCORE', 'N/A');

            $browser->edit_agent('Steve Austin', '3214141412', 'HFRWJ04618', '3215487546')->assert_agent_created_details('Steve Austin', '-AGT-', 'AGENT. SCORE', 'N/A');              
        });
    }
}
