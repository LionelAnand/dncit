<?php

namespace Tests\Browser;
namespace Revolution\Google\Sheets\Tests;


use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\agentsPage;
use Revolution\Google\Sheets\Facades\Sheets;


class AgentIDGenerationTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testWhetherAgentIDIsGeneratedInProperSeries()
    {

        /**************************** Test Name ****************************/
        dump('Agent ID Generation Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[];

            $browser->downloadsheets();

            /***** Create Objects for modules to store multiple values *****/
        
         
        


        $fileHandler = fopen("tests/Browser/Pages/Traits/Mock/DataBranches.csv", "r");

        while (  ! feof( $fileHandler )  )
        {

               //Read the file line by line for CSV data.
        $csvFileArray = fgetcsv( $fileHandler, 1024 );

                //Display data
        echo 'Branch-ID:'. $csvFileArray[0]; echo 'Name:'. $csvFileArray[1];
        echo 'Code:'.$csvFileArray[2]; echo 'Date:'.$csvFileArray[3];
        echo 'Address:'. $csvFileArray[4]; echo 'City:'. $csvFileArray[5];
        echo 'State:'.$csvFileArray[6]; echo 'District:'.$csvFileArray[7];
        echo 'Pincode:'. $csvFileArray[8]; echo 'Phone:'. $csvFileArray[9];
        echo 'Email:'.$csvFileArray[10]; echo 'Tier:'.$csvFileArray[11];
        echo 'Score:'. $csvFileArray[12]; echo 'ETO:'. $csvFileArray[13];
        echo 'ATO:'.$csvFileArray[14]; echo 'Subscribers-Count:'.$csvFileArray[15];
        echo 'RunningGroups:'.$csvFileArray[16]; 
        echo 'Closed Groups:'.$csvFileArray[17];
        echo 'New Groups:'.$csvFileArray[18]; 
        echo 'Enrollments-Count:'.$csvFileArray[19];
            }
dump($csvFileArray);


            $date = $browser->get_todays_date()->date;

            $branch = $browser->get_branch_name()->name;

            $browser->visit(new agentsPage)->click_branch_name_in_branches_page($branch)->click_agents_in_branch_ledger();

            $browser->create_a_new_agent('Brock Lesnar', '9856231478', 'BCNCN85858', '125409345678')->assert_agent_created_details('Brock Lesnar', '-AGT-', 'AGENT. SCORE', 'N/A');
        });
    }
}
