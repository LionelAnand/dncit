<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class addFollowUpOnSameInteractionDateAndAssertThatNoInteractionSavedTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testaddFollowUpOnSameInteractionDateAndAssertThatNoInteractionSaved()
    {
        $this->browse(function (Browser $browser) {

            $subscriber = (object)[];
            $date = date("d-m-Y");      
            $browser->visit(new employeesPage)->sign_in()            
                    ->pick_subscriber($subscriber)           
                    ->go_to_subscriber_main_ledger($subscriber)
                    ->go_to_interactions_tab_in_subscriber_main_ledger()                  
                    ->add_interaction('In Person', 'Testing purpose', $date)
                    ->assertDontsee('Interaction saved.');
        });
    }
}
