<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class addSubscriberInteractionsAndAssertTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testaddSubscriberInteractionsAndAssert()
    {
        $this->browse(function (Browser $browser) {
            $subscriber = (object)[];
            $date = date('d-m-Y');
            $date_one = date('d-m-Y');
            $date_one = date_create($date_one);
            date_add($date_one, date_interval_create_from_date_string('1 days'));
            $date_one = date_format($date_one, 'd-m-Y');
            dump($date);
            if (strlen($date) == '7') {
                $date = str_pad($date, 8, '0', STR_PAD_LEFT);
                dump($date);
            }
            dump($date_one);// Adding Date + 1 //
            $browser->visit(new employeesPage)->sign_in()
                  ->pick_subscriber($subscriber)
                  ->go_to_subscriber_main_ledger($subscriber)
                  ->go_to_interactions_tab_in_subscriber_main_ledger()
                  ->add_interaction('In Person', 'Testing purpose', $date_one)
                  ->assertSee('Interaction saved.');
        });
    }
}
