<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class recoveryInteractionsSubscriberWiseEligibleForLegalActionTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testrecoveryInteractionsSubscriberWiseEligibleForLegalAction()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in()
            ->go_to_subscriber_wise_interaction_menu()->select_eligible_for_legal()->generate_report() ->assert_interaction();
        });
    }
}