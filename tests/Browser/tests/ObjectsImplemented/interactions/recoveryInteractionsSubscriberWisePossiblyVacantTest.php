<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class recoveryInteractionsSubscriberWisePossiblyVacantTest extends DuskTestCase
{
    /**
     * Recovery Interactions Subscriber Wise Possibly Vacant Test
     *
     * @return void
     */
    public function testRecoveryInteractionsSubscriberWisePossiblyVacant()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in()
            ->go_to_subscriber_wise_interaction_menu()->select_Possibly_Vacant()
            ->generate_report() ->assert_interaction();
        });
    }
}
