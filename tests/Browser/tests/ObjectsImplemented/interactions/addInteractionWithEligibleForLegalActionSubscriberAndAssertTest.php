<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\guarantorsPage;
use Tests\Browser\Pages\loginPage;
use Tests\DuskTestCase;

class addInteractionWithEligibleForLegalActionSubscriberAndAssertTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
             $branch = (object)[]; $enrollment = (object)[];

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch)
                    ->pick_one_subscriber_from_eligible_for_legal_action($branch,$enrollment);

            $date = date('d-m-Y');
            $date_one = date('d-m-Y');
            $date_one = date_create($date_one);
            date_add($date_one, date_interval_create_from_date_string('1 days'));
            $date_one = date_format($date_one, 'd-m-Y');
            dump($date);
            if (strlen($date) == '7') {
                $date = str_pad($date, 8, '0', STR_PAD_LEFT);
                dump($date);
            }
            dump($date_one); // Adding Date + 1 //

             $browser->go_to_interactions_tab_in_enrollment_ledger()
                     ->add_interaction('In Person', 'Testing purpose', $date_one)
                     ->assertSee('Interaction saved.')
                     ->assert_in_interaction_report_in_legal_action_filter($branch)
                     ->assertSee($enrollment->primary_name); //assert name in interaction report as same in enrollment primary name
        });
    }
}
