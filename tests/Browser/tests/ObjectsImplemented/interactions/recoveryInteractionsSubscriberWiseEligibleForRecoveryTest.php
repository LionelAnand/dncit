<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class recoveryInteractionsSubscriberWiseEligibleForRecoveryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testrecoveryInteractionsSubscriberWiseEligibleForRecovery()
    {
    $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in()
            ->go_to_subscriber_wise_interaction_menu()->select_Eligible_for_Recovery()->generate_report()->assert_interaction();
        });
    }
}
