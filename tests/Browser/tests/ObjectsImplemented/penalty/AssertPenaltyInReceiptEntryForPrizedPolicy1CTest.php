<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;
use Exception;

class AssertPenaltyInReceiptEntryForPrizedPolicy1CTest extends DuskTestCase
{
	/**
	 * A Dusk test example.
	 *
	 * @return void
	 */
	public function testAssertPenaltyInReceiptEntryForPrizedPolicy1C()
	{
		/**************************** Test Name ****************************/
        dump('Assert Penalty In Receipt Entry For Prized Policy 1C Test');
    
        $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment = (object)[]; 
        $dates = (object)[]; $group = (object)[]; $subscriber = (object)[]; $employee = (object)[]; 
        $receipt1 = (object)[]; $penalty = (object)[];

        $date = date('d-m-Y h:i:s');

        $dates->auction = date_create($date); date_sub($dates->auction, date_interval_create_from_date_string('16 days')); 
        
        $dates->auction = date_format($dates->auction, "d-m-Y");

        $dates->receipt = date_create($date); $dates->receipt = date_format($dates->receipt, "d-m-Y");

        /***** Get Penalty Percentage for Non Prized policy 1C *****/
        $browser->go_to_policy_page()->penalty_percentage($penalty, 'prized', '1C', '1');

        /***** Get Branch Details, Pick random subscriber, Employee and Agent and get their details *****/            
        $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee);            

        $group->scheme = 20; $group->policy = 1; $enrollment->bid_percentage = 30;
        /******************** Create a new group in Policy 1C ********************/
        $browser->create_new_group_in_branch($branch, $group, $date);
        
        $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment)
        ->visit(new auctionsPage)->add_first_auction_entry($group->name, $dates->auction)->get_bid_amount_with_percentage($group, $enrollment)
        ->add_auction_entry_for_particular_subscriber($group, $dates->auction, $enrollment->id, $enrollment->bid_amount)->pause(4000)->visit($enrollment->url);
        
       /*** Adding collection for Other Charges ***/
       $browser->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('4000', $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Assert Penalty In Receipt Entry For Prized Policy 1C Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->get_details_in_receipt($receipt)->clickLink($receipt->subscriber);

       /*** Adding two collection entries ***/
       $browser->visit($enrollment->url)->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('40000', $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Assert Penalty In Receipt Entry For Prized Policy 1C Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->visit($enrollment->url)->click_toggle_settlement_in_enrollment_ledger();

       $browser->get_details_for_penalty_calculations_in_enrollment_ledger($penalty)->penalty_calculations($penalty);

       $penalty->attribute_before_receipt = $browser->attribute('#page-container #settlements-table tbody tr:nth-child(2) td:nth-child(4)', 'class');

       $receipt->amount = 40000 + $penalty->calculation;
       
       $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form($receipt->amount, $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Assert Penalty In Receipt Entry For Prized Policy 1C Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->get_details_in_receipt($receipt1)->clickLink($receipt1->subscriber);

       $browser->visit($enrollment->url)->click_toggle_settlement_in_enrollment_ledger();

       $penalty->attribute_after_receipt = $browser->attribute('#page-container #settlements-table tbody tr:nth-child(2) td:nth-child(4)', 'class');

       /*** Assert attribute of the penalty ***/
       $this->assertEquals($penalty->attribute_after_receipt,'text-right table-success ledger-settlement-info');

       if ($penalty->attribute_before_receipt != $penalty->attribute_after_receipt)
       {
           $browser->delete_group($group);
       }

       else 
       {
           $error = 'Penalty amount is not updated for policy 1C group';
           throw new Exception($error);
       }

       });

}

}
