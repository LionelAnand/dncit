<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class PrizeMoneyAdjustmentOtherChargesDeductionTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testwhetherOtherChargesAreDeductedDuringPrizeMoneyAdjustment()
    {
        dump('Prize Money Adjustment Other Charges Deduction Test');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 2;
            $group->policy = 1;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                ->sign_in()
                ->pick_random_branch($branch)
                ->pick_subscriber($subscriber)
                ->pick_employee($employee)
                ->pick_employee($introducer)
                ->add_subscriber_kyc_documents($subscriber)
                ->create_new_group_in_branch($branch, $group, $date)
                ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                ->add_first_auction_entry($group->name, $date)
                ->get_bid_amount_with_percentage($group, $enrollment)
                ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)->pause(5000)
                ->get_prize_money_details($group, $enrollment)
                ->get_enrollment_details($enrollment)
                ->get_deductable_tbc_after_auction($enrollment);

            $enrollment->disbursement = $group->chit_value - $enrollment->bid_amount;
            $browser->go_to_enrollment_with_id($enrollment)
                    ->clickLink('Other Charges')
                    ->assertSee('Not Paid')
                    ->disburse_prize_money($enrollment)
                    ->go_to_enrollment_with_id($enrollment)
                    ->clickLink('Other Charges')
                    ->assertSee('Paid');
        });
    }
}
