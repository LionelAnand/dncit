<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\guarantorsPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class DocumentationChargePaidTest extends DuskTestCase
{
    public function testAddAssertAndPaidStatusDocumentationCharge()
    {
        /**************************** Test Name ****************************/
        dump('Documentation Charge Paid Test');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $guarantor = (object)[];
            $guarantor2 = (object)[];
            $guarantor3 = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $enrollment->bid_percentage = 30;
            $enrollment->deduction = 1;
            $group->policy = 3;
            $browser->visit(new prizedMoneyPage)
                ->sign_in()
                ->pick_random_branch($branch)
                ->pick_subscriber($subscriber)
                ->visit(new employeesPage)
                ->pick_employee($employee)
                ->pick_employee($introducer)
                ->pick_guarantor($guarantor)
                ->pick_guarantor($guarantor2)
                ->pick_guarantor($guarantor3)
                ->add_subscriber_kyc_documents($subscriber)
                ->create_new_group_in_branch($branch, $group, $date)
                ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                ->add_first_auction_entry($group->name, $date)
                ->get_bid_amount_with_percentage($group, $enrollment)
                ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                ->visit(new prizedMoneyPage)
                ->upload_documents_necessary_for_prized_money_disbursement($enrollment)
                ->visit(new guarantorsPage)
                ->add_guarantor_to_enrollment($enrollment, $guarantor)
                ->add_guarantor_to_enrollment($enrollment, $guarantor2)
                ->add_guarantor_to_enrollment($enrollment, $guarantor3)
                ->upload_guarantor_documents_for_prized_money_disbursement($guarantor, $enrollment)
                ->upload_guarantor_documents_for_prized_money_disbursement($guarantor2, $enrollment)
                ->upload_guarantor_documents_for_prized_money_disbursement($guarantor3, $enrollment)
                ->visit(new prizedMoneyPage)
                ->get_prize_money_details($group, $enrollment);
            $this->assertEquals($enrollment->upm, $enrollment->disbursable);
            $undisbursed_before = $enrollment->upm;
            $enrollment->disbursement = $enrollment->upm;
            $browser->disburse_prize_money($enrollment)
                    ->get_upm($enrollment);
            $undisbursed_after = $enrollment->upm;
            $final_upm = $undisbursed_before - $enrollment->disbursement;
            $this->assertEquals($final_upm, $undisbursed_after);
            $browser->get_calculated_documentation_charge($group)

            ->assert_document_charges_paid($enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}
