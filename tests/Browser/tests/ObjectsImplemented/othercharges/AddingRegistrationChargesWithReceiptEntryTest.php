<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Tests\Browser\Pages\groupsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AddingRegistrationChargesWithReceiptEntryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAddingRegistrationChargesWithReceiptEntryTest()
    {
        /**************************** Test Name ****************************/
        dump('Adding Registration Charges With Receipt Entry Test');
        /**************************** Test Name ****************************/
    
        $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment = (object)[]; 
        $group = (object)[]; $subscriber = (object)[]; $employee = (object)[]; $receipt = (object)[];

        $date = date('d-m-Y');

        /***** Get Branch Details, Pick random subscriber, Employee and Agent and get their details *****/            
        $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee);            

        $group->scheme = 20; $group->policy = 1;
        /******************** Create a new group in Policy 1A ********************/
        $browser->create_new_group_in_branch($branch, $group, $date);
        
        $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment)->pause(4000)->visit($enrollment->url);

        $browser->add_registration_charge_in_other_charges_tab()->go_to_other_charges_tab_in_enrollment_ledger();

        $browser->assert_total_in_other_charges_tab(2,000)->go_to_ledgers_tab_in_enrollment_ledger();

       /*** Add collection entry along with other charges ***/
       $browser->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('10000', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Adding Registration Charges With Receipt Entry Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->get_details_in_receipt($receipt)->clickLink($receipt->subscriber);

       /*** Add and delete other charges ***/
       $browser->visit($enrollment->url)->assertSee($receipt->number)->go_to_other_charges_tab_in_enrollment_ledger()
       ->assertSee($receipt->alternate)->delete_other_charges_receipts($receipt)->visit($enrollment->url)
       ->go_to_ledgers_tab_in_enrollment_ledger()->assertDontSeeLink($receipt->number)->delete_group($group);
       });
    }
}

