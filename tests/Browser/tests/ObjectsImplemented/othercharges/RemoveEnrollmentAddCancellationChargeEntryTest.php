<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;

class RemoveEnrollmentAddCancellationChargeEntryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRemoveEnrollmentAddCancellatioNChargeEntryTest()
    {
        $this->browse(function (Browser $browser) {

        /**************************** Test Name ****************************/
        dump('Remove Enrollment Add Cancellation Charge Entry Test');
        /**************************** Test Name ****************************/

            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; $charges = (object)[];
            $employee = (object)[]; $agent = (object)[]; $commission = (object)[]; $receipt = (object)[];$enrollment = (object)[]; $enrollment_ledger = (object)[]; $refund = (object)[];

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch); $date = date('d-m-Y'); $back_date = $browser->set_back_date('5')->back_date;

            /***** Get cancellation percentage for policy 1A *****/
            $browser->go_to_policy_page()->get_cancellation_charge($charges, '1');

            /**** Pick random subscriber, Employee and Agent and get their details *****/
            $browser->pick_subscriber($subscriber)->pick_employee($employee)->pick_agent($agent);

            $group->scheme = 20; $group->policy = 1;
            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            /*** Cancellation Charge format */
            $cancellation_charge = $group->chit_value * $charges->cancellation;

            $receipt->refund = $cancellation_charge + 1000;

            $browser->visit($enrollment->url)->clickLink('Add Collection')
            ->enter_receipt_details_in_receipt_entry_form('50000', $back_date, 'Cash', '123', 'KVB', 'Chennai', $back_date, '1200', $back_date, '1200' ,'Remove Enrollment Add Cancellation Charge Entry Test')
            ->save_the_collections()->assert_that_collection_receipt_is_saved();
            /******************** Add Collection Entry - Cash Mode  ********************/

            $browser->get_details_in_receipt($receipt)->clickLink($receipt->subscriber)->get_details_in_enrollment_ledger($enrollment_ledger);
            $browser->clickLink($enrollment->name)->remove_the_enrollment();
            $browser->go_to_removed_enrollments_report($branch,'1')->assert_removed_enrollment_in_removed_enrollment_report($subscriber, $enrollment, $date); 

            $browser->clickLink($enrollment->name)->go_to_other_charges_tab_in_enrollment_ledger()->assert_cancellation_charges_not_paid_in_enrollment_ledger()
            ->remove_other_charges_receipts($charges, $enrollment);

            $browser->add_cancellation_charge_in_other_charges_tab()->assert_total_in_other_charges_tab(31,000);

            $browser->refund_collection_for_removed_enrollment($receipt->refund, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1800', $date, '1800' ,'Remove Enrollment Add Cancellation Charge Entry Test')
            ->press('Save')->assert_that_refund_receipt_is_saved()->clickLink($receipt->subscriber)->go_to_other_charges_tab_in_enrollment_ledger();

            $browser->assert_cancellation_charges_paid_in_enrollment_ledger()->delete_group($group);

        });
    }
}
