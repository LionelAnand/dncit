<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Facebook\WebDriver\WebDriverBy;

class AssertRegistrationFeesOnGroupEnrollmentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAssertRegistrationFeesOnGroupEnrollment()
    {
        /**************************** Test Name ****************************/
        dump('Assert Registration Fees On Group Enrollment Test');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];$subscriber =(object)[]; $employee =(object)[]; $employee2 =(object)[]; 
            $introducer = (object)[];$dates = (object)[];$group = (object)[]; $enrollment = (object)[];
            $date = date('d-m-Y');
            $enrollment->bid_percentage = 30;
$group->policy = 1;
            $browser->visit(new groupsPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer,$enrollment)
                    ->get_calculated_registration_charge($group)
                    ->get_bid_amount_with_percentage($group, $enrollment);
            
            $browser->go_to_enrollment_with_id($enrollment)->clickLink('Other Charges')
                    ->assertSee('Registration Charge')
                    ->get_registration_charge_from_erp($enrollment);
                   
                    $this->assertEquals($group->calculated_registration_charge, $enrollment->registration_charge_in_erp);

                
            

        });
    }
}
