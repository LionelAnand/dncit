<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\DuskTestCase;

class AssertDocumentChargeWhenAuctionEnteredTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAssertDocumentChargeWhenAnAuctionIsEntered()
    {
        /**************************** Test Name ****************************/
        dump('Assert Document Charge When Auction Entered Test');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            $date = date('d-m-Y');
            $enrollment->bid_percentage = 30;
            $browser->visit(new auctionsPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->get_calculated_documentation_charge($group)
                    ->get_bid_amount_with_percentage($group, $enrollment);

            $browser->go_to_enrollment_with_id($enrollment)->clickLink('Other Charges')
                    ->assertDontSee('Documentation Charge')
                    ->add_first_auction_entry($group->name, $date)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->pause(2000)->visit($enrollment->url)->go_to_other_charges_tab_in_enrollment_ledger()
                    ->assertSee('Documentation Charge')
                    ->get_documentation_charge_from_erp($enrollment);
                    
            $this->assertEquals($group->calculated_documentation_charge, $enrollment->documentation_charge_in_erp);
        });
    }
}
