<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;


class refundReceiptEntryCreditOrDebitcardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testrefundReceiptEntryCreditOrDebitcard()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new groupsPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            $employee = (object)[]; $agent = (object)[]; $commission = (object)[]; $receipt = (object)[];$enrollment = (object)[]; $enrollment_ledger = (object)[];$refund = (object)[];

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);

            $back_date = $browser->set_back_date('5')->back_date;

            $date = $browser->get_todays_date()->date;

            /***** Get Commission Percentage for policy 1A *****/
            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('agent', '1A','D1', '1')->commission_percentage;

            /**** Pick random subscriber, Employee and Agent and get their details *****/
            $browser->pick_subscriber($subscriber);

            $browser->pick_employee($employee);

            $browser->pick_agent($agent);

            $group->scheme = 20; $group->policy = 1;

            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;
            
            $browser->visit($enrollment->url)->clickLink('Add Collection')->enter_receipt_details_in_receipt_entry_form('15000', $back_date, 'Credit / Debit Card', '123', 'KVB', 'Chennai', $back_date, '1200', $back_date, '1200' ,'Refund - Receipt Entry Cash Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
            /******************** Add Collection Entry - Cash Mode  ********************/

            $browser->get_details_in_receipt($receipt)->clickLink($receipt->subscriber);
            $browser->get_details_in_enrollment_ledger($enrollment_ledger);
            $browser->clickLink($enrollment->name)->remove_the_enrollment();
            $browser->go_to_removed_enrollments_report($branch,'1')->assert_removed_enrollment_in_removed_enrollment_report($subscriber, $enrollment, $date); 

            $browser->clickLink($enrollment->name)->go_to_other_charges_tab_in_enrollment_ledger()->assert_cancellation_charges_not_paid_in_enrollment_ledger();

            $cancellation_charge = $group->chit_value * 0.03;

            if ($enrollment_ledger->paid_installment_amount < $cancellation_charge)
            {
                $browser->remove_other_charges()->assert_other_charges_removed();
            }

            $browser->go_to_ledgers_tab_in_enrollment_ledger();

            $refund = $browser->text('#ledger_tab > div.text-right.d-print-none > div > a');

            $browser->refund_collection_for_removed_enrollment('14000', $date, 'Credit / Debit Card', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Refund - Receipt Entry Cash Test')->press('Save')->assert_that_refund_receipt_is_saved();

            $browser->get_details_in_receipt($receipt);
            $this->assertEquals($receipt->payment_mode, 'Credit / Debit Card');

            $browser->clickLink($receipt->subscriber)->assertDontSee($refund);
        });
    }
}
