<?php

namespace Tests\Browser;

use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;

class AddAndEditGuarantorTest extends DuskTestCase
{


    public function testAddAndEditGuarantor()
    {
         /**************************** Test Name ****************************/
         dump('Add and Edit Guarantor Test');
         /**************************** Test Name ****************************/
 
         $this->browse(function (Browser $browser) {
             $browser->visit(new loginPage)->sign_in();
 
         /***** Create Objects for modules to store multiple values *****/
         $branch = (object)[]; $guarantor = (object)[];
 
        /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);
 
             $guarantor   = Factory::create();
 
             $browser->format_mobile($guarantor)->click_guarantors_tab_in_branch_ledger();
 
           $browser->create_a_new_guarantor($branch, $guarantor)->assert_guarantor_created()->get_member_details($guarantor, 'guarantor');

           $browser->assert_guarantor_details($guarantor)->edit_guarantor_details($branch, $guarantor);

           $browser->get_member_details($guarantor, 'guarantor')->assert_edited_guarantor_details($guarantor);                              
        });
    }
}
