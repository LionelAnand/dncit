<?php

namespace Tests\Browser;

use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;


class addGuarantorDocumentsTest extends DuskTestCase
{
    public function testAddGuarantorDocuments()
    {
         /**************************** Test Name ****************************/
        dump('Add Guarantor Documents Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $guarantor = (object)[];
        /***** Create Objects for modules to store multiple values *****/

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);

            $guarantor = Factory::create();

            $browser->format_mobile($guarantor);

            $browser->click_guarantors_tab_in_branch_ledger();

          $browser->create_a_new_guarantor($branch, $guarantor)->assert_guarantor_created();

          $browser->get_member_details($guarantor, 'guarantor');
            
          $browser->click_add_document()->upload_document('2', $guarantor)->assert_document_added();

        });
    }
}
