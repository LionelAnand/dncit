<?php

namespace Tests\Browser;

use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;
use Tests\Browser\Pages\guarantorsPage;

class GuarantorToSubscriberConversionTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testGuarantorToSubscriberConversion()
    {
        /**************************** Test Name ****************************/
        dump('Guarantor To Subscriber Conversion Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $subscriber = (object)[]; $guarantor = (object)[]; $relationship = (object)[];

        /***** Get Branch Details *****/
            $browser->pick_subscriber($subscriber)->pick_random_branch($branch);

            $guarantor = Factory::create();

            $browser->format_mobile($guarantor);

            $browser->click_guarantors_tab_in_branch_ledger();

          $browser->create_a_new_guarantor($branch, $guarantor)->assert_guarantor_created();

          $browser->get_member_details($guarantor, 'guarantor');
            
          $browser->click_add_document()->upload_document('2', $guarantor)->assert_document_added();          

          $browser->adding_relationship_to_a_guarantor($subscriber);          

          $documents_before_conversion = $browser->elements('#btnGroupTabs1');

          $documents_before_conversion = count($documents_before_conversion);

          $browser->get_relationship_details_in_guarantor_ledger($relationship);          

          $browser->convert_a_guarantor_into_a_subscriber()->go_to_documents_tab();

          $documents_after_conversion = $browser->elements('#btnGroupTabs1');

          $documents_after_conversion = count($documents_after_conversion);

          $this->assertEquals($documents_after_conversion, $documents_before_conversion);

                  $browser->go_to_relationships_tab_in_subscriber_ledger()->assert_relationship_details_after_converting_to_subscriber($relationship);
        });
    }
}
