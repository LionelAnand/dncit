<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\DuskTestCase;

class firstAuctionRequiresPsoAndByLawDetailsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testFirstAuctionRequiresPsoAndByLawDetailsTest()
    {
        $this->browse(function (Browser $browser) {
            dump('First Auction Requires PSO And ByLaw Details Test');

            $branch = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->bylaw_no = '';
            $group->pso_no = '';

            $browser->visit(new auctionsPage())->sign_in()
            ->pick_random_branch($branch)
            ->create_new_group_in_branch($branch, $group, $date)
            ->add_first_auction_entry($group->name, $date)
            ->assertSee('PSO and By Law information is required before creating auctions.')
            ->delete_group($group);
        });
    }
}
