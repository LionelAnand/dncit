<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class editEnrollmentTest extends DuskTestCase
{
    public function testEditEnrollment()
    {
        dump('Edit Enrollment Test');

        $this->browse(function (Browser $browser) {
            $branch = (object)[];$subscriber =(object)[]; $employee =(object)[]; $employee2 =(object)[]; 
            $introducer = (object)[];$introducer2 = (object)[];$group = (object)[]; $enrollment = (object)[];

            $date = date('d-m-Y');

            $browser->visit(new employeesPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($employee2)
                    ->pick_employee($introducer)
                    ->pick_employee($introducer2);
            $group->scheme = 1; $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer,$enrollment);
             $browser->edit_enrollment($group, $subscriber, $employee2, $introducer2, $enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}
