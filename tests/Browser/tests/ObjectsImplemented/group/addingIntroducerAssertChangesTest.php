<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class addingIntroducerAssertChangesTest extends DuskTestCase
{
    public function testaddingIntroducerAssertChanges()
    {
        $this->browse(function (Browser $browser) {
            $date = date('d-m-Y');
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $dates = (object)[];

            $browser->visit(new groupsPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer);
            $date = date('d-m-Y');
            $dates->from_date = $browser->set_back_date(100)->back_date;
            $dates->to_date = $date;
            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->enroll_new_subscriber(
                $group,
                $subscriber,
                $employee,
                $introducer,
                $enrollment
            )
                ->visit(new reportsPage)
                ->generate_subscriber_wise_Ato_Report($branch, $group, $dates)
                ->assertsee($group->name)
                ->click_group_name_in_current_page($group->name)
                ->go_to_enrollment_with_id($enrollment)
                ->clickLink('Intro. Comm.')
                ->assertsee('Introduced by: '.$introducer->name)
                ->delete_group($group);
        });
    }
}
