<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\prizedMoneyPage;

class flAfterWinningAuctionAssertionTest extends DuskTestCase
{
    public function testFlAfterWinningAuctionAssertionTest()
    {
        $this->browse(function (Browser $browser) {
            dump('fl After Winning Auction Assertion Test');

            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];

            $date = date('d-m-Y');
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->visit(new employeesPage)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
           $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                  ->add_first_auction_entry($group->name, $date)
                    ->log_out()
                    ->sign_in_as_group_manager()
                    ->get_bid_amount_with_percentage($group, $enrollment);
                    dump($enrollment->id);
            $this->assertEquals($enrollment->future_liab, 0);
            $browser->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                   
                    ->log_out()
                    ->sign_in()
                    ->get_group_details_with_id($group)
                    ->get_enrollment_details_with_id($enrollment);

                 $this->assertEquals($enrollment->future_liab, $group->fl);
            $browser->remove_disbursed_prize_money($enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}
