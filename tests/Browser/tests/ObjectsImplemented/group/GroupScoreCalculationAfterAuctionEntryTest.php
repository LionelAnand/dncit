<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\collectionsPage;
use Tests\DuskTestCase;

class GroupScoreCalculationAfterAuctionEntryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testGroupScoreCalculationAfterAuctionEntry()
    {
        $this->browse(function (Browser $browser) {
            $date = date('d-m-Y');
            $group = (object)[];
            $score1 = (object)[];
            $score2 = (object)[];
            $enrollment = (object)[];

            $browser ->visit(new collectionsPage)->sign_in()
                    ->pick_random_group_l($group)
                    ->calculate_group_score($group, $score1)
                    ->select_enrollment_for_auction($group, $enrollment);
            $this->assertEquals($score1->score, $score1->score_in_erp);
            $browser->visit(new auctionsPage)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, '30000')
                    ->visit(new collectionsPage)
                    ->pause(60000)
                    ->calculate_group_score($group, $score2);
            $this->assertEquals($score2->score, $score2->score_in_erp);
            $browser->visit(new auctionsPage)
                 ->remove_last_auction($group);
        });
    }
}
