<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\DuskTestCase;

class groupEditTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testGroupEdit()
    {
        $this->browse(function (Browser $browser) {
            dump('testGroupEdit'); $branch = (object)[];
            $group = (object)[];

            $date = date('d-m-Y');

            $browser->visit(new groupsPage)->sign_in();
            $browser->pick_random_branch($branch);
            $group->scheme = 1; $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->open_group_edit($group)
                    ->fill_group_edit_details(
                    [
                        'MonthyAuctionDate1' => '10',
                        'AuctionTime' => '1000am',
                        'PSONo' => '080596',
                        'PSODate' => '02072019',
                        'ByLawNo' => '08051996',
                        'ByLawDate' => '02072019',
                        'FDAmount' => '200000',
                        'FDInterest' => '7.5',
                        'FDDate' => '02072019',
                        'FDNumber' => '851996',
                        'FDReferenceNo' => '740172',
                        'FDBankName' => 'BOB',
                        'FDBranchName' => 'PORUR',
                    ]
                )
                ->press('Save')
                ->assert_group_details_edited()
                ->delete_group($group);
        });
    }
}
