<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class assertThatGroupAppearsInSubscriberPageTest extends DuskTestCase
{
    public function test_assert_that_group_appears_in_subscriber_page()
    {
        $this->browse(function (Browser $browser) {
            dump('test_assert_that_group_appears_in_subscriber_page');

            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];

            $date = date('d-m-Y');
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->visit(new employeesPage)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->go_to_subscriber_with_id($subscriber)
                    ->assertSee($group->name)
                    ->delete_group($group);
        });
    }
}
