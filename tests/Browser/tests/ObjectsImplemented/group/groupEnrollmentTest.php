<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\employeesPage;

class groupEnrollmentTest extends DuskTestCase
{
    public function testSubscriberCanBeEnrollmedToAGroup()
    {
        /**************************** Test Name ****************************/
        dump('groupEnrollmentTest');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();
            $browser->pick_subscriber($subscriber)->pick_employee($employee)->pick_employee($introducer);
            $browser->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}