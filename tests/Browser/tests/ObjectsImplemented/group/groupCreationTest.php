<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\groupsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class groupCreationTest extends DuskTestCase
{
    public function testGroupCreate()
    {
        $this->browse(function (Browser $browser) {
            dump('Group Creation Test');
            $group = (object)[];
            $branch = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new groupsPage)->sign_in();
            $browser->pick_random_branch($branch);
            $group->scheme = 1; $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->search_and_delete_the_group($group);
        });
       
    }
}
