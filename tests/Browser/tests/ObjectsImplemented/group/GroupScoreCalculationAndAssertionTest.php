<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\collectionsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class GroupScoreCalculationAndAssertionTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testGroupScoreCalculationAndAssertion()
    {
        /**************************** Test Name ****************************/
                    dump('Group Score Calculation And Assertion Test');
        /**************************** Test Name ****************************/

     $this->browse(function (Browser $browser) {
        $group = (object)[];
        $score1 = (object)[];
        $enrollment = (object)[];

        $browser ->visit(new collectionsPage)->sign_in()
                ->pick_random_group_l($group)
                ->calculate_group_score($group, $score1)
                ->select_enrollment_for_auction($group, $enrollment);
        $this->assertEquals($score1->score, $score1->score_in_erp);
       
    });
    }
}
