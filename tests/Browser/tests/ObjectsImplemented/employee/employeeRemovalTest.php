<?php

namespace Tests\Browser;

use Faker\Factory;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class employeeRemovalTest extends DuskTestCase
{
    public function testemployeeRemoval()
    {
        dump('testemployeeRemoval');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $browser->visit(new employeesPage)->sign_in();
            $employee = Factory::create();
            $browser->format_mobile($employee)
                        ->pick_random_branch($branch)
                        ->create_new_employee($branch, $employee);
            $employee->branch_id=$browser->get_branch_id($employee->branch)->id;
            $browser->remove_this_employee($employee);
        });
    }
}
