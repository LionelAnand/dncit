<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class transferEnrollmentAndAssertTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testtransferEnrollmentAndAssert()
    {
        $this->browse(function (Browser $browser) {
            $employee = (object)[];
            $employee2 = (object)[];
            $browser->visit(new employeesPage)->sign_in()
               ->pick_employee($employee)
               ->pick_employee($employee2)
               ->transfer_all_enrollments_from_one_employee_to_other_employee($employee->branch, $employee->name, $employee2->mobile, $employee2->name);
        });
    }
}
