<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\branchesPage;
use Tests\Browser\Pages\employeesPage;

class cannotDeleteEmployeeHaveActiveEnrollementTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testcannotDeleteEmployeeHaveActiveEnrollement()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];

            $browser->visit(new employeesPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->remove_an_employee_with_managing_enrollments($branch);
        });
    }
}
