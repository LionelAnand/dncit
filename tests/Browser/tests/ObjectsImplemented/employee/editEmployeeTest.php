<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\employeesPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class editEmployeeTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testEmployeeEdit()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];$employee = (object)[];
            $browser->visit(new employeesPage)->sign_in()
                    ->pick_employee($employee);
            $edit_employee = Factory::create();
            $browser->format_mobile($edit_employee)
                    ->edit_employee_details($employee, $edit_employee);
        });
    }
}
