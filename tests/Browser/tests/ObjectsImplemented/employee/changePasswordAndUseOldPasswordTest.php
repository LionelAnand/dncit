<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\employeesPage;

class ChangePasswordAndUseOldPasswordTest extends DuskTestCase
{
    public function test_change_password_for_this_employee_and_use_old_password()
    {
        dump('ChangePasswordAndUseOldPasswordTest');

        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $employee = (object)[];
            $date = date('d-m-Y');

            $browser->visit(new employeesPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_employee($employee)
                    ->get_employee_credentials($employee);
                    
            $provision_button_text = $browser->text('#employee_details_tab > p > a:nth-child(2)');
            if ($provision_button_text == 'Provision User Access') {
                $browser->click_provision_user_access()
                    ->fill_in_credentials($employee->id_name, 'password')
                    ->check_group_admin()
                    ->click_provision_button()
                    ->assert_to_confirm('User provisioned successfully.');
            } else {
                $browser->click_manage_user_access()
                        ->type('#password','password');
                 $employee->id_email = $browser->value('#email');
                 $browser->save();
            }
            $browser->change_password_for_this_employee($employee, 'new_password')
                    ->log_out()
                    ->custom_sign_in($employee->id_email, 'password')
                    ->assertSee('These credentials do not match our records.')
                    ->sign_in()
                    ->search_and_go_to_employee($employee)
                    ->delete_user_access();
        });
    }
}
