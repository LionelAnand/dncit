<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class dontTransferEnrollmentsIfEmployeeNameIsNotProvidedAndAssertTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testdontTransferEnrollmentsIfEmployeeNameIsNotProvidedAndAssert()
    {
        $this->browse(function (Browser $browser) {
            $employee = (object)[];
            $browser->visit(new employeesPage)->sign_in()
                    ->pick_employee($employee)
                    ->go_to_employee($employee->branch, $employee->name)
                    ->dont_transfer_enrollments_when_employee_not_selected_from_dropdown_list($employee->branch, $employee->name, ' ')
                    ->assertsee('The employee field is required.');
        });
    }
}
