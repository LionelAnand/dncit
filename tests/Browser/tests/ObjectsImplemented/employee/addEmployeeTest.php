<?php

namespace Tests\Browser;

use Faker\Factory;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\DuskTestCase;

class addEmployeeTest extends DuskTestCase
{
    use withFaker;

    public function testAddEmployee()
    {
        dump('addEmployeeTest');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $browser->visit(new employeesPage)->sign_in();
            $employee = Factory::create();
            $browser->format_mobile($employee)
                    ->pick_random_branch($branch)
                    ->create_new_employee($branch, $employee);
        });
    }
}
