<?php
namespace Tests\Browser;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;


class mergeSubscriberTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testMergeSubscriber()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 

            $subscriber1 = (object)[]; 
            /***** Create Objects for modules to store multiple values *****/

            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $browser->click_subscribers_tab_in_branch_ledger();

            $subscriber1 = Factory::create(); $subscriber2 = Factory::create();

            $browser->format_mobile($subscriber1);  $browser->format_mobile($subscriber2);

            $browser->create_a_new_subscriber($branch, $subscriber1, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber1, 'subscriber')->assert_subscriber_details($subscriber1);

            $browser->create_a_new_subscriber($branch, $subscriber2, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber2, 'subscriber')->assert_subscriber_details($subscriber2);

            $browser->merge_a_subscriber($subscriber1)->assert_subscriber_merged();

            $browser->assert_absence_of_merged_subscriber_in_global_search($subscriber1);

        });
    }
}
