<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\groupsPage;

class LockedSubscribersCanNotAddCollectionTest extends DuskTestCase
{

    public function testLockedSubscribersCanNotAddCollectionTest()
    {
        dump("Can't Add Collections for Locked Subscriber");
        $this->browse(function (Browser $browser) {
            
            $subscriber = (object)[];
            $employee = (object)[];
            $branch = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $introducer = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();
            $browser->pick_employee($employee)->assertsee($employee->name, $employee->mobile)
                    ->pick_employee($introducer)->assertsee($introducer->name, $introducer->mobile)
                    ->pick_subscriber($subscriber)->assertsee($subscriber->name, $subscriber->mobile);
           $browser->pick_random_branch($branch);
            $group->scheme = 3;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date)
            ->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment);
            $browser->clickLink($subscriber->name);
            try{
            $browser->go_to_details_tab()
                        ->check_subscriber_status_lock()
                        ->assert_edited_subscriber_details_saved()
                        ->assert_subscriber_locked();
                } catch (\Exception $e) {
            $browser->go_to_details_tab()
                        ->check_subscriber_status_unlock()
                        ->assert_edited_subscriber_details_saved();                               
                }
            $browser->visit(new auctionsPage)
                    ->go_to_group($group->name);
            $browser->clickLink($subscriber->name)
                    ->go_to_ledgers_tab_in_enrollment_ledger()
                    ->assert_add_collections_are_locked();
                    echo "Collections Cant Be Made For Locked Subscribers";
        });
    }
}
