<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Faker\Generator as Faker;
use Tests\Browser\Pages\loginPage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class sameSubscriberCannotBeMergedTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testsameSubscriberCannotBeMerged()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            /***** Create Objects for modules to store multiple values *****/

            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $browser->click_subscribers_tab_in_branch_ledger();

            $subscriber = Factory::create();

            $browser->format_mobile($subscriber);

            $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber, 'subscriber')->assert_subscriber_details($subscriber);

            $browser->merge_a_subscriber($subscriber)->assert_same_subscriber_cannot_be_merged();

        });
    }
}
