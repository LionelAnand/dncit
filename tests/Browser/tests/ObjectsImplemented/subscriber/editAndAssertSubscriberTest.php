<?php
namespace Tests\Browser;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;


class editAndAssertSubscriberTest extends DuskTestCase
{
    public function testeditAndAssertSubscriber()
    {
        $this->browse(function (Browser $browser) {

            /**************************** Test Name ****************************/
            dump('Subscriber Edit Test');
            /**************************** Test Name ****************************/

            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; 
            $subscriber1 = (object)[]; $subscriber2 = (object)[]; 
            /***** Create Objects for modules to store multiple values *****/

            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $browser->click_subscribers_tab_in_branch_ledger();

            $subscriber1 = Factory::create(); $subscriber2 = Factory::create();

            $browser->format_mobile($subscriber1);  $browser->format_mobile($subscriber2);

            $browser->create_a_new_subscriber($branch, $subscriber1, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber1, 'subscriber')->assert_subscriber_details($subscriber1);

            $browser->click_edit_subscriber_details_in_subscriber_ledger()->edit_subscriber_details($branch, $subscriber2)->assert_edited_subscriber_details_saved()->go_to_details_tab();

            $browser->get_member_details($subscriber2, 'subscriber')->assert_edited_subscriber_details($subscriber2);

        });
    }
}
