<?php

namespace Tests\Browser;

use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class lockTheSubscriberTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testlockTheSubscriber()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");
            dump('Locking The Subscriber Test');
            $browser->visit(new loginPage)->sign_in();
            $group = (object)[];
            $branch = (object)[]; 
            $subscriber = (object)[]; 
            $browser->pick_random_branch($branch);
            $browser->click_subscribers_tab_in_branch_ledger();
            $subscriber = Factory::create();
            $browser->format_mobile($subscriber);
            $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/Nature.jpg')
                    ->go_to_subscriber_details_tab();
            try {
                $browser->assertSee('This account has been locked by an Administrator.')
                    ->check_subscriber_status_unlock()
                    ->assertsee('Subscriber details saved.')
                    ->assertsee('This account has been locked by an Administrator.');
            } catch (\Exception $e) {
                $browser->clickLink('Details')
                    ->check_subscriber_status_lock()
                    ->assertsee('Subscriber details saved.')
                    ->assertsee('This account has been locked by an Administrator.');
            }
        });
    }
}