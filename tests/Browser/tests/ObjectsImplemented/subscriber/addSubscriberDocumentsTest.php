<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;
use Tests\Browser\Pages\groupsPage;

class addSubscriberDocumentsTest extends DuskTestCase
{
    public function testAddSubscriberDocuments()
    {

        /**************************** Test Name ****************************/
        dump('Add Subscriber Documents Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
        $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
        $employee = (object)[]; $agent = (object)[]; $commission = (object)[];
        /***** Create Objects for modules to store multiple values *****/

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;
            /***** Get Branch Details *****/

            $subscriber = Factory::create();

            $browser->format_mobile($subscriber);

            $browser->click_subscribers_tab_in_branch_ledger();

            $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->assert_subscriber_created();

            $browser->get_member_details($subscriber, 'subscriber');

            $browser->go_to_documents_tab()->add_subscriber_document_in_subscriber_ledger()->upload_document('5', $subscriber);

                $browser->go_to_documents_tab()->assert_document('Aadhar.jpg')
                ->delete_the_subscriber_documents();

        });
    }
}