<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Faker\Generator as Faker;
use Tests\Browser\Pages\loginPage;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;



class createNewSubscriberTest extends DuskTestCase
{
    public function testcreateNewSubscriber()
    {

        /**************************** Test Name ****************************/
        dump('Subscriber Entry Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            /***** Create Objects for modules to store multiple values *****/


            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $browser->click_subscribers_tab_in_branch_ledger();

            $subscriber = Factory::create();

            $browser->format_mobile($subscriber);
            
            $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->assert_subscriber_created();


        });
    }
}
