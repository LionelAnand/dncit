<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class addRelationshiptoUniqueSubscriberAssertTest extends DuskTestCase
{
   
    public function testaddRelationshiptoUniqueSubscriberfails()
    {
        dump('Adding Unique Subscriber Relationship Test');
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();
            $branch_name = $browser->get_branch_name()->name;            
            $subscriber = (object)[];
            $browser->pick_subscriber($subscriber);
            $browser->assertsee($subscriber->name, $subscriber->mobile);
            $browser->adding_relationship_to_a_subscriber($subscriber);
            $browser->assertsee("A relationship can only be added between two different individuals.");   
            echo "Relationship cant be added select another entity"; 
        });
    }
}
