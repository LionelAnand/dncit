<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;

class addRelationShipAndAssertingTest extends DuskTestCase
{
   
    public function testSubscriberRelationShipadded()
    {
        dump('Add Relationship Between Two Subscribers Test');
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();
            $branch_name = $browser->get_branch_name()->name;
            $subscriber1 = (object)[];
            $subscriber2 = (object)[];
            $browser->pick_subscriber($subscriber1);
            $browser->assertsee($subscriber1->name, $subscriber1->mobile);
            $browser->pick_subscriber($subscriber2);
            $browser->assertsee($subscriber2->name, $subscriber2->mobile);
            $browser->adding_relationship_to_a_subscriber($subscriber1);
            $browser->assertsee("Relationship added."); 
            echo "Relationship Added Successfully";                           
        });
    }
}
