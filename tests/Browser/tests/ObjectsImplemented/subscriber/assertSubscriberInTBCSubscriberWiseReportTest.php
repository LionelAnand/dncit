<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;


class assertSubscriberInTBCSubscriberWiseReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testassertSubscriberInTBCSubscriberWiseReport()
    {

      $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();


        /***** Create Objects for modules to store multiple values *****/
        $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; $after_auction = (object)[];
        $employee = (object)[]; $agent = (object)[]; $enrollment  = (object)[];

        $date = $browser->get_todays_date()->date;

        /**** Pick random subscriber, Employee and Agent and get their details *****/
        $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee)->pick_agent($agent);

        $group->scheme = 20; $group->policy = 1;

        /******************** Create a new group in Policy 1A ********************/
        $browser->create_new_group_in_branch($branch, $group, $date);

        $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);
        /******************** Create a new group in Policy 1A ********************/  

        $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

      $registration_fees = ($group->chit_value * 0.001);  // Registration fees - 0.1%

      $registration_fees = round($registration_fees);

      $enrollment->tbc = substr($enrollment->tbc, 0, -11);

      $enrollment->tbc = $browser->format_amount($enrollment->tbc)->format_amount;

      $this->assertEquals($registration_fees, $enrollment->tbc);

      $browser->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)
      ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, '1500')->clickLink($subscriber->name);

      $after_auction->to_be_collected_amount = $browser->get_details_in_enrollment_ledger($after_auction)->format_amount($after_auction->to_be_collected_amount)->format_amount;

      $browser->go_to_subscriber_wise_tbc_report($branch->id, '1', '', '', '')->assertSee($subscriber->name)->search_and_delete_the_group($group);
    });
    }
  }
