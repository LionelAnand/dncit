<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\groupsPage;

class lockedSubscribersCanNotDisbursePrizeMoneyTest extends DuskTestCase
{

    public function testLockedSubscribersCanNotDisbursePrizeMoneyTest()
    {
        dump("Can't Disburse Prize Money For Locked Subscriber");
        $this->browse(function (Browser $browser) {
            
            $subscriber = (object)[];
            $employee = (object)[];
            $branch = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $introducer = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();
            $browser->pick_employee($employee)->assertsee($employee->name, $employee->mobile)
                    ->pick_employee($introducer)->assertsee($introducer->name, $introducer->mobile)
                    ->pick_subscriber($subscriber)->assertsee($subscriber->name, $subscriber->mobile);
           $browser->pick_random_branch($branch);
            $group->scheme = 3;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->assertsee($group->name, $subscriber->name, $employee->name, $introducer->name);
            $browser->visit(new auctionsPage)
                    ->add_first_auction_entry($group->name, $date, '');
            $browser->add_auction_entry_for_unlocked_subscriber($group, $date, $enrollment->id, '10000')
                    ->assert_auction_is_saved();
            $browser->go_to_subscriber($subscriber->mobile);
            try{
            $browser
                    ->go_to_details_tab()
                        ->check_subscriber_status_lock()
                        ->assert_edited_subscriber_details_saved()
                        ->assert_subscriber_locked();             
                } catch (\Exception $e) {
            $browser->go_to_details_tab()
                        ->check_subscriber_status_unlock()
                        ->assert_edited_subscriber_details_saved();                  
                }
            $browser->visit(new auctionsPage)
                    ->go_to_group($group->name)
                    ->clickLink($subscriber->name)
                    ->go_to_prize_money_tab_in_enrollment_ledger()
                    ->assert_disburse_prize_money_is_locked();
                    echo "Prize Money Can't be Disbursed For Locked Subscribers";
        });
    }
}
