<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;

class SubscriberIDGenerationTest extends DuskTestCase
{

    public function testSubscriberIDGenerationTest(){
        
        $this->browse(function (Browser $browser) {
        dump('Subscriber ID Generation Test');  
        $branch = (object)[]; $subscriber = (object)[];
        $browser->visit(new loginPage)->sign_in();  
        
        $browser->pick_random_branch($branch);
        $browser->click_subscribers_tab_in_branch_ledger();
        $subscriber = Factory::create();
        $browser->format_mobile($subscriber);
        $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->assert_subscriber_created();
        $browser->get_member_details($subscriber, 'subscriber')->assert_subscriber_details($subscriber)->assert_subscriber_id_generated($subscriber);
        });
    }
}
