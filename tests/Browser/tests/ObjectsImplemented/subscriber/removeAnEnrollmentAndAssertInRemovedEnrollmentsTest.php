<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\comissionsPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Support\Facades\URL;



class addRemoveEnrollmentAndAssertInRemovedEnrollmentsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testaddRemoveEnrollmentAndAssertInRemovedEnrollments()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

           /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            $employee = (object)[]; $agent = (object)[]; $commission = (object)[]; $receipt = (object)[];$enrollment = (object)[];

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            /***** Get Commission Percentage for policy 1A *****/
            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('agent', '1A','D1', '1')->commission_percentage;

            /**** Pick random subscriber, Employee and Agent and get their details *****/
            $browser->pick_subscriber($subscriber);

            $browser->pick_employee($employee);

            $browser->pick_agent($agent);

            $group->scheme = 20; $group->policy = 1;

            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);

            /******************** Add Collection Entry - Cash Mode  ********************/
            $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Receipt Entry Cash Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
            /******************** Add Collection Entry - Cash Mode  ********************/

            $browser->get_details_in_receipt($receipt)->clickLink($receipt->subscriber);
            $browser->clickLink($enrollment->name)->remove_the_enrollment();
            $browser->go_to_removed_enrollments_report($branch,'1')->assert_removed_enrollment_in_removed_enrollment_report($subscriber, $enrollment, $date);           
            

        });
    }
}
