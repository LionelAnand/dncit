<?php

namespace Tests\Browser;

use Exception;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AssertTBCBeforeAndAfterReceiptEntryTest extends DuskTestCase
{
  public function testTBCIsUpdatedAfterAddingCollectionReceipt()
  {

    $this->browse(function (Browser $browser) {
      $browser->visit(new loginPage)->sign_in();

      /***** Create Objects for modules to store multiple values *****/
      $branch = (object)[]; $enrollment = (object)[]; 
      $enrollment_ledger = (object)[]; $enrollment_ledger1 = (object)[]; $receipt = (object)[];
      /***** Create Objects for modules to store multiple values *****/

      $date = $browser->get_todays_date()->date;

      $browser->pick_random_branch($branch);

      do {

        $browser->pick_random_enrollment($enrollment);

        $enrollment_ledger->to_be_collected_amount = $browser->get_details_in_enrollment_ledger($enrollment_ledger)->format_amount($enrollment_ledger->to_be_collected_amount)->format_amount;

      } while($enrollment_ledger->to_be_collected_amount == 0);

      /******************** Add Collection Entry - Cash Mode ********************/
      $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Receipt Entry Cash Test')->save_the_collections()->assert_that_collection_receipt_is_saved();

      /******************** Delete Receipt  ********************/
      $browser->get_details_in_receipt($receipt)->clickLink($receipt->subscriber)->visit($enrollment->url)->Regenerate_settlements_in_enrollment_ledger();

      $enrollment_ledger1->to_be_collected_amount = $browser->get_details_in_enrollment_ledger($enrollment_ledger1)->format_amount($enrollment_ledger1->to_be_collected_amount)->format_amount;

      if ($enrollment_ledger1->to_be_collected_amount < $enrollment_ledger->to_be_collected_amount)
      {
        $browser->clickLink($receipt->number)->delete_the_collection_receipt()->assert_receipt_deleted();
      }

      else
      {
        $error = 'TBC is not updated after adding a collection';
        throw new Exception($error);              
      }

    });
  }
}
