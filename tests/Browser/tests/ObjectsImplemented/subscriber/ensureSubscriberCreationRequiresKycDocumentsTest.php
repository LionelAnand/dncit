<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ensureSubscriberCreationRequiresKycDocumentsTest extends DuskTestCase
{
    public function test_ensure_group_enrollment_requires_kyc_documents()
    {
        $this->browse(function (Browser $browser){

            dump('Ensure Subscriber Creation Requires KYC Documents Test');

            $subscriber = (object)[]; $employee = (object)[]; $introducer = (object)[]; $branch = (object)[]; $group = (object)[]; $enrollment = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new loginPage)->sign_in(); 
            $browser->pick_random_branch($branch)->click_subscribers_tab_in_branch_ledger();

            $subscriber = Factory::create();

            $browser->format_mobile($subscriber);
            
            $browser->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber, 'subscriber')->assert_subscriber_details($subscriber);

            $employee = Factory::create(); $browser->format_mobile($employee);

            $browser->pick_employee($employee)->pick_employee($introducer);

            $access = $browser->text('#employee_details_tab > p > a:nth-child(2)');

            $browser->provision_useraccess($access, $employee, 'Group Manager');

            $group->scheme = 20; $group->policy = 1;

            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment);

            $browser->log_out()->custom_sign_in($employee->username, '123456')->visit($group->url);

            $browser->just_enroll_subscriber($group, $subscriber, $employee, $employee)
                ->assert_KYC_documents_are_mandatory_to_enroll_a_subscriber_in_group();

        });
    }
}
