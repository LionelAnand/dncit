<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;

class lockedSubscriberCantAddAuctionTest extends DuskTestCase
{

    public function testLockedSubscriberCantAddAuctionTest()
    {
        dump('Restrict Auction Entry For Un-Locked Susbcriber');
        $this->browse(function (Browser $browser) {
            
            $subscriber = (object)[];
            $employee = (object)[];
            $enrollment = (object)[];
            $introducer = (object)[];
            $branch = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();
            $browser->pick_random_branch($branch)->pick_employee($employee)->assertsee($employee->name, $employee->mobile)
                    ->pick_employee($introducer)->assertsee($introducer->name, $introducer->mobile)
                    ->pick_subscriber($subscriber)->assertsee($subscriber->name, $subscriber->mobile);

                    $group->scheme = 20; $group->policy = 1;

                    $browser->create_new_group_in_branch($branch, $group, $date);

                    $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment);
  
            $browser->go_to_subscriber($subscriber->name);
            try{
                $browser->clickLink('Details')
                        ->check_subscriber_status_lock()
                        ->assertsee('Subscriber details saved.')
                        ->assertsee('This account has been locked by an Administrator.');                     
                    } catch (\Exception $e) {
                $browser->clickLink('Details')
                        ->check_subscriber_status_unlock()
                        ->assertsee('Subscriber details saved.');                               
                }
                $browser->visit(new auctionsPage)
                    ->go_to_group($group->name)
                    ->add_first_auction_entry($group->name, $date)
                    ->add_auction_entry_for_unlocked_subscriber($group, $date, $enrollment->id,'60000')
                    ->assertsee("The selected subscriber's account is locked and hence cannot become the winner of the auction.");
        });
    }
}
