<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\employeesPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class lockedSubscribersCantBeEnrolledInAGroupTest extends DuskTestCase
{
    public function testLockedSubscribersCantBeEnrolledInAGroupTest()
    {
        dump('Locked Subscriber Cant Be Enrolled In A Group');
        $this->browse(function (Browser $browser) {
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $branch = (object)[];
            $group = (object)[];
            $subscriber1 = (object)[];
            $enrollment = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();

            $browser->pick_employee($employee)->assertsee($employee->name, $employee->mobile)
                    ->pick_employee($introducer)->assertsee($introducer->name, $introducer->mobile);
           $browser->pick_random_branch($branch);
            $group->scheme = 3;
            $group->policy = 3;

            $subscriber1 = Factory::create();

            $browser->format_mobile($subscriber1);

            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->create_a_new_subscriber($branch, $subscriber1, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber1, 'subscriber')->assert_subscriber_details($subscriber1);

            $browser->click_edit_subscriber_details_in_subscriber_ledger()->lock_a_subscriber_profile()->press('Save')->assert_edited_subscriber_details_saved()->assert_subscriber_locked();    
           
            $browser->visit(new groupsPage)->just_enroll_subscriber($group, $subscriber1, $employee, $introducer, $enrollment);
    
            $browser->assert_subscriber_locked_by_admin_to_enroll();
                    
        });
    }
}
