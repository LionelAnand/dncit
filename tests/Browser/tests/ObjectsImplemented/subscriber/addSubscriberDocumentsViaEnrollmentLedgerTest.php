<?php

namespace Tests\Browser;

use Exception;
use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class addSubscriberDocumentsViaEnrollmentLedgerTest extends DuskTestCase
{
    public function testAddSubscriberDocumentsViaEnrollmentLedger()
    {

        /**************************** Test Name ****************************/
        dump('Add Subscriber Documents Via Enrollment Ledger Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[]; $employee = (object)[]; $agent = (object)[]; 
            $subscriber = (object)[]; $document = (object)[]; $document1 = (object)[]; $enrollment = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            /***** Get Branch Details *****/
            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $subscriber = Factory::create();

            $browser->format_mobile($subscriber);

            $browser->click_subscribers_tab_in_branch_ledger()->create_a_new_subscriber($branch, $subscriber, '/Pictures/meow.jpg')->get_member_details($subscriber, 'subscriber')->go_to_documents_tab()->get_number_of_documents_in_subscriber_ledger($document);

            /**** Pick random employee and get their details *****/ 
            $browser->pick_employee($employee)->pick_agent($agent);

            $group->scheme = 20; $group->policy = 1;

            /******************** Create a new group in Policy 1A ********************/
            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $agent, $enrollment);
            /******************** Create a new group in Policy 1A ********************/

            $browser->go_to_documents_tab_in_enrollment_ledger()->add_subscriber_document_through_enrollment_ledger()->upload_document('5', $subscriber)->assert_document_added();

            $browser->go_to_documents_tab()->get_number_of_documents_in_subscriber_ledger($document1);

            if ($document1->count > $document->count)
            {
                $browser->delete_a_particular_document($document1)->assert_document_deleted();
            }

            else 
            {
                $error = 'Document has not been added via enrollment ledger';
                throw new Exception($error);
            }

        });
    }
}
