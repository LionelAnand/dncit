<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class EnrollSubscriberAfterAddingKYCDocumentsTest extends DuskTestCase
{
    
    public function testEnrollSubscriberAfterAddingKYCDocuments()
    {
        $this->browse(function (Browser $browser){
            dump('Enroll Subscriber After Adding KYC Documents Test');
            $subscriber = (object)[]; $group = (object)[]; $employee = (object)[];
            $introducer = (object)[]; $branch = (object)[];$enrollment = (object)[];
            
            $date = date('d-m-Y');
            $browser->visit(new loginPage)->sign_in();          
            $browser->pick_employee($employee)->assertsee($employee->name);
            $browser->pick_employee($introducer)->assertsee($introducer->name);
            $browser->pick_subscriber($subscriber)->assertsee($subscriber->name);
            $browser->check_subscriber_kyc_document();
            $browser->click_add_document_in_subscriber_ledger() 
            ->upload_document('3', $subscriber);
            $browser->go_to_documents_tab()->click_add_document_in_subscriber_ledger()
                    ->upload_document('2', $subscriber)->assertSee('New document saved.');
            $browser->pick_random_branch($branch);
            $group->scheme = 3;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->assertsee('Group Details')->visit(new groupsPage)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->assertsee($group->name, $subscriber->name, $employee->name, $introducer->name);
        });
    }
}



