<?php

namespace Tests\Browser;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Faker\Factory;
use Faker\Generator as Faker;
use Illuminate\Foundation\Testing\WithFaker;

class lockedSubscribersCannotBeMergedTest extends DuskTestCase
{
    /**
     * locked Subscribers Can not Be Merged Test.
     *
     * @return void
     */
    public function testlockedSubscribersCannotBeMerged()
    {

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $group = (object)[]; $branch = (object)[];  
            $subscriber1 = (object)[]; $subscriber2 = (object)[];  
            /***** Create Objects for modules to store multiple values *****/

            $browser->pick_random_branch($branch);

            $date = $browser->get_todays_date()->date;

            $browser->click_subscribers_tab_in_branch_ledger();

            $subscriber1 = Factory::create();

            $browser->format_mobile($subscriber1);

            $browser->create_a_new_subscriber($branch, $subscriber1, '/Pictures/meow.jpg')->assert_subscriber_created()->go_to_details_tab();

            $browser->get_member_details($subscriber1, 'subscriber')->assert_subscriber_details($subscriber1);

            $browser->click_edit_subscriber_details_in_subscriber_ledger()->lock_a_subscriber_profile()->press('Save')->assert_edited_subscriber_details_saved()->assert_subscriber_locked();

            $browser->pick_subscriber($subscriber2)->go_to_details_tab()->get_member_details($subscriber2, 'subscriber')->merge_a_subscriber($subscriber1)->assert_locked_subscriber_cannot_be_merged();

        });
    }
}
