<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\collectionsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class receiptEntryRtgsTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testWhetherRtgsModeInReceiptIsEnteredAssertedAndDeleted()
    {
        
        /**************************** Test Name ****************************/
        dump('Receipt Entry RTGS Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $branch = $browser->get_branch_name()->name;

            $browser->click_branch_name_in_branches_page($branch);

            $enrollment = $browser->click_enrollments_in_branch_ledger()->get_enrollment_name();

            $browser->go_to_enrollment_from_enrollments_page($enrollment->name);

            $date = $browser->get_todays_date()->date;

        /******************** Add Collection Entry - RTGS Mode  ********************/
        $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'RTGS', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200', 'Receipt Entry RTGS Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
        /******************** Add Collection Entry - RTGS Mode  ********************/

        /******************** Delete Receipt  ********************/
        $browser->get_details_in_receipt($receipt);

        $browser->delete_the_receipt_through_enrollment_ledger($receipt)->assert_receipt_deleted();
        /******************** Delete Receipt  ********************/


        });
    }
}
