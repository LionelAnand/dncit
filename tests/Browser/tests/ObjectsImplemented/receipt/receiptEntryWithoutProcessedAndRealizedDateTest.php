<?php

namespace Tests\Browser;

use Exception;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Tests\Browser\Pages\collectionsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class receiptEntryWithoutProcessedAndRealizedDateTest extends DuskTestCase {
	/**
	 * A Dusk test example.
	 *
	 * @return void
	 */
	public function testWithoutEnteringProcessedDateGoesToAdditionalCollection() {
		
		/**************************** Test Name ****************************/
        dump('Receipt Entry Without Processed and Realized Date Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[]; $transactor = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment_ledger = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            $browser->pick_random_branch($branch);

            $browser->pick_employee($transactor);

            $browser->pick_random_enrollment($enrollment);            

            $browser->click_toggle_settlement_in_enrollment_ledger();

            $browser->get_details_in_enrollment_ledger($enrollment_ledger);

            $settled_amount_before = $enrollment_ledger->settled_amount;

            $settled_amount_before = $browser->format_name($enrollment_ledger->settled_amount)->formatted_name;

            /******************** Add Collection Entry - Cheque Mode  ********************/
            $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cheque', '123', 'KVB', 'Chennai', '', '', '', '' ,'Receipt Entry Without Processed and Realized Date Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
            /******************** Add Collection Entry - Cheque Mode  ********************/

            $browser->get_details_in_receipt($receipt);

            $browser->clickLink($receipt->subscriber)->assertSee('Additional Collection');

            $browser->click_toggle_settlement_in_enrollment_ledger()->get_details_in_enrollment_ledger($enrollment_ledger);

            $settled_amount_after = $enrollment_ledger->settled_amount;

            $settled_amount_after = $browser->format_name($enrollment_ledger->settled_amount)->formatted_name;

            /******************** Delete Receipt  ********************/
            if ($settled_amount_after == $settled_amount_before) 
            {
                $browser->clickLink($receipt->number)->delete_the_collection_receipt()->assert_receipt_deleted();
            }

            else 
            {
              $error = 'The amount gets added without it is been realized';
              throw new Exception($error);
            }
          /******************** Delete Receipt  ********************/

      });
    }
}
