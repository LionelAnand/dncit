<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\groupsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;
use Exception;

class AdvanceReceiptEntryAssertInAdditionalReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAdvanceReceiptEntryAssertInAdditionalReport()
    {
        /**************************** Test Name ****************************/
        dump('Advance Receipt Entry Assert In Additional Report');
        /**************************** Test Name ****************************/
    
        $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment = (object)[];
        $group = (object)[]; $subscriber = (object)[]; $employee = (object)[]; $enrollment_ledger = (object)[]; $enrollment_ledger1 = (object)[];

        $date = date('d-m-Y');

        /***** Get Branch Details, Pick random subscriber, Employee and Agent and get their details *****/            
        $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee);            

        $group->scheme = 20; $group->policy = 1; $enrollment->bid_percentage = 30;
        /******************** Create a new group in Policy 1A ********************/
        $browser->create_new_group_in_branch($branch, $group, $date);
        
        $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment)
        ->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)->get_bid_amount_with_percentage($group, $enrollment)
        ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)

       ->visit($enrollment->url)->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('4000', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Advance Receipt Entry Assert In Additional Report')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->visit($enrollment->url);

       $browser->get_details_in_enrollment_ledger($enrollment_ledger);

       $enrollment_ledger->to_be_collected_amount = $browser->format_amount($enrollment_ledger->to_be_collected_amount)->format_amount;

       $enrollment_ledger->balance_amount = $browser->format_amount($enrollment_ledger->balance_amount)->format_amount;

       $receipt->amount = $enrollment_ledger->to_be_collected_amount + 5000;

       $browser->visit($enrollment->url)->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form($receipt->amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Advance Receipt Entry Assert In Additional Report')
       ->save_the_collections()->assert_that_collection_receipt_is_saved();

       $browser->visit($enrollment->url);

       $browser->get_details_in_enrollment_ledger($enrollment_ledger1);

       $enrollment_ledger1->to_be_collected_amount = $browser->format_amount($enrollment_ledger1->to_be_collected_amount)->format_amount;

       $enrollment_ledger1->balance_amount_format = $browser->format_amount($enrollment_ledger1->balance_amount)->format_amount;

       if ($enrollment_ledger1->balance_amount_format > $enrollment_ledger->balance_amount)
       {
           $browser->go_to_additional_collections_report($branch->id)->assertSee($subscriber->name, $group->name, $enrollment_ledger1->balance_amount);
           $browser->delete_group($group);
       }

       else
       {
           $error = 'Additional collection is not seen on entering advance amount.';
           throw new Exception($error);
       }
             
        });
    }

}


