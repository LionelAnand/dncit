<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Tests\Browser\Pages\groupsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BackDateReceiptEntryRestrictionForCollectionRoleTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testBackDateReceiptEntryRestrictionForCollectionRoleTest()
    {
        /**************************** Test Name ****************************/
        dump('Back Date Receipt Entry Restriction for Collection Role Test');
        /**************************** Test Name ****************************/
        
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();
            $back_date = $browser->set_back_date('5')->back_date;

            /***** Create Objects for modules to store multiple values *****/
            $date = (object)[]; $branch = (object)[]; $enrollment = (object)[]; $employee = (object)[]; $branch = (object)[]; $group = (object)[];
            $subscriber = (object)[]; $introducer = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee);
            
            $group->scheme = 20; $group->policy = 1;

            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment);
            
            $employee = Factory::create();
            
            $browser->pick_employee($introducer);            

            $access = $browser->text('#employee_details_tab > p > a:nth-child(2)');

            $browser->provision_useraccess($access, $employee, 'Collection');

            $browser->log_out()->custom_sign_in($employee->username, '123456')->search_for($group->name, $group->name)->visit($enrollment->url);

            /******************** Add Collection Entry - NEFT Mode  ********************/
            $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form_collection_role('1500', $back_date, 'NEFT', '123', 'KVB', 'Chennai', 'Receipt Entry NEFT Test')->press('Save Collection');

            /******************** Back Date Receipt Entry Restriction  ********************/
            $browser->assert_back_date_entry_restricted();

        });
    }
}
