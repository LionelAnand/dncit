<?php

namespace Tests\Browser;

use Exception;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AssertBonusInReceiptEntryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testWhetherBonusIsGeneratedOnPromptDPayment()
    {
        /**************************** Test Name ****************************/
        dump('Assert Bonus In Receipt Entry Test');
        /**************************** Test Name ****************************/
    
        $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment = (object)[]; 
        $dates = (object)[]; $group = (object)[]; $subscriber = (object)[]; $employee = (object)[]; 
        $enrollment_ledger = (object)[]; $receipt1 = (object)[];

        $date = date('d-m-Y h:i:s');

        $dates->auction = date_create($date); date_sub($dates->auction, date_interval_create_from_date_string('1 days')); 
        
        $dates->auction = date_format($dates->auction, "d-m-Y");

        $dates->receipt = date_create($date); $dates->receipt = date_format($dates->receipt, "d-m-Y");

        /***** Get Branch Details, Pick random subscriber, Employee and Agent and get their details *****/            
        $browser->pick_random_branch($branch)->pick_subscriber($subscriber)->pick_employee($employee);            

        $group->scheme = 20; $group->policy = 1; $enrollment->bid_percentage = 30;
        /******************** Create a new group in Policy 1A ********************/
        $browser->create_new_group_in_branch($branch, $group, $date);
        
        $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $employee, $enrollment)
        ->visit(new auctionsPage)->add_first_auction_entry($group->name, $dates->auction)->get_bid_amount_with_percentage($group, $enrollment)
        ->add_auction_entry_for_particular_subscriber($group, $dates->auction, $enrollment->id, $enrollment->bid_amount)->pause(4000)
        ->visit($enrollment->url)->click_toggle_settlement_in_enrollment_ledger();

       $bonus_before = $browser->attribute('#page-container main#main-container #settlements-table > tbody > tr:nth-child(2) > td:nth-child(6)', 'class');
       
       /*** Adding collection for Other Charges ***/
       $browser->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('4000', $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Assert Bonus In Receipt Entry Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->get_details_in_receipt($receipt)->clickLink($receipt->subscriber);

       /*** Adding two collection entries ***/
       $browser->visit($enrollment->url)->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('40000', $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Assert Bonus In Receipt Entry Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved();
      
       $browser->visit($enrollment->url)->click_add_collection()
       ->enter_receipt_details_in_receipt_entry_form('30000', $dates->receipt, 'Cash', '123', 'KVB', 'Chennai', $dates->receipt, '1200', $dates->receipt, '1200' ,'Receipt Entry Cash Test')
       ->save_the_collections()->assert_that_collection_receipt_is_saved()->get_details_in_receipt($receipt1)->visit($enrollment->url);

       $browser->visit($enrollment->url)->click_toggle_settlement_in_enrollment_ledger()->get_details_in_enrollment_ledger($enrollment_ledger);

       /*** Asserting attribute of bonus before and after adding collection entries ***/
       $bonus_after = $browser->attribute('#page-container main#main-container #settlements-table > tbody > tr:nth-child(2) > td:nth-child(6)', 'class');

       if ($bonus_before != $bonus_after && $enrollment_ledger->bonus_amount == '280')
       {
           $browser->delete_group($group);
       }

       else 
       {
           $error = 'Bonus is not calculated';
           throw new Exception($error);
       }

       });
   }
}