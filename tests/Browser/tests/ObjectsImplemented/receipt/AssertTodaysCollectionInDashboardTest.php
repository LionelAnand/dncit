<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\collectionsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class AssertTodaysCollectionInDashboardTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testAssertTodaysCollectionInDashboard()
    {
        /**************************** Test Name ****************************/
        dump('Assert Todays Collection In Dashboard Test');
        /**************************** Test Name ****************************/
    
        $this->browse(function (Browser $browser) {
        $browser->visit(new loginPage)->sign_in();

        /***** Create Objects for modules to store multiple values *****/
        $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment = (object)[];

        $date = $browser->get_todays_date()->date;

        $browser->pick_random_branch($branch);

        $browser->pick_random_enrollment($enrollment);

        /******************** Add Collection Entry - Cash Mode  ********************/
        $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Receipt Entry Cash Test')->save_the_collections()->assert_that_collection_receipt_is_saved();

        /******************** Delete Receipt  ********************/
        $browser->get_details_in_receipt($receipt)->go_to_dashboard()->go_to_todays_collection();

        $browser->assertSee($receipt->number)->clickLink($receipt->number)->delete_the_collection_receipt()->assert_receipt_deleted();

        });
    }
}
