<?php

namespace Tests\Browser;

use Faker\Factory;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BackDateReceiptEntryRestrictionForGroupManagerTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testBackDateReceiptEntryRestrictionForGroupManagerTest()
    {
        /**************************** Test Name ****************************/
        dump('Back Date Receipt Entry Restriction for Group Manager Role Test');
        /**************************** Test Name ****************************/
        
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();
            $back_date = $browser->set_back_date('5')->back_date;

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[]; $enrollment = (object)[]; $employee = (object)[]; $branch = (object)[];

            $browser->pick_employee($employee);

            $employee = Factory::create();

            $access = $browser->text('#employee_details_tab > p > a:nth-child(2)');

            $browser->provision_useraccess($access, $employee, 'Group Manager');

            $browser->log_out()->custom_sign_in($employee->username, '123456');

            $branch = $browser->get_branch_name()->name;

            $browser->click_branch_name_in_branches_page($branch);

            $enrollment = $browser->click_enrollments_in_branch_ledger()->get_enrollment_name();

            $browser->go_to_enrollment_from_enrollments_page($enrollment->name);

            /******************** Add Collection Entry - NEFT Mode  ********************/
            $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $back_date, 'NEFT', '123', 'KVB', 'Chennai', $back_date, '1200', $back_date, '1200' , 'Receipt Entry NEFT Test')->press('Save Collection');

            /******************** Back Date Receipt Entry Restriction  ********************/
            $browser->assert_back_date_entry_restricted();

        });
    }
}
