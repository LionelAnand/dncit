<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\collectionsPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class receiptEntryTransactorNameTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testWhetherTransactorNameisChangedAndAsserted()
    {

        /**************************** Test Name ****************************/
        dump('Receipt Entry Transactor Name Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[]; $transactor = (object)[]; $enrollment = (object)[]; $receipt = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $browser->pick_random_branch($branch)->pick_employee($transactor)->pick_random_enrollment($enrollment);

            $date = $browser->get_todays_date()->date;

        /******************** Add Collection Entry - Cheque Mode  ********************/
         $browser->click_add_collection()->enter_transactor_name($transactor->name)->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cheque', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200', 'Receipt Entry Cheque Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
        /******************** Add Collection Entry - Cheque Mode  ********************/

            $browser->get_details_in_receipt($receipt);

            $browser->go_to_datewise_collection_report('Any Branch', 'Any Branch', $date, $date, $transactor->id, '');

            $browser->assertSee($receipt->number)->clickLink($receipt->number)->delete_the_collection_receipt()->assert_receipt_deleted();
        });
}
}
