<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\branchesPage;
use Tests\Browser\Pages\collectionsPage;
use Tests\Browser\Pages\loginPage;

class revisionHistoryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRevisionHistory()
    {

        /**************************** Test Name ****************************/
        dump('Revision History Test');
        /**************************** Test Name ****************************/

            $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            /***** Create Objects for modules to store multiple values *****/
            $branch = (object)[]; $transactor = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $enrollment_ledger = (object)[]; $assert_log = (object)[];
            /***** Create Objects for modules to store multiple values *****/

            $date = $browser->get_todays_date()->date;

            $browser->pick_random_branch($branch);

            $browser->pick_employee($transactor);

            $browser->pick_random_enrollment($enrollment);

            /******************** Add Collection Entry - Cheque Mode  ********************/
            $browser->click_add_collection()->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cheque', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Revision History Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
            /******************** Add Collection Entry - Cheque Mode  ********************/

            $browser->get_details_in_receipt($receipt);

            $browser->click_change_log_tab_in_collection_receipt()->assert_details_in_change_log($receipt);

            $browser->go_to_collection_details_in_receipts_page()->delete_the_collection_receipt()->assert_receipt_deleted();

        });
    }
}
