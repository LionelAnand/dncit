<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Laravel\Dusk\Page as BasePage;
use Facebook\WebDriver\WebDriverBy;

class obcReceiptEntryTest extends DuskTestCase
{

    public function testobcReceiptEntryTest()
    {
     /**************************** Test Name ****************************/
     dump('Receipt Entry OBC Test');
     /**************************** Test Name ****************************/
 
     $this->browse(function (Browser $browser) {
     $browser->visit(new loginPage)->sign_in();

     /***** Create Objects for modules to store multiple values *****/
     $branch = (object)[]; $enrollment = (object)[]; $receipt = (object)[]; $employee = (object)[];

     $date = $browser->get_todays_date()->date;

     $browser->pick_employee($employee);

     $branch = $browser->get_branch_name()->name;

     $browser->click_branch_name_in_branches_page($branch);

     $enrollment = $browser->click_enrollments_in_branch_ledger()->get_enrollment_name();

     $browser->go_to_enrollment_from_enrollments_page($enrollment->name);

     /******************** Add Collection Entry - Cash Mode  ********************/
     $browser->click_add_collection()->enter_transactor_name($employee->name)->enter_receipt_details_in_receipt_entry_form('1500', $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Receipt Entry OBC Test')->save_the_collections()->assert_that_collection_receipt_is_saved();
     /******************** Add Collection Entry - Cash Mode  ********************/

     /******************** Delete Receipt  ********************/
     $browser->get_details_in_receipt($receipt);

     $this->assertEquals($employee->name, $receipt->collected_by);

     $browser->delete_the_receipt_through_enrollment_ledger($receipt)->assert_receipt_deleted();
     /******************** Delete Receipt  ********************/

        });
    }
}
