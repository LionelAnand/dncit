<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class prizedChitCannotBeLiendTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testprizedChitCannotBeLiend()
    {
        $this->browse(function (Browser $browser) {
            $branch      = (object)[];
            $subscriber  = (object)[];
            $subscriber2 = (object)[];
            $employee    = (object)[];
            $employee2   = (object)[];
            $introducer  = (object)[];
            $introducer2 = (object)[];
            $enrollment  = (object)[];
            $enrollment2 = (object)[];
            $group       = (object)[];
            $enrollment = (object)[];
            $enrollment2 = (object)[];
            $enrollment->bid_amount = 1000;
            $date        = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in()
                    ->pick_subscriber($subscriber)
                    ->pick_subscriber($subscriber2)
                    ->pick_employee($employee)
                    ->pick_employee($employee2)
                    ->pick_employee($introducer)
                    ->pick_employee($introducer2)
                    ->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->enroll_new_subscriber($group, $subscriber2, $employee2, $introducer2, $enrollment2)
                    ->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)
                    ->add_auction_entry_for_particular_subscriber($group,$date, $enrollment->id, $enrollment->bid_amount)
                    //*add auction for one enrollment, and try to add it as guarantee**//
                    ->assert_that_prized_chit_cannot_be_lined($group,$subscriber2,$enrollment)                                  
                    ->search_and_delete_the_group($group);
        });
    }
}
             
       
