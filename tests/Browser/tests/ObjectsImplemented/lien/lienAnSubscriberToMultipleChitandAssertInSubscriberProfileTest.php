<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;

class lienAnSubscriberToMultipleChitandAssertInSubscriberProfileTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testlienAnSubscriberToMultipleChitandAssertInSubscriberProfile()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $subscriber2 = (object)[];
            $subscriber3 = (object)[];
            $employee = (object)[];
            $employee2 = (object)[];
            $employee3 = (object)[];
            $introducer = (object)[];
            $introducer2 = (object)[];
            $introducer3 = (object)[];
            $enrollment = (object)[];
            $enrollment2 = (object)[];
            $enrollment3 = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            $enrollment->bid_amount = 1000;

            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in()
                ->pick_subscriber($subscriber)
                ->pick_subscriber($subscriber2)
                ->pick_subscriber($subscriber3)
                ->pick_employee($employee)
                ->pick_employee($employee2)
                ->pick_employee($employee3)
                ->pick_employee($introducer)
                ->pick_employee($introducer2)
                ->pick_employee($introducer3)
                ->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                ->enroll_new_subscriber($group, $subscriber2, $employee2, $introducer2, $enrollment2)
                ->enroll_new_subscriber($group, $subscriber3, $employee3, $introducer3, $enrollment3);
           $browser
                ->visit(new auctionsPage)
                ->add_first_auction_entry($group->name, $date)
                ->add_guarantee_to_enrollment($group,$subscriber2, $subscriber->mobile) //*subscriber will be the guarantee to subscriber2 & subscriber 3
                ->add_guarantee_to_enrollment($group,$subscriber3, $subscriber->mobile)
                ->assert_that_guarantee_added()
                ->go_to_subscriber_main_ledger($subscriber)
                ->go_to_guarantees_tab_in_subscriber_main_ledger()
                ->assertsee($enrollment2->name)
                ->assertsee($enrollment3->name)
              //  ->assert that guarantees given by this subscriber in its main ledger to subscriber2 and subscriber3
                ->remove_guarantor_from_enrollment($enrollment2)
                ->remove_guarantor_from_enrollment($enrollment3)
              //  ->remove_guarantor_from_enrollment($enrollment3)
                /**to check removal from guarantee */
                ->go_to_subscriber_main_ledger($subscriber)
                ->go_to_guarantees_tab_in_subscriber_main_ledger()
                ->assertDontsee($enrollment2->name)
                ->assertDontsee($enrollment3->name)
                //  ->assert that guarantees  removal in subscriber main ledger
                ->search_and_delete_the_group($group);
        });
    }
}
