<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class lienAnGuarantorAndAssertTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testlienAnGuarantorAndAssert()
    { 
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $guarantor = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            $enrollment->bid_amount = 1000;
            
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in()
                ->pick_subscriber($subscriber)
                ->pick_employee($employee)
                ->pick_employee($introducer)
                ->pick_guarantor($guarantor)
                ->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                ->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)
                ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                ->add_guarantee_to_enrollment($group, $subscriber, $guarantor->mobile)
                ->assert_that_guarantee_added()
                ->remove_guarantor_from_enrollment($enrollment) /**to check removal from guarantee */
                ->search_and_delete_the_group($group);
                });

    }
}