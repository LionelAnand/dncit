<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\groupsPage;
use Tests\DuskTestCase;

class chitLienTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testChitLien()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $subscriber2 = (object)[];
            $employee = (object)[];
            $employee2 = (object)[];
            $introducer = (object)[];
            $introducer2 = (object)[];
            $enrollment = (object)[];
            $enrollment2 = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            $enrollment2 = (object)[];
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in()
                    ->pick_subscriber($subscriber)
                    ->pick_subscriber($subscriber2)
                    ->pick_employee($employee)
                    ->pick_employee($employee2)
                    ->pick_employee($introducer)
                    ->pick_employee($introducer2)
                    ->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->enroll_new_subscriber($group, $subscriber2, $employee2, $introducer2, $enrollment2)
                    ->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)->get_bid_amount_with_percentage($group, $enrollment);
            $browser->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->lien_the_non_prized_chit($enrollment2->name, $group, $subscriber)
                    ->assert_that_guarantee_added()
                             //**add auction entry for lined chit and assert that lined chit cannot win auction */
                    ->assert_that_lined_enrollment_cannot_win_auction($group, $enrollment2, $date)
                    ->remove_guarantor_from_enrollment($enrollment) /**to check removal from guarantee */
                    ->search_and_delete_the_group($group);
        });
    }
}
