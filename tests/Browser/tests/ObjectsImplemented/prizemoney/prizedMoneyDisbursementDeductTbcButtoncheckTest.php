<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class prizedMoneyDisbursementDeductTbcButtoncheckTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testprizedMoneyDisbursementDeductTbcButtoncheck()
    {
        $this->browse(function (Browser $browser) {
            dump('testprizedMoneyDisbursementDeductTbcButtoncheck');

            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 2;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber)
                    ->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->visit(new prizedMoneyPage)
                    ->get_prize_money_details($group, $enrollment)
                    ->get_enrollment_details($enrollment)
                    ->get_deductable_tbc_after_auction($enrollment);
            $enrollment->tbc = $browser->format_amount($enrollment->tbc)->format_amount; // 2,744 as 2744
            $enrollment->tbc = substr($enrollment->tbc, 0, -5);
            $this->assertEquals($enrollment->upm, $enrollment->disbursable);
            $enrollment->deduction = 1;  //it will check The Deductable TBC button
            $enrollment->disbursement = $enrollment->upm;
            $enrollment->deductable_tbc = $enrollment->deductable_tbc;
            $this->assertEquals( $enrollment->deductable_tbc,$enrollment->tbc);//Deductable TBC from prize money
            $browser->disburse_prize_money($enrollment);
           
        });
    }
}
