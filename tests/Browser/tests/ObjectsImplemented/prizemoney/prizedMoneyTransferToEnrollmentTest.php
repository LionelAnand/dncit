<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\prizedMoneyPage;

class prizedMoneyTransferToEnrollmentTest extends DuskTestCase
{
    public function testprizedMoneyTransferToEnrollment()
    {
        dump('testprizedMoneyTransferToEnrollment');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 3;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()->pick_random_branch($branch)->pick_subscriber($subscriber)
                    ->pick_employee($employee)->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
            $browser->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)
                    ->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->get_prize_money_details($group, $enrollment)
                    ->prize_money_adjustment_without_deduction($enrollment, '10000', $date)
                    ->assert_prize_money_disbursed('10,000');
            

        });
    }
}
