<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\collectionsPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class AssertPrizeMoneyAdjustmentInAdjustmentFilterCollectionReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testPrizeMoneyAdjustmentEntryInAdjustmentFilterCollectionReport()
    {
        dump('PrizeMoneyAdjustmentEntryInAdjustmentFilterCollectionReportTest');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 3;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()->pick_random_branch($branch)->pick_subscriber($subscriber)
                    ->pick_employee($employee)->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
            $browser->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)
                    ->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->get_prize_money_details($group, $enrollment)
                    ->prize_money_adjustment_without_deduction($enrollment, '10000', $date)
                    ->assert_prize_money_disbursed('10,000')
                    ->go_to_enrollment_with_id($enrollment)
                    ->clickLink('Prize Money');

            $prize_money_adjustment_receipt = $browser->text('#prize_money_tab > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(7) > small');

            // Go to the adjusted subscriber and assert the charges added
            $browser->visit(new collectionsPage)
                    ->go_to_enrollment_with_id($enrollment)
                    ->go_to_other_charges_tab_in_enrollment_ledger()
                    ->assertSee($prize_money_adjustment_receipt)
                    ->clickLink($prize_money_adjustment_receipt);
            $receipt_number = $browser->text('Receipt_Number');
            $receipt_amount = $browser->text('Receipt Amount');
            $browser->pause(1000)
                    ->go_to_datewise_collection_report($branch->id, 'Any Branch', $date, $date, '', 'Adjustment')
                    ->assert_information_in_date_wise_collection_report()
                    ->assertSee($subscriber->name, $group->name, $receipt_number, $receipt_amount, 'PSW', 'KKN')
                    ->clickLink($receipt_number);
            $window = collect($browser->driver->getWindowHandles())->last();
            $browser->driver->switchTo()->window($window);
            $browser->delete_the_collection_receipt()
                    ->assert_receipt_deleted();
        });
    }
}
