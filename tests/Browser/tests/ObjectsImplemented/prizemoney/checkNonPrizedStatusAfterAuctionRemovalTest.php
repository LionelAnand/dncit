<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\prizedMoneyPage;


class checkNonPrizedStatusAfterAuctionRemovalTest extends DuskTestCase
{

    public function testCheckNonPrizedStatusAfterAuctionRemoval()
    {
        $this->browse(function (Browser $browser) {
            dump('testCheckNonPrizedStatusAfterAuctionRemoval');
            $subscriber = (object)[]; $employee = (object)[]; $introducer = (object)[]; $enrollment = (object)[];
            $enrollment1 = (object)[]; $branch = (object)[]; $group = (object)[];
            $enrollment->bid_amount = 2000;

            $date = date('d-m-Y');
            $browser->visit(new prizedMoneyPage)->sign_in()
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->pick_random_branch($branch);

            $group->scheme = 1; $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)->visit($group->url);
            $browser->go_to_auctions_tab_in_group_ledger()
                    ->add_first_auction_entry($group->name, $date);
            $browser->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount);
            $browser->go_to_enrollment_with_id($enrollment)
                ->assertSeeIn('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(1)','Successful Bidder')
                ->clicklink($enrollment->name);
            $browser->go_to_auctions_tab_in_group_ledger()
                    ->remove_auction_for_enrollment($group, $enrollment)->pause(5000);
            $browser->go_to_enrollment_with_id($enrollment)
                    ->visit($enrollment->url)
                    ->get_enrollment_details($enrollment1);
            $this->assertEquals($enrollment1->prized_status, 'Non Prized');
        
        });
    }
}
