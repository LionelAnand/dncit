<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class assertGstPercentageOnPrizedMoneyDisbursementTest extends DuskTestCase
{
    public function testAssertGstPercentageOnPrizedMoneyDisbursement()
    {
        $this->browse(function (Browser $browser) {
            dump('testAssertGstPercentageOnPrizedMoneyDisbursement');
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber)
                    ->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)->pause(2000)
                    ->calculate_gst($enrollment);
            $this->assertEquals($enrollment->calculated_gst, $enrollment->displayed_gst);
                   
        });
    }
}
