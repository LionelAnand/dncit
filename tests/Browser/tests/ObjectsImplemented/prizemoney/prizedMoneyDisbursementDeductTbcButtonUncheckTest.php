<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\guarantorsPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;
use Exception;

class prizedMoneyDisbursementDeductTbcButtonUncheckTest extends DuskTestCase
{
    public function testprizedMoneyDisbursementDeductTbcButtonUncheck()
    {
                $this->browse(function (Browser $browser) {
            dump('testprizedMoneyDisbursementDeductTbcButtonUncheck');
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $branch = (object)[];
            $group = (object)[];
            $receipts =(object)[];
            $enrollment_ledger = (object)[];
            $enrollment_ledger1 = (object)[];
            $prized_money_details = (object)[]; 
            $enrollment->bid_amount = 2000;
            $date = date('d-m-Y');
            $browser->visit(new prizedMoneyPage)->sign_in()
                    ->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->pick_random_branch($branch);

            $group->scheme = 1;
            $group->policy = 3;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment);
            $browser->go_to_auctions_tab_of_group($group)
                    ->add_first_auction_entry($group->name, $date);
            $browser->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)->pause(5000)->clicklink($subscriber->name)->get_details_in_enrollment_ledger($enrollment_ledger);
            $browser->assertseeIn('#settlements-table > tbody > tr.table-info > td.text-right.bg-warning',$enrollment_ledger->to_be_collected_amount)
                   ->get_prize_money_details($group, $enrollment);
                $this->assertEquals($enrollment->upm, $enrollment->disbursable);
                     $undisbursed_before = $enrollment->upm;
                     $enrollment->disbursement = $enrollment->upm;
            $browser->disburse_prize_money_without_deduction($enrollment);
            $browser->go_to_other_charges_tab_in_enrollment_ledger()->delete_other_charges_receipts($receipts);
            $browser->visit($enrollment->url)->get_details_in_enrollment_ledger($enrollment_ledger1);
            $this->assertEquals($enrollment_ledger->to_be_collected_amount, $enrollment_ledger1->to_be_collected_amount);

        });
    }
}
