<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\prizedMoneyPage;

class checkSuccessfulBidderStatusAfterAuctionEntryTest extends DuskTestCase
{
    public function testCheckSuccessfulBidderdStatusAfterAuctionEntry()
    {
        $this->browse(function (Browser $browser) {
            dump('testCheckSuccessfulBidderdStatusAfterAuctionEntry');
        $branch = (object)[];
        $subscriber = (object)[];
        $employee = (object)[];
        $introducer = (object)[];
        $enrollment = (object)[];
        $group = (object)[];
        $date = date('d-m-Y');
        $group->scheme = 1;
        $group->policy = 3;
        $enrollment->bid_percentage = 30;
        $browser->visit(new prizedMoneyPage)->sign_in()
                ->pick_random_branch($branch)->pick_subscriber($subscriber)
                ->visit(new employeesPage)->pick_employee($employee)->pick_employee($introducer)                    
                ->add_subscriber_kyc_documents($subscriber)               
                ->create_new_group_in_branch($branch, $group, $date)
                ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)               
                ->add_first_auction_entry($group->name, $date)->get_bid_amount_with_percentage($group, $enrollment)
                ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                ->pause(4000)
                ->go_to_enrollment_with_id($enrollment)
                ->assertSeeIn('#main-container > div.bg-primary-dark > div > div > div.col-10 > h3 > div > div:nth-child(1)','Successful Bidder');
        });
    }
}
