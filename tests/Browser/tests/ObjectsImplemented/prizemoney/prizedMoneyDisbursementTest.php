<?php
namespace Tests\Browser;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\guarantorsPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;
class prizedMoneyDisbursementTest extends DuskTestCase
{
    public function testPrizedMoneyDisbrsement()
    {
        $this->browse(function (Browser $browser) {
            dump('prizedMoneyDisbursementTest');
            $branch = (object)[];
            $subscriber = (object)[];
            $subscriber2 = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $guarantor = (object)[];
            $guarantor2 = (object)[];
            $guarantor3 = (object)[];
            $enrollment = (object)[];
            $enrollment2 = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_subscriber($subscriber)
                    ->pick_subscriber($subscriber2)
                    ->visit(new employeesPage)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->pick_guarantor($guarantor)
                    ->pick_guarantor($guarantor2)
                    ->pick_guarantor($guarantor3)
                    ->add_subscriber_kyc_documents($subscriber)
                    ->add_subscriber_kyc_documents($subscriber2)
                    ->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->enroll_new_subscriber($group, $subscriber2, $employee, $introducer, $enrollment2)
                    ->add_first_auction_entry($group->name, $date)
                    ->log_out()
                    ->sign_in_as_group_manager()
                    ->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->pause(4000)
                    ->visit(new prizedMoneyPage)
                    ->confirm_disburse_prize_money_button_is_disabled($enrollment)
                    ->upload_documents_necessary_for_prized_money_disbursement($enrollment)
                    ->visit(new guarantorsPage)
                    ->add_guarantor_to_enrollment($enrollment, $guarantor)
                    ->add_guarantor_to_enrollment($enrollment, $guarantor2)
                    ->add_guarantor_to_enrollment($enrollment, $guarantor3)
                    ->upload_guarantor_documents_for_prized_money_disbursement($guarantor, $enrollment)
                    ->upload_guarantor_documents_for_prized_money_disbursement($guarantor2, $enrollment)
                    ->upload_guarantor_documents_for_prized_money_disbursement($guarantor3, $enrollment)
                    ->visit(new prizedMoneyPage)
                    ->get_prize_money_details($group, $enrollment);
                    dump($enrollment->upm);dump($enrollment->disbursable);
            $this->assertEquals($enrollment->upm, $enrollment->disbursable);
            $undisbursed_before = $enrollment->upm;
            $enrollment->disbursement = $enrollment->upm;
            $browser->disburse_prize_money($enrollment)
                    ->get_upm($enrollment)
                    ->log_out()
                    ->sign_in();
            $undisbursed_after = $enrollment->upm;
            $final_upm = $undisbursed_before - $enrollment->disbursement;
            $this->assertEquals($final_upm, $undisbursed_after);
            $browser->remove_disbursed_prize_money($enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}