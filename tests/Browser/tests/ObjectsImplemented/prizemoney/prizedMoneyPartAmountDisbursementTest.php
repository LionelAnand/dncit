<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\DuskTestCase;

class prizedMoneyPartAmountDisbursementTest extends DuskTestCase
{
    public function testPrizedMoneyPartAmountDisbursement()
    {
        $this->browse(function (Browser $browser) {
            dump('testPrizedMoneyPartAmountDisbursement');
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];

            $date = date('d-m-Y');
            $group->scheme = 3;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()->pick_random_branch($branch)->pick_subscriber($subscriber)
                    ->pick_employee($employee)->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
            $browser->create_new_group_in_branch($branch, $group, $date)
                    ->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)
                    ->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->get_prize_money_details($group, $enrollment);
            $this->assertEquals($enrollment->upm, $enrollment->disbursable);
           
            $enrollment->disbursement = $enrollment->upm * 30 / 100; //part amount disbursmenet, here 30%
            $browser->disburse_prize_money_without_deduction($enrollment)
                    ->get_upm($enrollment);
           

            $disbursed = $browser->text('#prize_money_tab > div:nth-child(2) > div > table > tbody > tr.table-info > td.text-right');
            $disbursed = str_replace(',', '', $enrollment->disbursement) * 1;
            $this->assertEquals($disbursed, $enrollment->disbursement);
           
        });
    }
}
