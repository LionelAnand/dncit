<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class dropChitReportExportAllBranchTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testdropChitReportExportAllBranch()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y"); $branch = (object)[]; 

            $browser->visit(new loginPage)->sign_in()->visit(new reportsPage);

            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch);
            $browser->go_to_removed_enrollments_report($branch, '1')
                ->assert_the_details_in_removed_enrollments_report()->export_the_report();
        });
    }
}
