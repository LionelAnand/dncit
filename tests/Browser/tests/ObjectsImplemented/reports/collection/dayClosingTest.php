<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
class DayClosingTest extends DuskTestCase
{

  public function testExportDayClosingReport()
  {

   $this->browse(function (Browser $browser) {
   
   $date = date("d-m-Y"); $branch = (object)[];

     $browser->visit(new loginPage)->sign_in()->pick_random_branch($branch)->go_to_datewise_collection_report($branch->id,'',$date,$date, '', 'Cash')->assert_the_details_in_datewise_collection_report()->export_the_report();
   });
 }
}

