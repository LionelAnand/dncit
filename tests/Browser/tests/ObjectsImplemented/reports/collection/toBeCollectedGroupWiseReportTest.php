<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\Traits\common;
class toBeCollectedGroupWiseReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testtoBeCollectedGroupWiseReport()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $browser->visit(new loginPage)->sign_in()->pick_random_branch($branch)->go_to_group_wise_tbc_report($branch->id)->assert_the_details_in_group_wise_tbc_report();
        });
    }
}
