<?php

namespace Tests\Browser;

use Exception;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Facebook\WebDriver\WebDriverBy;

class SubscriberWiseTBCReportMinimumPaymentDaysTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSubscriberWiseTBCReportMinimumPaymentDays()
    {
        /**************************** Test Name ****************************/
        dump('Subscriber Wise TBC Report Minimum Payment Days Test');
        /**************************** Test Name ****************************/

        $this->browse(function (Browser $browser) {

            $branch = (object)[]; $employee = (object)[];

            $browser->visit(new loginPage)->sign_in()->pick_random_branch($branch)->pick_employee($employee)
            ->go_to_subscriber_wise_tbc_report($branch->id, '1', '30', '', '');

            $minimum_payment_days = array($browser->driver->findElements(WebDriverBy::xpath('//tr[*]//td[11]//small[1]')));
              
              if ($minimum_payment_days >= 30) 
              {              
              $browser->export_the_report();              
              }              
              else 
              {              
              $error = 'Payment days is greater than specified in the parameter of the report.';
              throw new Exception($error);             
              }

        });
    }
}
