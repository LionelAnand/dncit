<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\Traits\common;


class tbcReportAbove60DaysTest extends DuskTestCase
{
    /**
     * TBC Report Above 60 Days Test
     *
     * @return void
     */
    public function testtbcReportAbove60Days()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
        $browser->visit(new loginPage)->sign_in()->pick_random_branch($branch)->go_to_subscriber_wise_tbc_report($branch->id,'All Employees','60','0','Any')->assert_the_details_in_subscriber_wise_tbc_report()->export_the_report();
        });
    }
}
