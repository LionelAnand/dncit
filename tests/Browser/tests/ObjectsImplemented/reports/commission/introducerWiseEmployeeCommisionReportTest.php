<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\comissionsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\groupsPage;



class introducerWiseEmployeeCommisionReportTest extends DuskTestCase
{
  
 public function testIntroducerWiseEmployeeCommisionReport(){

        dump('Introducer Wise Employee Commission Report');
     
        $this->browse(function (Browser $browser) {
            $browser->visit(new loginPage)->sign_in();

            $group = (object)[]; $branch = (object)[]; $subscriber = (object)[]; 
            $employee = (object)[]; $introducer = (object)[]; $commission = (object)[]; $enrollment = (object)[];

            $date = $browser->get_todays_date()->date;

            $browser->pick_random_branch($branch);

            $commission->percentage = $browser->go_to_policy_page()->commission_percentage('employee', '3','D1', '7')->commission_percentage;

           $browser->pick_subscriber($subscriber)->pick_employee($employee)->pick_employee($introducer);

            $group->scheme = 19; $group->policy = 9;

            $browser->create_new_group_in_branch($branch, $group, $date);
            
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment);

            $group->chit_value = $browser->format_amount($group->chit_value)->format_amount;

            $browser->visit(new comissionsPage)->visit($group->url);

           $browser->go_to_enrollment_with_id($enrollment)->go_to_introducer_commission_tab_in_enrollment_ledger()->assert_commission_disbursement_details($employee->name, $group->chit_value);

            $browser->get_details_from_introducer_commission_tab_in_enrollment_ledger($commission);

            $commission->chit_value = $browser->format_amount($commission->chit_value)->format_amount;

            $commission->commission = $browser->format_amount($commission->commission)->format_amount;

            $commission->tds = $browser->format_amount($commission->tds)->format_amount;

            $commission->disbursable = $browser->format_amount($commission->disbursable)->format_amount;

            $commission->undisbursed = $browser->format_amount($commission->undisbursed)->format_amount;

            $this->assertEquals($group->chit_value, $commission->chit_value);

            $employee_commission = $commission->chit_value * $commission->percentage / 100;
            $this->assertEquals($employee_commission, '750');

            if ($introducer->pan == '')
            {
                $browser->assert_commission_details_with_pan_card($commission->tds = '150', $commission->disbursable_commission = '600', $commission->undisbursed_commission = '600');
                $amount = '600';
            }

            else 
            {
                $browser->assert_commission_details_without_pan_card($commission->tds = '37.5', $commission->disbursable_commission = '712.5', $commission->undisbursed_commission = '712.5');
                $amount = '712';
            }

            $browser->click_disburse_introducer_commission_in_introducer_commission_tab()
            ->enter_introducer_commission_disbursement_details($amount, $date, 'Cash', '123', 'KVB', 'Chennai', $date, '1200', $date, '1200' ,'Employee Commission Disbursement and Assertions Test')->press('Save')->assert_that_introducer_commission_has_been_disbursed()->go_to_introducer_commission_tab_in_enrollment_ledger();
            
        if ($introducer->pan == '')
            {
                $browser->assert_commission_details_after_disbursement_with_pan_card($commission->disbursable_commission = '600', $commission->undisbursed_commission = '0');
            }

            else 
            {
                $browser->assert_commission_details_after_disbursement_without_pan_card($commission->disbursable_commission = '712.5', $commission->undisbursed_commission = '0.5');
            }
         
            $browser->generate_introducer_wise_introducer_commission_report($branch->name, $date, $date)
            ->assertSee($subscriber->name)->clickLink($subscriber->name)->clickLink($group->name)
                    ->go_to_enrollment_with_id($enrollment)
                    ->go_to_introducer_commission_tab_in_enrollment_ledger()
                    ->remove_introducer_commission_disbursements()->assert_commission_deleted();

        });
}
}
