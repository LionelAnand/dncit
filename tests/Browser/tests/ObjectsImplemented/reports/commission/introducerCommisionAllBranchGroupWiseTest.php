<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\comissionsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\loginPage;

class introducerCommisionAllBranchGroupWiseTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testIntroducerCommisionAllBranchGroupWise()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y");
            $branch=(object)[];

            $browser->visit(new loginPage)->sign_in();

            /***** Get Branch Details *****/

            $browser->pick_random_branch($branch);

                $browser->click_introducer_commission_in_menu()
                ->click_group_wise_from_introducer_commission_in_menu()->choose_branch_in_group_wise_introducer_commission_report($branch)
                ->click_generate_in_reports()->export_the_report();
              
        });
    }
}
