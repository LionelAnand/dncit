<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
class generateBranchWiseIntroducerCommisionReportTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testgenerateBranchWiseIntroducerCommisionReportTest()
    {
        $this->browse(function (Browser $browser) {
            dump('generateBranchWiseIntroducerCommisionReportTest');
            $date = date("d-m-Y");
            $browser->visit(new loginPage)->sign_in()->click_introducer_commission_in_menu()
                    ->click_branch_wise_from_introducer_commission_in_menu()->export_the_report();
        }); 
    }
    
}
