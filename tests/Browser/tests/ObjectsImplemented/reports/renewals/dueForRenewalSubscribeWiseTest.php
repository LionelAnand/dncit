<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class dueForRenewalSubscribeWiseTest extends DuskTestCase
{
    /**
     * Due For Renewal Subscribe Wise Test
     *
     * @return void
     */
    public function testDueForRenewalSubscribeWise()
    {
        $this->browse(function (Browser $browser) {
            $date = date("d-m-Y"); $branch = (object)[];
            

            $browser->visit(new loginPage)->sign_in()->visit(new reportsPage);
            /***** Get Branch Details *****/            
            $browser->pick_random_branch($branch)->go_to_due_for_renewal_report($branch, '1', '1');
        });
    }
}

