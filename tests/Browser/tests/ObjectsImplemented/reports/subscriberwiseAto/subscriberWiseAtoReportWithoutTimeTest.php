<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class subscriberWiseAtoReportWithoutTimeTest extends DuskTestCase
{
    public function testsubscriberWiseAtoBusinessReportWithoutTime()
    {
        dump('subscriberWiseAtoBusinessReportWithoutTimeTest');
        $this->browse(function (Browser $browser) {
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = '';
            $date->to_date = '';
            $browser->pick_random_branch($branch);
            $group->status = 'Closed';
            $browser->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch, $group, $date);
        });
    }
}
