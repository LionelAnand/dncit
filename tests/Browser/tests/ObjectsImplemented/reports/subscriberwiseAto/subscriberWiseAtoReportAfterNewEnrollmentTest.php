<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class subscriberWiseAtoReportAfterNewEnrollmentTest extends DuskTestCase
{
    public function testsubscriberWiseAtoBusinessReportAfterNewEnrollmentTest()
    {
        dump('testsubscriberWiseAtoBusinessReportAfterNewEnrollmentTest');
        $this->browse(function (Browser $browser) {
            $dates = (object)[];
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in();
            $browser->pick_subscriber($subscriber)
                    ->pick_employee($employee)
                    ->pick_employee($introducer)
                    ->pick_random_branch($branch);

            $group->scheme = 1;
            $group->policy = 3;
            $group->status = 'All';
            $dates->from_date = $browser->set_back_date(60)->back_date;
            $dates->to_date = date('d-m-Y');
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer,$enrollment)
                    ->visit(new reportsPage)->generate_subscriber_wise_ATO_report($branch, $group, $dates)->assertSee($group->name)
                    ->visit(new groupsPage)->search_and_delete_the_group($group);
        });
    }
}
