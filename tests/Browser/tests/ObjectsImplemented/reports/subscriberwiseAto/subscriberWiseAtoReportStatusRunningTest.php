<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\reportsPage;

class subscriberWiseAtoReportStatusRunningTest extends DuskTestCase
{
    public function testsubscriberWiseAtoBusinessReportStatusRunning()
    {            dump('subscriberWiseAtoBusinessReportStatusRunningTest');

        $this->browse(function (Browser $browser) {

            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(60)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $branch->id = 'All';
            $group->status = 'Running';
            $browser->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch, $group, $date);
        });
    }
}
