<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class groupWiseATOReportStatusNewTest extends DuskTestCase
{
    public function testgroupWiseATOReportStatusNewTest()
    {
        dump('testgroupWiseATOReportStatusNewTest');
        $this->browse(function (Browser $browser) {
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(200)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $group->status = 'New';
            $browser->visit(new reportsPage)->generate_group_wise_ATO_report($branch, $group, $date);
        });
    }
}
