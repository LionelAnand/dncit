<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\reportsPage;

class groupWiseATOReportAllBranchesTest extends DuskTestCase
{
    public function testgroupWiseATOReportAllBranches()
    {
        $this->browse(function (Browser $browser) {
            dump('testgroupWiseATOReportAllBranches');
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(2000)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $branch->id = 'All';
            $group->status = 'Closed';
            $browser->visit(new reportsPage)->generate_group_wise_ATO_report($branch, $group, $date);
        });
    }
}
