<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class groupWiseATOReportWithoutDateTest extends DuskTestCase
{
    public function testgroupWiseATOReportWithoutDate()
    {
        dump('testgroupWiseATOReportWithoutDate');
        $this->browse(function (Browser $browser) {
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = '';
            $date->to_date = '';
            $browser->pick_random_branch($branch);
            $group->status = 'Closed';
            $browser->visit(new reportsPage)->generate_group_wise_ATO_report($branch, $group, $date);
        });
    }
}
