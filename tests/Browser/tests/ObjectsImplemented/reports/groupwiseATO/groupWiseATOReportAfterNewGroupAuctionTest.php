<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class groupWiseATOReportAfterNewGroupAuctionTest extends DuskTestCase
{
    public function testgroupWiseATOReportAfterNewGroupAuction()
    {
        $this->browse(function (Browser $browser) {
            dump('testgroupWiseATOReportAfterNewGroupAuction');

            $dates = (object)[];
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $subscriber2 = (object)[];
            $employee2 = (object)[];
            $introducer2 = (object)[];
            $group = (object)[];
            $enrollment = (object)[];
            
            $date = date('d-m-Y');
            $browser->visit(new employeesPage)->sign_in()
                    ->pick_subscriber($subscriber)
                    ->pick_subscriber($subscriber2)
                    ->pick_employee($employee)
                    ->pick_employee($employee2)
                    ->pick_employee($introducer)
                    ->pick_employee($introducer2)
                    ->pick_random_branch($branch);
            $group->scheme = 1;
            $group->policy = 3;
            $group->status = 'All';
            $dates->from_date = date('d-m-Y');
            $dates->to_date = date('d-m-Y');
            $browser->create_new_group_in_branch($branch, $group, $date);

            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer,$enrollment)->enroll_new_subscriber($group, $subscriber2, $employee2, $introducer2,$enrollment)
                    ->visit(new auctionsPage)->add_first_auction_entry($group->name, $date)
                    ->visit(new reportsPage)->generate_group_wise_ATO_report($branch, $group, $dates)
                    ->visit(new groupsPage)->search_and_delete_the_group($group);
        });
    }
}
