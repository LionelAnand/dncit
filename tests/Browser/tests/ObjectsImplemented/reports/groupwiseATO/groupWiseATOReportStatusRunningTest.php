<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class groupWiseATOReportStatusRunningTest extends DuskTestCase
{
    public function testgroupWiseATOReportStatusRunning()
    {
        dump('testgroupWiseATOReportStatusRunning');
        $this->browse(function (Browser $browser) {
   
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(100)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $branch->id = 'All';
            $group->status = 'Running';
            $browser->visit(new reportsPage)->generate_group_wise_ATO_report($branch, $group, $date);
        });
    }
}
