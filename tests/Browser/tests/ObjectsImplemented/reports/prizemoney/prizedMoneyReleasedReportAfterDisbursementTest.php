<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\prizedMoneyPage;
use Tests\Browser\Pages\reportsPage;

class prizedMoneyReleasedReportAfterDisbursementTest extends DuskTestCase
{
    public function testPrizedMoneyReleasedReportAfterDisbursement()
    {
       $this->browse(function (Browser $browser) {
        dump('Check Prize Money Disbursement in Disbursed Prize Money Report');
            $branch = (object)[];
            $subscriber = (object)[];            
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];

            $date = date('d-m-Y');
            $group->scheme = 3;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new prizedMoneyPage)
                    ->sign_in()->pick_random_branch($branch)->pick_subscriber($subscriber)
                    ->pick_employee($employee)->pick_employee($introducer)
                    ->add_subscriber_kyc_documents($subscriber);
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
                    ->add_first_auction_entry($group->name, $date)
                    ->get_bid_amount_with_percentage($group, $enrollment)
                    ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
                    ->confirm_disburse_prize_money_button_enabled($enrollment)
                    ->get_prize_money_details($group, $enrollment);
            $this->assertEquals($enrollment->upm, $enrollment->disbursable);
            $undisbursed_upm = $enrollment->upm;            
            $enrollment->disbursement = $enrollment->upm;
            $browser->disburse_prize_money($enrollment)
                    ->get_upm($enrollment);                    
            $disbursed_pm = $enrollment->upm;
            $final_upm = $undisbursed_upm - $enrollment->disbursement;
            $this->assertEquals($final_upm, $disbursed_pm);
            $browser->generate_disbursed_prize_money_report($branch->id, $date, $date);
            $browser->remove_disbursed_prize_money($enrollment)
                    ->assert_and_remove_other_charges_one($enrollment)
                    ->search_and_delete_the_group($group);
        });
    }
}
