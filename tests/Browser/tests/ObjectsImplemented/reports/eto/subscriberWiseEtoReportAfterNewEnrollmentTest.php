<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\employeesPage;
use Tests\Browser\Pages\groupsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class subscriberWiseEtoReportAfterNewEnrollmentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testsubscriberWiseETOBusinessReportAfterNewEnrollmentTest()
    {
        dump('testsubscriberWiseETOBusinessReportAfterNewEnrollmentTest');
        $this->browse(function (Browser $browser) {
            $date = date('d-m-Y');
            $branch = (object)[];
            $branch2 = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $dates = (object)[];
            $enrollment = (object)[];

             $browser->visit(new employeesPage)->sign_in()
                    ->pick_random_branch($branch)
                    ->pick_random_branch($branch2)
                    ->pick_subscriber($subscriber)
                    ->pick_employee_from($branch2, $introducer)
                    ->pick_employee( $employee);

            $group->scheme = 1;
            $group->policy = 3;
            $group->status = 'All';
            $dates->from_date = $date;
            $dates->to_date = $date;
            $browser->create_new_group_in_branch($branch, $group, $date);
            $browser->visit(new groupsPage)->enroll_new_subscriber($group, $subscriber, $employee, $introducer,$enrollment)
                    ->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch2, $group, $dates)->assertSee($group->name)
                    ->visit(new groupsPage)->search_and_delete_the_group($group);
        });
    }
}
