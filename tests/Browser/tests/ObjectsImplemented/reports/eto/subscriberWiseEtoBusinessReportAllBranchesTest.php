<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;

class subscriberWiseEtoBusinessReportAllBranchesTest extends DuskTestCase
{
    public function testsubscriberWiseEtoBusinessReportAllBranches()
    {            dump('testsubscriberWiseEtoBusinessReportAllBranches');

        $this->browse(function (Browser $browser) {
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(30)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $group->status = 'All';
            $browser->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch, $group, $date);
        });
    }
}
