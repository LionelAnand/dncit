<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class subscriberWiseEtoBusinessReportStatusNewTest extends DuskTestCase
{
    public function testsubscriberWiseEtoBusinessReportStatusNew()
    {
        dump('testsubscriberWiseEtoBusinessReportStatusNew');
        $this->browse(function (Browser $browser) {
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(60)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $branch->id = 'All';
            $group->status = 'New';
            $browser->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch, $group, $date);
        });
    }
}
