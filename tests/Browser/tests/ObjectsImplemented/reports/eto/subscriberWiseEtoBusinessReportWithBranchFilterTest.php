<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\groupsPage;

class subscriberWiseEtoBusinessReportWithBranchFilterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testSubscriberWiseEtoBusinessReportWithBranchFilter()
    {
        $this->browse(function (Browser $browser) {
            dump('testSubscriberWiseEtoBusinessReportWithBranchFilter');
            $date = (object)[]; $branch = (object)[];   $group = (object)[];
            $browser->visit(new reportsPage)->sign_in();
            $date->from_date = $browser->set_back_date(60)->back_date;
            $date->to_date = date('d-m-Y');
            $browser->pick_random_branch($branch);
            $group->status = 'All';
            $browser->visit(new reportsPage)->generate_subscriber_wise_ETO_report($branch, $group, $date);
        });
    }
}
