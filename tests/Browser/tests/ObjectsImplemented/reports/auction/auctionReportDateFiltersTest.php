<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\reportsPage;
use Tests\Browser\Pages\auctionsPage;

class auctionReportDateFiltersTest extends DuskTestCase
{
    /**
     * All Branch Wise Auction Report Without Date
     *
     * @return void
     */
    public function testAuctionReportDateFilters()
    {
        $this->browse(function (Browser $browser) {

        $date = date("d-m-Y");     

            $browser->visit(new loginPage)->sign_in()
            ->go_to_datewise_auction_report('All Branches', '', 'All Auctions')
            ->assert_the_details_in_datewise_auction_report()->export_the_report();
        });
    }
}
