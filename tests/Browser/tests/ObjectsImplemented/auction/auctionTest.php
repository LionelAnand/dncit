<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\loginPage;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\employeesPage;

class auctionTest extends DuskTestCase
{
    public function testAuction()
    {
        dump('Auction Entry Test');
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $subscriber = (object)[];
            $employee = (object)[];
            $introducer = (object)[];
            $enrollment = (object)[];
            $group = (object)[];
            $date = date('d-m-Y');
            $group->scheme = 1;
            $group->policy = 3;
            $enrollment->bid_percentage = 30;
            $browser->visit(new employeesPage)
            ->sign_in()
            ->pick_random_branch($branch)
            ->pick_subscriber($subscriber)
            ->pick_employee($employee)
            ->pick_employee($introducer)
            ->add_subscriber_kyc_documents($subscriber);
    $browser->create_new_group_in_branch($branch, $group, $date);

    $browser->enroll_new_subscriber($group, $subscriber, $employee, $introducer, $enrollment)
            ->add_first_auction_entry($group->name, $date)
            ->get_bid_amount_with_percentage($group, $enrollment)
            ->add_auction_entry_for_particular_subscriber($group, $date, $enrollment->id, $enrollment->bid_amount)
            ->remove_the_last_auction($group)
            ->delete_group($group);
        });
    }
}
