<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\Browser\Pages\auctionsPage;
use Tests\Browser\Pages\reportsPage;
use Tests\DuskTestCase;

class prizedAuctionDeleteTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testprizedAuctionDelete()
    {
        $this->browse(function (Browser $browser) {
            $branch = (object)[];
            $dates = (object)[];
            $enrollment = (object)[];
            $group = (object)[];

            $browser->visit(new auctionsPage)
                    ->sign_in();

            $date = date('d-m-Y');
            $dates->from_date = $browser->set_back_date(100)->back_date;
            $dates->to_date = $date;
            $browser->visit(new reportsPage)
                    ->pick_a_prized_enrollment($dates, $enrollment);
                    $group->id = $enrollment->group_id;
            $browser->get_group_details_with_id($group)
                    ->remove_auction_for_enrollment($group, $enrollment)
                    ->assertsee('Auction cannot be deleted when prize money has been disbursed.');
        });
    }
}
