#!/bin/bash

curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer

apt-get install php php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mysql php7.2-xml php-token-stream php7.2-json php7.2-curl php7.2-dom php7.2-simplexml php7.2-xsl php7.2-intl php7.2-mbstring php7.2-ctype php7.2-zip php7.2-xmlwriter php7.2-gd php7.2-iconv curl
apt-get install git
which git
echo "Enter your Username"
read USERNAME
git config --global user.name "$USERNAME"
echo "Enter your email"
read email

git config --global user.email "$email"
git clone https://$USERNAME@bitbucket.org/LionelAnand/dncit.git
chmod a+rwx dncit
cd dncit
cp environment.php .env
composer install
composer update
php artisan dusk:install

rm tests/Browser/ExampleTest.php
rm tests/Browser/Pages/HomePage.php
cd ..
cd ..
cp Downloads/dncit Desktop/dncit
rm Downloads/dncit
echo "Setup complete!, you're ready to go"