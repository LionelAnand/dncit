<?php
/*namespace App;*/

require __DIR__ . '/vendor/autoload.php';
include_once "google-api-php-client/examples/templates/base.php";


$client = new \Google_Client();
$client->setApplicationName('demo_client');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/storage/credentials.json');
$sheets = new \Google_Service_Sheets($client);
$sheet_values = [];
$currentRow = 2;
$spreadsheetId = getenv("SPREADSHEET_ID");
$range = 'A2:AJ';
$rows = $sheets->spreadsheets_values->get($spreadsheetId, $range, ['majorDimension' => 'ROWS']);
if (isset($rows['values'])) {
    foreach ($rows['values'] as $row) {
        if (empty($row[0])) {
            break;
        }
         
        $sheet_values[] = [
            'ID' => $row[0],
            'Agent Code' => $row[1],
            'Branch ID' => $row[2],
            'Prefix' => $row[3],
            'Name' => $row[4],
            'Gender' => $row[5],
            'DOB' => $row[6],
            'State Present' => $row[7],
            'District Present' => $row[8],
            'Pincode Present' => $row[9],
            'Address Permanent' => $row[10],
            'City Permanent' => $row[11],
            'State Permanent' => $row[12],
            'District Permanent' => $row[13],
            'Pincode Permanent' => $row[14],
            'Pan.No' => $row[15],
            'Ration.No' => $row[16],
            'Aadhaar.No' => $row[17],
            'Father Name' => $row[18],
            'Mother Name' => $row[19],
            'Spouse Name' => $row[20],
            'Monthly Income' => $row[21],
            'Source of Income' => $row[22],
            'GST.No' => $row[23],
            'Introduced Enrollments' => $row[24],
            'Relationships' => $row[25],
            'Agent Score' => $row[26],
            'Enrollments' => $row[27],
            'Email' => $row[28],
        ];
    }
}

print_r($sheet_values);