<?php 
namespace Anand\Google;
use Revolution\Google\Sheets\Facades\Sheets;
/*use Illuminate\Support\Facades\Facade\Log;
*/
class Reader{

  protected $sheets;
  protected $spreadsheetId = '1y7PZWTv20fcv421_N7NG3WARPgUCNSsI3KSla0IApPA';
  private $download_path = '';
  private $sheets_list = ["agents","auctions","branches","charges","collections","document_types","documents_enrollments","documents_guarantors","employees","enrollments","groups","guarantees_enrollments","guarantees_guarantors","guarantors","holidays","policies","prize_money_disbursements","relationships","schemes","subscribers","users"];

  
  public function __construct(string $spreadsheetId, $client){
    $this->download_path = __DIR__.'/storage/csv/';
    if (!is_dir($this->download_path))
        mkdir($this->download_path, 0755, true);
    $client->setApplicationName(getenv('APP_NAME'));
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS_READONLY]);
    $client->setAccessType('offline');
    $client->setAuthConfig(env('GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION','credentials.json'));
    $this->sheets = new \Google_Service_Sheets($client);
    
    }
   public function downloadSheets() {     
    if(!$this->spreadsheetId) return;
    foreach($this->sheets_list as $sheet_name){
      $array_data = $this->sheets->spreadsheets_values->get($this->spreadsheetId, $sheet_name)->getValues();
      $file = fopen($this->download_path . $sheet_name . ".tsv", "w");
      foreach ($array_data as $item) {
        $utf_item = array_map("utf8_decode", $item);
        fputcsv($file, $utf_item, "\t");
      }
      fclose($file);
    }
  }
} 