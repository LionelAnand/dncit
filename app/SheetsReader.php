<?php 
/*namespace Anand\Google;
*//*use Revolution\Google\Sheets\Facades\Sheets;*/
use GoogleSheetReader.php;

require __DIR__ . '/vendor/autoload.php';

$sheets_list = ["agents","auctions","branches","charges","collections","document_types","documents_enrollments","documents_guarantors","employees","enrollments","groups","guarantees_enrollments","guarantees_guarantors","guarantors","holidays","policies","prize_money_disbursements","relationships","schemes","subscribers","users"];

$apiKey = getenv( 'GOOGLE_API_KEY' );
$client = new \Google_Client();
$client->setApplicationName('demo_client');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS_READONLY]);
$client->setAccessType('offline');
$jsonAuth = getenv('GOOGLE_SERVICE_ACCOUNT_JSON_LOCATION');
$client->setAuthConfig(json_decode($json));/*
$client->setAuthConfig(__DIR__ . '/storage/credentials.json');*/
$spreadsheetId = "1y7PZWTv20fcv421_N7NG3WARPgUCNSsI3KSla0IApPA";
$range ="";
$download_path = __DIR__ . '/storage/csv/';
if (!is_dir($download_path))
        mkdir($download_path, 0755, true);
$sheets = New Google_Service_Sheets($client);
if(!$spreadsheetId) return;
    foreach($sheets_list as $sheet_name){
      $array_data = $sheets->spreadsheets_values->get($spreadsheetId, $sheet_name)->getValues();
      $file = fopen($download_path . $sheet_name . ".tsv", "w");
      foreach ($array_data as $item) {
        $utf_item = array_map("utf8_decode", $item);
        fputcsv($file, $utf_item, "\t");
      }
      fclose($file);
    }
  